function varargout = DopplerProfiler_GUI(varargin)
% DOPPLERPROFILER_GUI MATLAB code for DopplerProfiler_GUI.fig
%      DOPPLERPROFILER_GUI, by itself, creates a new DOPPLERPROFILER_GUI or raises the existing
%      singleton*.
%
%      H = DOPPLERPROFILER_GUI returns the handle to a new DOPPLERPROFILER_GUI or the handle to
%      the existing singleton*.
%
%      DOPPLERPROFILER_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DOPPLERPROFILER_GUI.M with the given input arguments.
%
%      DOPPLERPROFILER_GUI('Property','Value',...) creates a new DOPPLERPROFILER_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DopplerProfiler_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DopplerProfiler_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DopplerProfiler_GUI

% Last Modified by GUIDE v2.5 17-Mar-2015 17:01:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @DopplerProfiler_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @DopplerProfiler_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DopplerProfiler_GUI is made visible.
function DopplerProfiler_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DopplerProfiler_GUI (see VARARGIN)
% ------------------------------------------------------------------------

%------------------------------------------------------------------------
% Choose default command line output for DopplerProfiler_GUI
% TODO F interp, plot flow
handles.output = hObject;

enable3D = false;
handles.image_resolution = [1.5 1.5 1.5]';
handles.model_t_resolution = 0.1; %ms
% general parameters
colors(1,:) = [1 0 0];
colors(2,:) = [0 0 1];
colors(3,:) = [0 0.5 0 ];

Mslice{1}=[1 0 0
    0 -1 0
    0 0 -1]';
Mslice{2}=[1 0 0
    0 0 1
    0 -1 0]';
Mslice{3}=[0 1 0
    0 0 1
    1 0 0]';
handles.boundary_meshes = [];

RotZ = eye(3);
RotY = eye(3);
RotX = eye(3);
CurrentAxisMatrix = eye(3);
handles.CurrentAxisMatrix = CurrentAxisMatrix;
handles.colors = colors;
handles.currentFrame =1;
handles.Mslice = Mslice;
factor=0.5;
% setup plots
axis_h = zeros(3,1);
handles.isdicom = false;

handles.profileSmoothnes=1;

axis_h(1)=handles.axes1;
axis_h(2)=handles.axes2;
axis_h(3)=handles.axes3;
axis_h(4)=handles.axes4;
handles.axis_h = axis_h;
handles.pause_loop = false;
guidata(hObject, handles);


handles.factor=factor;
handles.enable3D=enable3D;
handles.enableViewContour = true;
handles.Rot{1} = RotZ;
handles.Rot{2} = RotY;
handles.Rot{3} = RotX;
handles.n_images = [0 0];

% default colormaps

handles.colormap{1}=gray;
handles.colormap{2}=dopplerColors;
handles.overly_th = 5;



% default window limits
handles.windowLimits{1}=[0 255];
handles.windowLimits{2}=[0 256];
% Update handles structure
guidata(hObject, handles);

% input data----------------------------------------------
im = ImageType([10 10 10 2]',[-4.5 -4.5 2 0]',[1 1 1 1]',eye(4));
im.data(1:3:end)=20;
handles.im{1} = im;
handles.n_images = [1 0];
guidata(hObject, handles);
updateData();
% im = resampleImage(im,[],'spacing',[4 4 4]');
%-----------------------------------------------------------



% UIWAIT makes DopplerProfiler_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DopplerProfiler_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function fileMenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function fileMenuOpen_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpen_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename,pathname]= uigetfile('*.mhd','Open mhd image');
if ~filename
    return;
end
handles.isdicom = false;
im = read_mhd([pathname filename]);
handles.im{1} = im;
handles.windowLimits{1}=[min(im.data(:)) max(im.data(:))];
handles.im{2} = [];
handles.n_images = [1 0];
handles.image_resolution = [1.5 1.5 1.5]';
handles.model_t_resolution = 0.1; %ms
guidata(hObject, handles);
updateData();






% --- Executes on slider movement.
function slider_time_Callback(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

t=floor(get(hObject,'Value'));
set(handles.frame_label,'String',[  num2str(t) '/' num2str(handles.im{1}.size(4))  ]);
handles.currentFrame = t;
guidata(hObject, handles);
for i=3:-1:1
    handles =vp_sliceUpdate_Fcn(0,i);
    guidata(hObject, handles);
end


%;cf_plot_slice_x(bmc,dc,slx,t,venc, doppler_th); subplot(2,2,2);cf_plot_slice_y(bmc,dc,sly,t,venc, doppler_th); subplot(2,2,4); cf_plot_slice_z(bms,dsr,slz,t,venc, doppler_th); subplot(2,2,1);hold on;  hspx= cf_plot_sphere_x(bmc,slx,bms,slz,nav);    hold off;  subplot(2,2,2);hold on;  hspy= cf_plot_sphere_y(bmc,sly,bms,slz,nav);    hold off;   % Callback string.


% --- Executes during object creation, after setting all properties.
function slider_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
M = 1;
set(hObject,'Max',M,'Min',1,'Value',1,'SliderStep',[1/M 3/M]);
% slider_frame = uicontrol(fh,'Style','slider','Max',bms.size(4),'Min',1,'Value',1,'SliderStep',[1/bms.size(4) 10/bms.size(4)],'Position',[60 10 180 25],'callback',cb_frame);
% label_frame = uicontrol(fh,'Style','text','Position',[5 10 50 20],'String','Frame');
% label_frame2 = uicontrol(fh,'Style','text','Position',[250 10 40 20],'String',['1/' num2str(bmc.size(4))  ]);


% --- Executes on button press in pushbutton_pause.
function pushbutton_pause_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_pause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.pause_loop = true;
%set(handles.pushbutton_pause,'Enable','off');
guidata(hObject, handles);


% --- Executes on button press in pushbutton_play.
function pushbutton_play_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_play (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton_play,'Enable','off');
while true % this is broken by the pause button only
    
    %for t=1:handles.im{1}.size(4)
    handles=guidata(hObject);
    if (handles.pause_loop)
        break;
    end
    t = mod(handles.currentFrame, handles.im{1}.size(4))+1;
    set(handles.slider_time,'Value',t);
    set(handles.frame_label,'String',[  num2str(t) '/' num2str(handles.im{1}.size(4))  ]);
    handles.currentFrame = t;
    guidata(hObject, handles);
    for i=3:-1:1
        handles =vp_sliceUpdate_Fcn(0,i);
        guidata(hObject, handles);
    end
    pause(0.01);
end
set(handles.pushbutton_play,'Enable','on');
handles.pause_loop = false;
guidata(hObject, handles);
%set(handles.pushbutton_pause,'Enable','on');


% --------------------------------------------------------------------
function fileMenuOpenOverly_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuOpenOverly_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~handles.n_images(1)
    disp('No image data!')
    return;
end
handles.isdicom = false;

[filename,pathname]= uigetfile('*.mhd','Open mhd overly image');
if ~filename
    return;
end
im = read_mhd([pathname filename]);
im = resampleImage(im,handles.im{1},'in_gui');
handles.im{2} = im;
handles.windowLimits{2}=[-max(abs(im.data(:))) max(abs(im.data(:)))];
set(handles.checkbox_overlay,'Enable','on') ; %set to off
val = get(handles.checkbox_overlay,'Max');
set(handles.checkbox_overlay,'Value',val);  % uncheck
guidata(hObject, handles);
checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);
%updateData();


function updateData()
% this function is to be called when there are changes in input data. It
% should reload the views, adjust the slider, etc. It should first of all
% clean up all elements in the axis.

handles = guidata(gcf);

if ~handles.n_images(1)
    disp('No image data!')
    return;
end

for i=1:3
    all_objects = get(handles.axis_h(i),'Children');
    delete(all_objects );% if there is image data, remove it
end

if handles.enable3D
    all_objects = get(handles.axis_h(4),'Children');
    delete(all_objects );% if there is image data, remove it
end

for i=1:handles.im{1}.size(4)
    handles.roi_mask{i} =[];
end
guidata(gcf, handles);

bounds = handles.im{1}.GetBounds();
axis_radius = norm(bounds([2 4 6])-bounds([1 3 5]))/6;

handles.slice_thicknes = 1;

set(handles.slider_time,'Max',handles.im{1}.size(4),'Min',1,'Value',1,'SliderStep',[1/(handles.im{1}.size(4)-1) 3/handles.im{1}.size(4)]);
xmin=bounds(1);
xmax=bounds(2);
ymin=bounds(3);
ymax=bounds(4);
zmin=bounds(5);
zmax=bounds(6);
im_centroid =[(xmax+xmin)/2 (ymax+ymin)/2 (zmax+zmin)/2]';
handles.centroid = im_centroid;
for i=1:3
    handles.image_centre{i} =im_centroid;
    guidata(gcf, handles);
end

cidx = [3 3 2];
sp_l = cell(3,1); % dragable lines
sp_line = cell(3,1); % non dragable lines
for i=1:3
    sp_l{i,1} = imline(handles.axis_h(i),[-axis_radius*handles.factor 0; 0+axis_radius 0]);
    set(gcf,'CurrentAxes',handles.axis_h(i));
    sp_line{i,1} = line([ 0 0], [-axis_radius*handles.factor  +axis_radius],'Color',handles.colors(cidx(i),:));
    axis equal;
end

handles.axis_radius=axis_radius;
handles.sp_l = sp_l;
handles.sp_line = sp_line;
%handles.sp_p = sp_p;
handles.handle_3D_slice=[0 0 0];
guidata(gcf, handles);

% update callbacks
sp_sliceUpdate=cell(3,3);
for i=1:3
    handles =vp_sliceUpdate_Fcn(0,i);
    guidata(gcf, handles);
    sp_sliceUpdate{i} = @(x)vp_sliceUpdate_Fcn(x,i);
end

setColor(sp_l{1},handles.colors(2,:));
sp1_cf = @(x)vp_axis_constrainFcn(x,1);
setPositionConstraintFcn(sp_l{1},sp1_cf);
addNewPositionCallback(sp_l{1},sp_sliceUpdate{2});
addNewPositionCallback(sp_l{1},sp_sliceUpdate{3});


%% Subplot (2,1)

setColor(sp_l{2},handles.colors(1,:));
sp2_cf = @(x)vp_axis_constrainFcn(x,2);
setPositionConstraintFcn(sp_l{2},sp2_cf);
addNewPositionCallback(sp_l{2},sp_sliceUpdate{1});
addNewPositionCallback(sp_l{2},sp_sliceUpdate{3});

%% Subplot (2,2)

setColor(sp_l{3},handles.colors(1,:));
sp3_cf = @(x)vp_axis_constrainFcn(x,3);
setPositionConstraintFcn(sp_l{3},sp3_cf);
addNewPositionCallback(sp_l{3},sp_sliceUpdate{1});
addNewPositionCallback(sp_l{3},sp_sliceUpdate{2});


% --------------------------------------------------------------------
function fileMenuRemoveOverly_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuRemoveOverly_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~handles.n_images(2)
    disp('No overly data!')
    return;
end
handles.im{2} = [];
handles.windowLimits{2}=[0 255];
handles.n_images = [handles.n_images(1)  0];
guidata(hObject, handles);
val = get(handles.checkbox_overlay,'Min');
set(handles.checkbox_overlay,'Value',val);  % uncheck
set(handles.checkbox_overlay,'Enable','off') ; %set to off
updateData();


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

active = get(hObject,'Value');
if active
    handles.enable3D = true;
    guidata(hObject, handles);
    % update visualisation
    for i=3:-1:1
        handles =vp_sliceUpdate_Fcn(0,i);
        guidata(hObject, handles);
    end
else
    set(gcf,'CurrentAxes',handles.axis_h(4));
    % Something wrong here. These are not the appropriate handles
    for i=3:-1:1
        delete(handles.handle_3D_slice(i));
        handles.handle_3D_slice(i)=0;
    end
    handles.enable3D = false;
end
guidata(hObject, handles);


% --- Executes on button press in togglebutton_segment_ROI.
function togglebutton_segment_ROI_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_segment_ROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_segment_ROI

is_pressed = get(hObject,'Value');
view_number = 1;
if is_pressed
    set(hObject,'String','Finish');
    %h = imellipse(handles.axis_h(view_number));
    h = impoly(handles.axis_h(view_number));
    handles.ellipse = h;
    guidata(hObject, handles);
else
    % save the segmentation somewhere and delete the ellipse object. Keep
    % track of the segmentation frame too.
    
    set(hObject,'String','Segment ROI')
    im_handle = findobj(handles.axis_h(view_number),'type','image');
    BW = createMask(handles.ellipse,im_handle(1));
    handles.roi_mask{handles.currentFrame}.mask=BW;
    %vert = getVertices(handles.ellipse);
    vert = getPosition(handles.ellipse);
    handles.roi_mask{handles.currentFrame}.centroid=mean(vert);
    handles.roi_mask{handles.currentFrame}.points=vert;
    
    npts=50;
    handles.roi_mask{handles.currentFrame}.splinepoints= adaption_of_boundary(handles.roi_mask{handles.currentFrame}.points',npts);
    guidata(hObject, handles);
    delete(handles.ellipse);
    
    % add the segmentation to the segmentations list
    contents_cell = cellstr(get(handles.listbox_segmentations,'String'));
    % find the place of the current frame in the list
    pos = 0;
    for i=1:numel(contents_cell)
        n = str2num(contents_cell{i});
        if ~numel(n)
            % there is no elements so I just add this one
            new_cell{1}=num2str(handles.currentFrame);
            break;
        end
        if n==handles.currentFrame
            new_cell = contents_cell;
            break;
        end
        pos = pos+1;
        if n< handles.currentFrame
            new_cell{pos}=num2str(n);
            if i==numel(contents_cell)
                pos = pos+1;
                new_cell{pos}=num2str(handles.currentFrame);
            end
        else
            new_cell{pos}=num2str(handles.currentFrame);
            pos = pos+1;
            new_cell{pos}=num2str(n);
        end
        
    end
    
    set(handles.listbox_segmentations,'string',new_cell);
    guidata(hObject, handles);
    % update view 1
    vp_sliceUpdate_Fcn([],1);
end


% --- Executes on button press in checkbox_overlay.
function checkbox_overlay_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_overlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_overlay
OnVal = get(hObject,'Max');
val = get(hObject,'Value');
if (val==OnVal)
    handles.n_images = [handles.n_images(1)  1];
else
    handles.n_images = [handles.n_images(1)  0];
end
guidata(hObject, handles);
% update visualisation
for i=3:-1:1
    handles =vp_sliceUpdate_Fcn(0,i);
    guidata(hObject, handles);
end


% --- Executes on slider movement.
function slider_sliceThicknes_Callback(hObject, eventdata, handles)
% hObject    handle to slider_sliceThicknes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

v = get(hObject,'Value');
set(handles.text_sliceThicknes,'String',num2str(v));
handles.slice_thicknes = v;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_sliceThicknes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_sliceThicknes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
M=20;
set(hObject,'Max',M,'Min',1,'Value',1,'SliderStep',[1/(M-1) 3/(M-1)]);


% --- Executes on button press in pushbutton_profiles.
function pushbutton_profiles_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_profiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% calculate 3D flow profiles assuming the direction provided by the axes

% interpolate the contour
thereismask=false(handles.im{1}.size(4),1);
for i=1:numel(handles.roi_mask)
    thereismask(i)=numel(handles.roi_mask{i});
end

h = waitbar(0,'Propagate to all frames');
for fr=1:handles.im{1}.size(4)
    %disp(['Calculate profile for frame ' num2str(fr) ' out of ' num2str(handles.im{1}.size(4))])
    fl = calculate_one_profile(fr,handles,hObject, thereismask);
    handles = guidata(hObject);
    if fl<0
        return;
    end
    waitbar(fr/handles.im{1}.size(4));
end
close(h);

% maybe know we will need som temporal information. Also maybe a dialog
% with the temporal resolution desired

t = (0:handles.im{1}.size(4)-1)/handles.patient_info.frame_rate+handles.patient_info.trigger_delay;
t_cycle = [0 60/handles.patient_info.heart_rate];
t2 = 0:1/(10*handles.patient_info.frame_rate):max(t_cycle);

handles.time=t;
handles.sampling_t = handles.time(1):handles.model_t_resolution:60/handles.patient_info.heart_rate+handles.model_t_resolution/2;


% [filename,pathname]= uiputfile('*.txt','Save profiles to file','profiles.txt');
% if ~filename
%     return;
% end
%
% fid = fopen([pathname filename],'w');
%
% fprintf(fid,'%s\n','# Velocity profiles: px,py,pz - vx,vy, vz' );
%
% for fr=1:handles.im{1}.size(4)
%     handles.roi_mask{fr}.centroid;
%     handles.roi_mask{fr}.mask;
%     handles.profile{fr};
%
%     idx = find(abs(handles.profile{fr}.data)>0);
%     positions = handles.profile{fr}.GetPosition(idx');
%     vectors = handles.profile{fr}.GetValue(positions);
%     fprintf(fid,'%s %d\n','# Frame', fr );
%     fprintf(fid,'%f %f %f \t %f %f %f\n',[positions' vectors']');
%
% end
%
% fclose(fid);





guidata(hObject, handles);




% --- Executes on button press in pushbutton_see_profile.
function pushbutton_see_profile_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_see_profile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

fr = handles.currentFrame;

thereismask=false(handles.im{1}.size(4),1);
for i=1:numel(handles.roi_mask)
    thereismask(i)=numel(handles.roi_mask{i});
end

fl = calculate_one_profile(fr,handles,hObject, thereismask);
if fl<0
    return
end
handles = guidata(hObject);
view_number = 1;
M2_3D = eye(4);
M2_3D(1:3,1:3)= handles.CurrentAxisMatrix/handles.Rot{view_number} * handles.Mslice{view_number};
M2_3D(1:3,4)=  handles.image_centre{view_number};

im3Db = handles.im{1}.extractFrame(fr);
sliceb = resliceImage(im3Db,'mat',M2_3D,'interpolation','linear');

rgb2 =  matrixToRGB(handles.profile{fr}.data , jet(64),[0 max(handles.profile{fr}.data(:))]);
alpha2 = abs(handles.profile{fr}.data)>handles.overly_th;

figure,
subplot(1,2,1)
sliceb.show();
colormap(gray)
hold on;
handles.profile{fr}.quiver('use_jet','quiver_options',{0});
hold off;
axis equal;
subplot(1,2,2)
imagesc(sliceb.data)
hold on;
imagesc(rgb2,'AlphaData', alpha2); %'XData',[bds(1) bds(2)],'YData',[bds(3) bds(4)],
hold off;




%% Function calculate one profile
function fl = calculate_one_profile(profilen,handles,hObject, thereismask)

fl=1;
if (~isfield(handles,'roi_mask'))
    errordlg('Please draw a contour on the top left panel first','Error');
    fl=-1;
    return
end
%% Propagate the segmentation

if ( numel(handles.roi_mask)<profilen || ( ~(numel(handles.roi_mask)<profilen) && ~numel(handles.roi_mask{profilen}) ))
    
    
    %for i=1:numel(handles.roi_mask)
    for i=profilen
        if (~thereismask(i))
            i1 = find(thereismask(1:i),1,'last');
            e1 = find(thereismask((i+1):end),1,'last');
            
            i2 = find(thereismask((i+1):end),1,'first');
            e2 = find(thereismask(1:i),1,'first');
            
            p1=i1;
            p2=i2+i;
            
            if ~numel(i1)
                % there is no frame before this one, let's take from the
                % previous cycle
                i1 = e1+i;
                p1 = e1-numel(thereismask)+i;
            end
            if ~numel(i2)
                % there is no frame before this one, let's take from the
                % next cycle
                i2 = e2;
                p2 = e2+numel(thereismask);
            else
                i2 = i2+i;
            end
            % Find the relative position of the frame i with respect to i1
            % and i2
            p = (i-p1)/(p2-p1);
            
            %% calculate the deformation from one to another
            fittedpoints_1 = handles.roi_mask{i1}.splinepoints;
            fittedpoints_2 = handles.roi_mask{i2}.splinepoints;
            
            % TODO rigid alignments of the points to correct for rotations
            % first!
            
            % fit the transformation
            displacement = fittedpoints_2-fittedpoints_1;
            
            bounds([1 3])=min([fittedpoints_1 fittedpoints_2  ],[],2);
            bounds([2 4])=max([fittedpoints_1 fittedpoints_2 ],[],2);
            bounds = bounds(:);
            bsd = 3;
            rnth=0.0;
            border = 0;
            lambda=0.00001;
            
            spacing = (bounds([2 4])-bounds([1 3]))/2*1.1;
            
            control_points = createBSplineGrid(bounds, bsd, spacing, border);
            [B,removed_nodes] = createBSplineSamplingMatrix(fittedpoints_1', control_points,'od',1,'remove_empty_nodes',rnth);
            
            % Recover the two components independently:
            S = createBSplineContinuousDerivativeSamplingMatrix(control_points,'od',1,'operator','grad','remove_empty_nodes',removed_nodes);
            
            A = B'*B+lambda*S;
            
            b = B'*displacement(1,:)';
            xx = A\b;
            
            b = B'*displacement(2,:)';
            xy = A\b;
            
            coefficients = VectorImageType(control_points);
            
            coefficients.datax(find(removed_nodes==0)) = xx;
            coefficients.datay(find(removed_nodes==0)) = xy;
            
            B2 = createBSplineSamplingMatrix(fittedpoints_1', control_points,'od',1,'remove_empty_nodes',removed_nodes);
            
            displacement_imposedx = B2*xx;
            displacement_imposedy = B2*xy;
            % todo use the calculated p1, p2
            deformed_contour = fittedpoints_1 + (p)*[displacement_imposedx displacement_imposedy]';
            
            % retrieve the interpolated roi points
            handles.roi_mask{i}.points = deformed_contour';
            handles.roi_mask{i}.centroid = mean(deformed_contour');
            
            ce = impoly(handles.axes1 ,[0 0 10 10]);
            ce.setPosition(handles.roi_mask{i}.points);
            
            im_handle = findobj(handles.axes1,'type','image');
            BW = createMask(ce,im_handle(1));
            handles.roi_mask{i}.mask = BW;
            delete(ce);
            
            %B3 = createBSplineSamplingMatrix(fittedpoints_1', control_points,'od',1,'remove_empty_nodes',removed_nodes);
            %displacement_imposedx = B3*xx;
            %displacement_imposedy = B3*xy;
            
            handles.roi_mask{i}.splinepoints =handles.roi_mask{i}.points';
            
            
        end
        
        %% add to the listbox
        contents_cell = cellstr(get(handles.listbox_segmentations,'String'));
        % find the place of the current frame in the list
        pos = 1;
        n = str2num(contents_cell{1});
        j=1;
        if ~numel(n)
            % there is no elements so I just add this one
            new_cell{1}=num2str(i);
        else
            for j=1:numel(contents_cell)
                n = str2num(contents_cell{j});
                if n>=i
                    j=j-1;
                    break;
                end
                new_cell{j}=contents_cell{j};
            end
            
            % we found the position
            new_cell{j+1}=num2str(i);
            for k=j+1:numel(contents_cell)
                new_cell{k+1}=contents_cell{k};
            end
        end
        set(handles.listbox_segmentations,'string',new_cell);
        guidata(hObject, handles);
    end
end

% if ( numel(handles.roi_mask)<profilen || ~numel(handles.roi_mask{profilen}) )
%     errordlg(['Please draw a contour for frame ' num2str(profilen) ' on the top left panel first'],'Error');
%     fl=-1;
%     return
% end

flow_direction = handles.CurrentAxisMatrix(:,3);
aortic_point = handles.centroid(:);

% Change image_number 1 to 2 for the image ->it has to be the overlay!s

if handles.n_images(2)
    image_number=2;
else
    image_number=1;
    disp('There is no overlay, profile computed from background image');
end

view_number = 1;

M2_3D = eye(4);
M2_3D(1:3,1:3)= handles.CurrentAxisMatrix/handles.Rot{view_number} * handles.Mslice{view_number};
M2_3D(1:3,4)=  handles.image_centre{view_number};

% this will be the sample slice


im3D = handles.im{image_number}.extractFrame(profilen);
slice = resliceImage(im3D,'mat',M2_3D,'interpolation','linear','thicknes',handles.slice_thicknes);
if (handles.isdicom)
    if handles.slice_thicknes>1
        cumulated_data = zeros(slice.size(1),slice.size(2) );
        for s = 1:(2*(handles.slice_thicknes-1)+1)
            [ix,iy,iz]=ndgrid(1:slice.size(1), 1:slice.size(2), s);
            positions = slice.GetPosition([ix(:) iy(:) iz(:)]'); % beam source is at 0 0 0
            bd_norm = sqrt(sum(positions.^2));
            beamdirections = -positions./[bd_norm ; bd_norm ; bd_norm];
            true_velocity_magnitude = slice.data(:,:,s)./reshape(dot(beamdirections,flow_direction*ones(1,size(beamdirections,2))),[slice.size(1) slice.size(2)]);
            cumulated_data =cumulated_data +true_velocity_magnitude;
        end
        true_velocity_magnitude = cumulated_data/(2*(handles.slice_thicknes-1)+1);
    else
        
        positions = slice.GetPosition(1:prod(slice.size)); % beam source is at 0 0 0
        bd_norm = sqrt(sum(positions.^2));
        beamdirections = -positions./[bd_norm ; bd_norm ; bd_norm];
        true_velocity_magnitude = slice.data./reshape(dot(beamdirections,flow_direction*ones(1,size(beamdirections,2))),[slice.size(1) slice.size(2)]);
        
    end
else
    true_velocity_magnitude = slice.data;
end

true_velocity_magnitude  = true_velocity_magnitude .* handles.roi_mask{profilen}.mask';

if handles.profileSmoothnes>1
    % smooth the profile with a Gaussian convolution kernel
    hsize = ((handles.profileSmoothnes-1)*2+1)*[1 1];
    sigma = hsize(1)/3.5;
    h = fspecial('gaussian', hsize, sigma);
    true_velocity_magnitude = imfilter(true_velocity_magnitude,h);
    true_velocity_magnitude  = true_velocity_magnitude .* handles.roi_mask{profilen}.mask';
end

sliceref = resliceImage(im3D,'mat',M2_3D,'interpolation','linear');

profile = VectorImageType(sliceref);
clear sliceref;
profile.datax(1:end) = true_velocity_magnitude*flow_direction(1);
profile.datay(1:end) = true_velocity_magnitude*flow_direction(2);
profile.dataz(1:end) = true_velocity_magnitude*flow_direction(3);
profile.data(1:end) = true_velocity_magnitude;

handles.profile{profilen} = profile;
guidata(hObject, handles);


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function about_menu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to about_menu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

txt = sprintf('%s\n%s\n%s','DopplerProfiler (c) Alberto Gomez 2014 - Kings College London','Thanks to Nick Gaddum for the MR I/O.','Contact: alberto.gomez@kcl.ac.uk');
h = msgbox(txt,'About DopplerProfiler');

% --- Executes on selection change in listbox_segmentations.
function listbox_segmentations_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_segmentations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_segmentations contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_segmentations


% --- Executes during object creation, after setting all properties.
function listbox_segmentations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_segmentations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function editMenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to editMenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function settingsMenu_tag_Callback(hObject, eventdata, handles)
% hObject    handle to settingsMenu_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
current_settings.resolution = handles.image_resolution;
current_settings.model_t_resolution = handles.model_t_resolution; % ms

dialog_handle = settings_vp_dialog(current_settings);
%if dialog_handle
%    return;
%end
new_settings = guidata(dialog_handle);
close(gcf);

% Update the settings


change_sp_res = sum(new_settings.resolution-handles.image_resolution);

handles.image_resolution = new_settings.resolution;
handles.model_t_resolution = new_settings.model_t_resolution;

t = (0:handles.im{1}.size(4)-1)/handles.patient_info.frame_rate+handles.patient_info.trigger_delay;
handles.time=t;

handles.sampling_t = handles.time(1):handles.model_t_resolution:60/handles.patient_info.heart_rate+handles.model_t_resolution/2;
%handles.sampling_t = handles.time(1):handles.model_t_resolution:60/handles.patient_info.heart_rate+handles.model_t_resolution/2;
guidata(hObject, handles);
if handles.isdicom
    if (change_sp_res)
        BMode_image =  convertToCartesianCoordinatesCentered(handles.im_spherical{1},'resolution',handles.image_resolution);
        handles.im{1} = BMode_image;
        clear BMode_image;
    end
    % Add to viewer
    handles.windowLimits{1}=[min(handles.im{1}.data(:)) max(handles.im{1}.data(:))];
    if (change_sp_res)
        handles.im{2} = [];
        handles.n_images = [1 0];
        guidata(hObject, handles);
        updateData();
        handles = guidata(hObject);
        
        Doppler_image =  convertToCartesianCoordinatesCentered(handles.im_spherical{2},'resolution',handles.image_resolution);
        handles.im{2} = Doppler_image;
        clear Doppler_image;
        
        handles.im{2} = resampleImage(handles.im{2},handles.im{1},'in_gui');
    end
    handles.windowLimits{2}=[-max(abs(handles.im{2}.data(:))) max(abs(handles.im{2}.data(:)))];
    set(handles.checkbox_overlay,'Enable','on') ; %set to off
    val = get(handles.checkbox_overlay,'Max');
    set(handles.checkbox_overlay,'Value',val);  % uncheck
    guidata(hObject, handles);
    checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);
else
    if (change_sp_res)
        handles.im{1} = resampleImage(handles.im{1},[],'in_gui','spacing',[handles.image_resolution ; handles.im{1}.spacing(end)],'interpolation','linear');
        clear BMode_image;
    end
    % Add to viewer
    handles.windowLimits{1}=[min(handles.im{1}.data(:)) max(handles.im{1}.data(:))];
    if (change_sp_res)
        tmp_im = handles.im{2};
        handles.im{2} = [];
        handles.n_images = [1 0];
        guidata(hObject, handles);
        updateData();
        handles = guidata(hObject);
        
        handles.im{2} = resampleImage(tmp_im,handles.im{1},'in_gui');
        clear tmp_im;
    end
    handles.windowLimits{2}=[-max(abs(handles.im{2}.data(:))) max(abs(handles.im{2}.data(:)))];
    set(handles.checkbox_overlay,'Enable','on') ; %set to off
    val = get(handles.checkbox_overlay,'Max');
    set(handles.checkbox_overlay,'Value',val);  % uncheck
    guidata(hObject, handles);
    checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);
    
end

% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[filename,pathname]= uigetfile({'*','All Files'},'Open DICOMDIR or par-rec file');
if ~filename
    return;
end

if strcmp(lower(filename(end-2:end)),'par')
    disp('This is a par-rec image');
    
    [Doppler_image, BMode_image,params] = read_parrec2DFlow([pathname filename]);
    handles.patient_info = params;
    
    
    % set the good orientation automatically (compensate for this later on)
    %handles.CurrentAxisMatrix = BMode_image.orientation(1:3,1:3);
    %handles.centroid = BMode_image.orientation(1:3,4);
    
    BMode_image = setOrientation(BMode_image, BMode_image.orientation);
    Doppler_image = setOrientation(Doppler_image, Doppler_image.orientation);
else
    handles.isdicom = true;
    [thumbnails, patient]=readDICOMDIR([pathname filename]);
    patim = DICOMthumbnails(thumbnails,patient);
    dicom_file = [pathname patient{patim(1)}.image{patim(2)}];
    
    [BMode_image_spherical, Doppler_image_spherical,params] = exportIE33DICOM(dicom_file);
    
    handles.patient_info = params;
    handles.im_spherical{1} = BMode_image_spherical;
    handles.im_spherical{2} = Doppler_image_spherical;
    
    % TODO convert to cartesian
    BMode_image =  convertToCartesianCoordinatesCentered(BMode_image_spherical,'resolution',handles.image_resolution);
    Doppler_image =  convertToCartesianCoordinatesCentered(Doppler_image_spherical,'resolution',handles.image_resolution);
    
end


% Make the period be multiple of 0.025
factor_mult = 0.025;
handles.factor_mult = factor_mult;
handles.patient_info.heart_rate = 60/(round((60/handles.patient_info.heart_rate)/factor_mult)*factor_mult);

% Add to viewer

handles.im{1} = BMode_image;
handles.windowLimits{1}=[min(BMode_image.data(:)) max(BMode_image.data(:))];
handles.im{2} = [];
handles.n_images = [1 0];
guidata(hObject, handles);
updateData();
handles = guidata(hObject);

handles.im{2} = resampleImage(Doppler_image,BMode_image,'in_gui');
handles.windowLimits{2}=[-max(abs(Doppler_image.data(:))) max(abs(Doppler_image.data(:)))];
set(handles.checkbox_overlay,'Enable','on') ; %set to off
val = get(handles.checkbox_overlay,'Max');
set(handles.checkbox_overlay,'Value',val);  % uncheck
guidata(hObject, handles);
checkbox_overlay_Callback(handles.checkbox_overlay, eventdata, handles);

% --------------------------------------------------------------------
function fileMenuQuit_tag_Callback(hObject, eventdata, handles)
% hObject    handle to fileMenuQuit_tag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)


% --- Executes on button press in pushbutton_removeOneROI.
function pushbutton_removeOneROI_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_removeOneROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = cellstr(get(handles.listbox_segmentations,'String'));
profilen =  str2num(contents{get(handles.listbox_segmentations,'Value')});

j=0;
new_cell{1}=[''];
for i=setdiff(1:numel(handles.roi_mask),profilen)
    if numel(handles.roi_mask{i})
        j=j+1;
        new_cell{j}=num2str(i);
    end
end



handles.roi_mask{profilen}=[];
% change the selection of the listbox first!
set(handles.listbox_segmentations,'Value',1);
set(handles.listbox_segmentations,'string',new_cell);
guidata(hObject, handles);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
active = get(hObject,'Value');
if active
    handles.enableViewContour = true;
    guidata(hObject, handles);
else
    handles.enableViewContour = false;
    guidata(hObject, handles);
end
% update visualisation
for i=3:-1:1
    handles =vp_sliceUpdate_Fcn(0,i);
    guidata(hObject, handles);
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton_fitToModel.
function pushbutton_fitToModel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_fitToModel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~numel(handles.roi_mask)
    disp('Please segment all frames')
end

[mesh_format,ok] = listdlg('PromptString','Select a mesh format:',...
                        'SelectionMode','single',...
                        'ListString',{'VTK','CRIMSON'})
if ~ok
    return;
end
    
if handles.n_images(2)
    image_number=2;
else
    image_number=1;
    disp('There is no overlay, profile computed from background image');
end

% Ultrasound matrix
view_number = 1;
Multrasound_3D = eye(4);
Multrasound_3D(1:3,1:3)= handles.CurrentAxisMatrix/handles.Rot{view_number} * handles.Mslice{view_number};
Multrasound_3D(1:3,4)=  handles.image_centre{view_number};

ultrasound_corr_point = zeros(3,1);
ultrasound_corr_point(1)= str2num(get(handles.edit_us_x,'String'));
ultrasound_corr_point(2)= str2num(get(handles.edit_us_y,'String'));
ultrasound_corr_point(3)= str2num(get(handles.edit_us_z,'String'));

ultrasound_corr_point_2D = Multrasound_3D \ [ultrasound_corr_point ;1];
ultrasound_corr_point_2D = ultrasound_corr_point_2D(1:2);

% For example use this: model_corr_point = [158.277 -6.40576 229.54]'
model_corr_point = zeros(3,1);
model_corr_point(1)= str2num(get(handles.edit_model_x,'String'));
model_corr_point(2)= str2num(get(handles.edit_model_y,'String'));
model_corr_point(3)= str2num(get(handles.edit_model_z,'String'));


if mesh_format ==1
    boundary_m = read_vtkMesh();
else
    boundary_m = read_nbcMesh();
end

% Extract the border points of the mesh
[~,paths] = boundary_m.detectBorders();
model_points = boundary_m.points(paths{1}([1:end],1),:);
model_all_points = boundary_m.points;
% transform the model points to 2D

centroid_model = mean(model_all_points);

model_points_centred = model_all_points-ones(size(model_all_points,1),1)*centroid_model;
[V,D] = eig(model_points_centred'*model_points_centred);

M_model = eye(4);
M_model(1:3,1:3) = V(:,[3 2 1]);
M_model(1:3,4) = centroid_model(:);

normals_ = boundary_m.GetNormalAtFaces(1:boundary_m.ntriangles);
mean_normal_ = mean(normals_);

flow_direction_MRI = M_model(1:3,3); 
if (dot(mean_normal_ ,M_model(1:3,3))>0)
    M_model(1:3,1:3) = [ -V(:,3) V(:,2) -V(:,1) ];
    flow_direction_MRI = -flow_direction_MRI;
end

M_model(1:3,1) = cross(M_model(1:3,2), M_model(1:3,3));

if get(handles.checkbox_positiveOrientation,'Value')==0
    M_model(1:3,1:3) = [ -M_model(1:3,1) M_model(1:3,2) -M_model(1:3,3) ];
    flow_direction_MRI = -flow_direction_MRI;
end

if get(handles.checkbox_positiveSign,'Value')==0
    flow_direction_MRI = -flow_direction_MRI;
end


%ptri = boundary_m.points(boundary_m.triangles(1,:),:);
%v21 = ptri(1,:)-ptri(2,:);
%v23 = ptri(3,:)-ptri(2,:);
%crossv  =cross(v23,v21);



model_corr_point_proyected = M_model \ [model_corr_point ;1];
model_corr_point_proyected(3)=0;
model_corr_point_backproyected = M_model * model_corr_point_proyected;




% Set the x vector to point towards the manually picked point for
% consistency
%ref_x_vector = (model_corr_point_backproyected(1:3)-centroid_model');
%ref_x_vector = ref_x_vector/norm(ref_x_vector);

%M_model(1:3,1)=ref_x_vector;
%M_model(1:3,2) = cross(M_model(1:3,3), M_model(1:3,1));



%flow_direction = handles.CurrentAxisMatrix(:,3);

model_points_2D = M_model \ [model_points' ; ones(1,size(model_points,1))];
model_points_2D = model_points_2D(1:2,:)';

model_corr_point_2D = M_model \ [model_corr_point ;1];
model_corr_point_2D = model_corr_point_2D(1:2);

% retrieve the points of the segmentation corresponding to the first frame
% and align centroids before calculating the rotation

centroid_image = mean(handles.roi_mask{1}.splinepoints');
centroid_model = mean(model_points_2D);

% find the rotation between the two points and apply the inverse to the

vus = [ultrasound_corr_point_2D-centroid_image' ;0]/norm(ultrasound_corr_point_2D-centroid_image');
vmo = [model_corr_point_2D ;0]/norm(model_corr_point_2D);
angle_1 = atan2(vus(2),vus(1));
angle_2 = atan2(vmo(2),vmo(1));
angle_ = angle_2 - angle_1;
angle_*180/pi;

%cosa = dot(vmo, vus);

if false
    figure,
    plotpoints2(handles.roi_mask{1}.splinepoints'-ones(size(handles.roi_mask{1}.splinepoints,2),1)*centroid_image,'.-');
    hold on;
    plotpoints2(ultrasound_corr_point_2D'-centroid_image,'bo');
    plotpoints2(model_corr_point_2D','ro');
    plotpoints2(model_points_2D,'r.-');
    plotpoints2([0 0 ; ultrasound_corr_point_2D'-centroid_image],'-')
    plotpoints2([0 0 ; model_corr_point_2D'],'r')
    hold off;
end

RotMatrix = [cos(angle_) -sin(angle_); sin(angle_) cos(angle_)]; % rotation matrix

boundary_point_idx = find(boundary_m.attributes(2).attribute_array==0);
mesh_boundary_points_3D = boundary_m.points(boundary_point_idx,:); % only the non-boundary points
%mesh_boundary_points_3D = boundary_m.points; %all points
mesh_boundary_points_2D = M_model \ [mesh_boundary_points_3D' ; ones(1,size(mesh_boundary_points_3D,1))];
mesh_boundary_points_2D = mesh_boundary_points_2D(1:2,:)';

% extract the points of the segmentation, and interpolate them to the
% current time resolution
t = handles.sampling_t;
us_splinepoints = [];
for i=1:numel(handles.time)
    us_splinepoints = cat(3,us_splinepoints,handles.roi_mask{i}.splinepoints);
end
% interpolate spline points to the time steps o fthe model
%npoints = size(us_splinepoints,2);
%[nin,tin]=ndgrid(1:npoints,handles.time);
%[nout,tout]=ndgrid(1:npoints,t);
%us_points_2Dx = interpn(nin,tin,squeeze(us_splinepoints(1,:,:)),nout,tout);
%us_points_2Dy = interpn(nin,tin,squeeze(us_splinepoints(2,:,:)),nout,tout);

npts=50;
model_points_2D_rotated = (RotMatrix \ model_points_2D')';
adapted_model_points_2D_rotated= adaption_of_boundary(model_points_2D_rotated',npts);
model_corr_point_2D_rotated = RotMatrix \ model_corr_point_2D;

% create the 2D+t (=3D) profile image

velocityProfileImage_tmp = setOrientation(handles.profile{1},Multrasound_3D);

newmatrix = eye(3);
newmatrix(1:2,1:2)=velocityProfileImage_tmp.orientation(1:2,1:2);
velocityProfileImage2Dpt = ImageType([velocityProfileImage_tmp.size(1:2); numel(handles.profile)],...
    [velocityProfileImage_tmp.origin(1:2);handles.im{image_number}.origin(end)], ...
    [velocityProfileImage_tmp.spacing(1:2) ; handles.im{image_number}.spacing(end)],newmatrix);

for i=1:numel(handles.profile)
    
    velocityProfileImage2Dpt.data(:,:,i) = handles.profile{i}.data;
    %velocityProfileImage2Dpt.datax(:,:,i) = handles.profile{i}.datax;
    %velocityProfileImage2Dpt.datay(:,:,i) = handles.profile{i}.datay;
    %velocityProfileImage2Dpt.dataz(:,:,i) = handles.profile{i}.dataz;
end


% the interpolated 2D contours from echo are in newx newy.
h = waitbar(0,'Please wait while inflow is calculated at each input data point');
steps = numel(handles.time);
true_velocity_magnitude = zeros(numel(boundary_point_idx),steps);
for i=1:numel(handles.time)
    
    %% Fit spline to [us_points_2Dx us_points_2Dy] to the model_points_2D
    us_points_2D = us_splinepoints(:,:,i)';
    % centre the points before rotation
    displacement_us = mean(us_points_2D);
    
    % 1 - rotate  the model points_2D.
    
    
    
    if false
        % this fugure shows the result of rotating the us to the model
        % points
        ultrasound_corr_point_2D_tx = (RotMatrix\(ultrasound_corr_point_2D -displacement_us'))';
        figure,
        plotpoints2(us_points_2D-ones(size(us_points_2D,1),1)*displacement_us,'.-');
        hold on;
        plotpoints2(ultrasound_corr_point_2D'-displacement_us,'o')
        
        plotpoints2(model_corr_point_2D_rotated','ro');
        plotpoints2(model_points_2D_rotated,'r.-');
        hold off;
    end
    
    
    adapted_us_points_2D= adaption_of_boundary(us_points_2D',npts);
    
    %% Transform the ultrasound points using the R matrix
    
    if false
        % this figure shows the correspondance between model points and us
        % points
        figure,
        plotpoints2(adapted_us_points_2D'-ones(size(us_points_2D,1),1)*displacement_us,'.-')
        hold on;
        for i=1:size(adapted_us_points_2D,2)
            text(adapted_us_points_2D(1,i)-displacement_us(1), adapted_us_points_2D(2,i)-displacement_us(2),num2str(i))
            text(adapted_model_points_2D_rotated(1,i), adapted_model_points_2D_rotated(2,i),num2str(i))
        end
        plotpoints2(adapted_model_points_2D_rotated','r.-')
        hold off;
    end
    
    %% Calculate deformation between the two sets of points
    displacement = (adapted_us_points_2D-(ones(size(us_points_2D,1),1)*displacement_us)')-adapted_model_points_2D_rotated;
    
    
    bounds([1 3])=min([adapted_us_points_2D adapted_model_points_2D_rotated],[],2);
    bounds([2 4])=max([adapted_us_points_2D adapted_model_points_2D_rotated],[],2);
    bounds = bounds(:);
    bsd = 3;
    rnth=0.0;
    border = 0;
    lambda=0.00001;
    
    spacing = (bounds([2 4])-bounds([1 3]))/2*1.1;
    
    control_points = createBSplineGrid(bounds, bsd, spacing, border);
    [B,removed_nodes] = createBSplineSamplingMatrix(adapted_model_points_2D_rotated', control_points,'od',1,'remove_empty_nodes',rnth);
    
    % Recover the two components independently:
    S = createBSplineContinuousDerivativeSamplingMatrix(control_points,'od',1,'operator','grad','remove_empty_nodes',removed_nodes);
    
    A = B'*B+lambda*S;
    
    b = B'*displacement(1,:)';
    xx = A\b;
    
    b = B'*displacement(2,:)';
    xy = A\b;
    
    coefficients = VectorImageType(control_points);
    
    coefficients.datax(find(removed_nodes==0)) = xx;
    coefficients.datay(find(removed_nodes==0)) = xy;
    
    displacement_imposedx = B*xx;
    displacement_imposedy = B*xy;
    
    
    if false
        figure
        quiver(adapted_us_points_2D(1,:)-displacement_us(1)*ones(1,size(adapted_us_points_2D,2)), adapted_us_points_2D(2,:)-displacement_us(2)*ones(1,size(adapted_us_points_2D,2)), -displacement(1,:), -displacement(2,:),0)
        hold on;
        quiver(adapted_us_points_2D(1,:)-displacement_us(1)*ones(1,size(adapted_us_points_2D,2)), adapted_us_points_2D(2,:)-displacement_us(2)*ones(1,size(adapted_us_points_2D,2)), -displacement_imposedx', -displacement_imposedy',0,'Color',[1 0 0])
        plotpoints2(adapted_model_points_2D_rotated','r.-')
        plotpoints2(adapted_us_points_2D'-ones(size(us_points_2D,1),1)*displacement_us,'.-')
        hold off;
    end
    
    adapted_modelBoundary_points_2D_rotated_warped = adapted_model_points_2D_rotated + [displacement_imposedx displacement_imposedy]';
    
    
    if false
        % this figure shows the correspondance between model points and us
        % points
        figure,
        plotpoints2(adapted_us_points_2D'-(ones(size(us_points_2D,1),1)*displacement_us),'.-')
        hold on;
        for i=1:size(adapted_us_points_2D,2)
            text(adapted_us_points_2D(1,i)-displacement_us(1), adapted_us_points_2D(2,i)-displacement_us(2),num2str(i))
            text(adapted_modelBoundary_points_2D_rotated_warped(1,i), adapted_modelBoundary_points_2D_rotated_warped(2,i),num2str(i))
        end
        plotpoints2(adapted_modelBoundary_points_2D_rotated_warped','r.-')
        hold off;
    end
    
    
    %% Deform all the points in the boundary_m (which are not contour)
    % 1. Rotate the points
    mesh_boundary_points_2D_rotated = (RotMatrix \ mesh_boundary_points_2D')';
    
    if false
        figure,
        plotpoints2(mesh_boundary_points_2D_rotated,'.')
        hold on;
        plotpoints2(adapted_model_points_2D_rotated','r-.')
        
        hold off;
    end
    
    
    % 2 warp the points
    B = createBSplineSamplingMatrix(mesh_boundary_points_2D_rotated, control_points,'od',1,'remove_empty_nodes',removed_nodes);
    displacement_allPoints_imposedx = B*xx;
    displacement_allPoints_imposedy = B*xy;
    mesh_boundary_points_2D_rotated_warped = mesh_boundary_points_2D_rotated + [displacement_allPoints_imposedx displacement_allPoints_imposedy];
    % 3. Displace to conmpensate centroid of image data
    mesh_boundary_points_2D_rotated_warped_centered = mesh_boundary_points_2D_rotated_warped + ones(size(mesh_boundary_points_2D_rotated_warped,1),1)*displacement_us;
    
    if false
        % this figure shows the correspondance between model points and us
        % points
        figure,
        subplot(1,3,1)
        plotpoints2(mesh_boundary_points_2D_rotated_warped(1:100,:),'.')
        hold on;
        plotpoints2(mesh_boundary_points_2D_rotated(1:100,:),'ro')
        hold off;
        
        subplot(1,3,2)
        hold on;
        plotpoints2(adapted_us_points_2D'-ones(size(adapted_us_points_2D,2),1)*displacement_us,'.-')
        plotpoints2(mesh_boundary_points_2D_rotated,'ro')
        plotpoints2(mesh_boundary_points_2D_rotated_warped,'r.')
        plotpoints2(adapted_modelBoundary_points_2D_rotated_warped','r*')
        hold off;
        
        
        subplot(1,3,3)
        hold on;
        plotpoints2(adapted_us_points_2D'-ones(size(adapted_us_points_2D,2),1)*displacement_us,'.-')
        plotpoints2(mesh_boundary_points_2D_rotated_warped,'r.')
        plotpoints2(adapted_modelBoundary_points_2D_rotated_warped','r*')
        hold off;
    end
    
    
    
    if (0)
        % display the deformation field
        figure
        plotpoints2(adapted_us_points_2D','.-')
        hold on;
        quiver(mesh_boundary_points_2D_rotated_warped_centered(:,1), mesh_boundary_points_2D_rotated_warped_centered(:,2), -displacement_allPoints_imposedx, -displacement_allPoints_imposedy,0)
        plotpoints2(adapted_model_points_2D_rotated'+ones(size(adapted_us_points_2D,2),1)*displacement_us,'r-.')
        hold off;
    end
    
    
    
    
    %% SO FAR SO GOOD
    
    
    %% Now interpolate the pre-calculated_profile image to these points
    
    
    cm_to_mm = 10;
    %deformed_points_3D = Multrasound_3D * [deformed_points_2D_tx' ; zeros(1,size(deformed_points_2D_tx,1)); ones(1,size(deformed_points_2D_tx,1))];
    
    %mesh_boundary_points_rotated_warped_centered_3D = Multrasound_3D * [ mesh_boundary_points_2D_rotated_warped_centered' ; zeros(1,size(mesh_boundary_points_2D_rotated_warped_centered,1)); ones(1,size(mesh_boundary_points_2D_rotated_warped_centered,1))];
    %mesh_boundary_points_rotated_warped_centered_3D(4,:) =handles.time(i);
    mesh_boundary_points_rotated_warped_centered_2D = [mesh_boundary_points_2D_rotated_warped_centered ones(size(mesh_boundary_points_2D_rotated_warped_centered,1),1)*handles.time(i)];
    true_velocity_magnitude(:,i) = velocityProfileImage2Dpt.GetValue(mesh_boundary_points_rotated_warped_centered_2D','linear')*cm_to_mm;
    
    if false
        figure
        values = true_velocity_magnitude(:,i);
        subplot(1,2,1)
        scatter(mesh_boundary_points_rotated_warped_centered_2D(:,1), mesh_boundary_points_rotated_warped_centered_2D(:,2), values*0+30, values)
        hold on;
        plotpoints2(handles.roi_mask{1}.splinepoints','.-');
        plotpoints2(ultrasound_corr_point_2D','*')
        hold off;
        axis equal
        
        subplot(1,2,2)
        scatter(mesh_boundary_points_2D(:,1), mesh_boundary_points_2D(:,2), values*0+30, values)
        
        hold on
        plotpoints2(model_points_2D	,'r-');
        plotpoints2(model_corr_point_2D','*r')
        hold off;
        axis equal
        
    end
    
    waitbar(i / steps);
end
true_velocity_magnitude(true_velocity_magnitude~=true_velocity_magnitude)=0;
close(h)
%% Smoothing

if handles.smoothing_t.which_method ==1 % Fourier smoothing
    nmodes = handles.smoothing_t.nmodes;
    
    % Fourier smoothing is not possible because there is a sample gap in
    % the input data....
    
    input_time = handles.time;
    
    M = ceil((length(input_time)+1)/2);
    h = waitbar(0,'Please wait while inflow is Fourier interpolated.');
    true_velocity_magnitude_interpolated = zeros(size(mesh_boundary_points_2D,1),numel(handles.sampling_t));
    steps = size(mesh_boundary_points_2D,1);
    for ii=1:size(mesh_boundary_points_2D,1)
        
        input_velocity = true_velocity_magnitude(ii,:);
        
        Y = fft(input_velocity);
        if ~mod(length(input_time),2) && mod(length(handles.sampling_t),2) || ...
                mod(length(input_time),2) && ~mod(length(handles.sampling_t),2)
            Y2=fftshift(Y);
            Y2(end+1)=Y2(1);
            Y = ifftshift(Y2);
        end
        Yshifted = fftshift(Y);
        nmodes = min(nmodes, M);
        nzeros = M-nmodes;
        Yshifted([1:nzeros end-nzeros+1:end])=0;
        
        % padd with zeros
        npads = ceil((numel(handles.sampling_t)-numel(Yshifted))/2);
        
        upsampling = numel(handles.sampling_t)/numel(input_time);
        Ypadded = padarray(Yshifted,[0 npads],0);
        true_velocity_magnitude_interpolated(ii,:) = ifft(ifftshift(Ypadded))*upsampling;
        
        % the band width will be
        fs =1/(input_time(2)-input_time(1)); %Hz
        Af=fs/length(input_time);
        BW = fs/2 + Af*npads; % Hz
        new_fs = 2*BW;
        new_At = 1/new_fs;
        handles.sampling_t = (0:numel(handles.sampling_t)-1)*new_At;
        
        % add the last sample to insure that first and last are exactly the
        % same
        
        waitbar(ii / steps);
    end
    close(h);
    
    
elseif handles.smoothing_t.which_method ==2 % Cyclic bsplines
    ncontrol_points = handles.smoothing_t.ncontrol_points;
    lambda = handles.smoothing_t.lambda;
    mu = handles.smoothing_t.mu;
    
    bounds = [0 handles.time(end)]';
    bsd = 3;
    rnth=0.25;
    border = 0;
    spacing =(bounds(2)-bounds(1))/(ncontrol_points-1);
    control_points = createBSplineGrid(bounds, bsd, spacing(:), border,'cyclic',1,[0 60/handles.patient_info.heart_rate]);
    [B,removed_nodes] = createBSplineSamplingMatrix(handles.time(:), control_points,'od',1,'remove_empty_nodes',rnth,'cyclic',1);
    Adiv = createBSplineContinuousDerivativeSamplingMatrix(control_points,'od',1,'operator','grad','remove_empty_nodes',removed_nodes);
    
    tfull = abs(t(:)*ones(1,numel(handles.time)) - ones(numel(t),1)*handles.time(:)');
    tfull_min = min(tfull');
    regindices = find(tfull_min > (handles.time(2)-handles.time(1))/2);
    
    regpos = t(regindices)';
    S = createBSplineDerivativeSamplingMatrix(regpos, control_points,'bsd',bsd,'od',1,'remove_empty_nodes',removed_nodes,'operator','grad');
    A = B'*B+lambda/(1-lambda)*(S'*S)+mu/(1-mu)*(Adiv);
    
    B2 = createBSplineSamplingMatrix(t(:), control_points,'od',1,'remove_empty_nodes',removed_nodes,'cyclic',1);
    true_velocity_magnitude_interpolated = zeros(numel(boundary_point_idx),numel(t));
    h = waitbar(0,'Please wait while inflow is Cyclic-bspline interpolated.');
    steps = size(mesh_boundary_points_2D,1);
    for ii=1:size(mesh_boundary_points_2D,1)
        
        b = B'*true_velocity_magnitude(ii,:)';
        c = A\b;
        
        b2 = B2*c;
        
        true_velocity_magnitude_interpolated(ii,:) = b2;
        waitbar(ii / steps);
    end
    close(h)

else
    disp('Error: smoothing must be fourier or cyclic bsplines')
    return;
end

% calculate inflow area
[~,areas]=boundary_m.GetTriangleNormal(1:boundary_m.ntriangles);
inflow_area = sum(areas);

h = waitbar(0,'Please wait while meshes are evaluated.');
steps = numel(t);
for i=1:numel(t)
    boundary_mesh{i} = MeshType(boundary_m);
    at = AttributeType();
    at.attribute='field';
    at.name='inflow_velocity';
    at.numComp=3; % between 1 and 4
    at.type='float';
    at.nelements= boundary_mesh{i}.npoints;
    at.attribute_array = zeros(boundary_m.npoints,3);
    at.attribute_array(boundary_point_idx,:)= true_velocity_magnitude_interpolated(:,i)*flow_direction_MRI';
    ind =  boundary_mesh{i}.find_attribute(at.name);
    if (ind > 0)
        boundary_mesh{i}.attributes(ind)=at;
    else
        n_attributes = numel( boundary_mesh{i}.attributes);
        boundary_mesh{i}.attributes(n_attributes+1)=at;
    end
    
    % for display
    %im3D = handles.im{image_number}.extractVolume(t(i));
    %slice = resliceImage(im3D,'mat',Multrasound_3D,'interpolation','linear','thicknes',handles.slice_thicknes);
    
    waitbar(i / steps);
end
close(h);

handles.boundary_meshes = boundary_mesh;
guidata(hObject, handles);

mean_vel = nanmean(true_velocity_magnitude_interpolated)*inflow_area;
max_vel = nanmax(true_velocity_magnitude_interpolated);
min_vel = nanmin(true_velocity_magnitude_interpolated);

mean_veli = nanmean(true_velocity_magnitude)*inflow_area;
max_veli = nanmax(true_velocity_magnitude);
min_veli = nanmin(true_velocity_magnitude);

figure,

subplot(2,3,1)
title('Max velocity')
hold on;
plot(handles.sampling_t,max_vel,'b')
plot(handles.time,max_veli,'--b')
hold off;
grid on;
legend('Smoothed','Original')
xlabel('Time (s)')
ylabel('vel (mm/s)')

subplot(2,3,2)
title('Approx. flow rate')
hold on;
plot(handles.sampling_t,mean_vel,'r')
plot(handles.time,mean_veli,'--r')
hold off;
grid on;
legend('Smoothed','Original')
xlabel('Time (s)')
ylabel('flow rate (mm^3/s)')
subplot(2,3,3)
title('Min velocity')
hold on;
plot(handles.sampling_t,min_vel,'k')
plot(handles.time,min_veli,'--k')
hold off;
grid on;
legend('Smoothed','Original')
xlabel('Time (s)')
ylabel('vel (mm/s)')


cardiac_output = trapz(handles.sampling_t,mean_vel)*60/10^6;
cardiac_outputi = trapz(handles.time,mean_veli)*60/10^6;

subplot(2,1,2)
axis off;
text(0,0.8,'                                           From smoothed data                        From input data')
text(0,0.6,['Max velocity (over time):              ' num2str(max(max_vel))   ' mm/s           ' num2str(max(max_veli)) ' mm/s'])
text(0,0.4,['Mean fr (over time):                   ' num2str(mean(mean_vel)) ' mm^3/s         ' num2str(mean(mean_veli)) ' mm^3/s'])
text(0,0.2,['Min velocity (over time):              ' num2str(min(min_vel))   ' mm/s           ' num2str(min(min_veli)) ' mm/s'])
text(0,0,['Cardiac output:                          ' num2str(cardiac_output) ' l/min          ' num2str(cardiac_outputi) ' l/s'])





% --- Executes on button press in pushbutton_savemodelInflow.
function pushbutton_savemodelInflow_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_savemodelInflow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~numel(handles.boundary_meshes)
    disp('ERROR: fit to model first')
    return
end

[filename,pathname]= uiputfile({'*','All Files'},'Save profiles to file. Write name without extension.','profiles');
if ~filename
    return;
end


steps = numel(handles.boundary_meshes);
if get(handles.checkbox_saveVTKMeshes,'Value')==1
    % save meshes
    mkdir([pathname '/meshes']);
    
    % write meshes
    h = waitbar(0,'Please wait while meshes are written...');
    for i=1:steps
        write_vtkMesh([pathname '/meshes/' filename num2str(i) '.vtk'], handles.boundary_meshes{i});
        waitbar(i / steps);
    end
    close(h);
end

% write bct files

% Format
% npoints ntimeframes
% p0x p0y p0z ntimeframes
%v00x v00y v00z t0
%v01x v01y v01z t1
% ...
% p1x p1y p1z ntimeframes
%v10x v10y v10z t0
%v11x v11y v11z t1
% ...
%
fid = fopen([pathname 'bct.dat'],'w');
npoints = handles.boundary_meshes{1}.npoints;
fprintf(fid,'%d %d\n', npoints, steps);
h = waitbar(0,'Please wait while bct.dat file is written...');
timepoints = zeros(steps,npoints,3);
times = zeros(steps,1);
for i=1:steps
    timepoints(i,:,:)=handles.boundary_meshes{i}.attributes(3).attribute_array(:,:);
    times(i) = (i-1)*handles.model_t_resolution;
end
for i=1:npoints
    fprintf(fid,'%f %f %f %d\n',[handles.boundary_meshes{1}.points(i,:) steps]');
    veltime = squeeze(timepoints(:,i,:));
    fprintf(fid,'%f %f %f %f\n',[veltime times]');
    waitbar(i / npoints);
end
close(h);
fclose(fid);



function edit_us_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_us_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_us_x as text
%        str2double(get(hObject,'String')) returns contents of edit_us_x as a double


% --- Executes during object creation, after setting all properties.
function edit_us_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_us_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_us_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_us_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_us_y as text
%        str2double(get(hObject,'String')) returns contents of edit_us_y as a double


% --- Executes during object creation, after setting all properties.
function edit_us_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_us_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_us_z_Callback(hObject, eventdata, handles)
% hObject    handle to edit_us_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_us_z as text
%        str2double(get(hObject,'String')) returns contents of edit_us_z as a double


% --- Executes during object creation, after setting all properties.
function edit_us_z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_us_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_uspoint_lock.
function checkbox_uspoint_lock_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_uspoint_lock (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_uspoint_lock



function edit_model_x_Callback(hObject, eventdata, handles)
% hObject    handle to edit_model_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_model_x as text
%        str2double(get(hObject,'String')) returns contents of edit_model_x as a double


% --- Executes during object creation, after setting all properties.
function edit_model_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_model_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_model_y_Callback(hObject, eventdata, handles)
% hObject    handle to edit_model_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_model_y as text
%        str2double(get(hObject,'String')) returns contents of edit_model_y as a double


% --- Executes during object creation, after setting all properties.
function edit_model_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_model_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_model_z_Callback(hObject, eventdata, handles)
% hObject    handle to edit_model_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_model_z as text
%        str2double(get(hObject,'String')) returns contents of edit_model_z as a double


% --- Executes during object creation, after setting all properties.
function edit_model_z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_model_z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_modelpoint_lock.
function checkbox_modelpoint_lock_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_modelpoint_lock (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_modelpoint_lock


% --- Executes on slider movement.
function slider_profileSmoothnes_Callback(hObject, eventdata, handles)
% hObject    handle to slider_profileSmoothnes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

v = get(hObject,'Value');
set(handles.text_profileSmoothnes,'String',num2str(v));
handles.profileSmoothnes = v;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_profileSmoothnes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_profileSmoothnes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in checkbox_saveVTKMeshes.
function checkbox_saveVTKMeshes_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_saveVTKMeshes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_saveVTKMeshes


% --------------------------------------------------------------------
function Untitled_5_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

dialog_handle = patientInformation_vp(handles);
if ~dialog_handle
    return;
end
close(gcf);


% --- Executes on button press in checkbox_tSmoothing.
function checkbox_tSmoothing_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_tSmoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_tSmoothing


% --------------------------------------------------------------------
function Untitled_tSmoothingMenu_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_tSmoothingMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.t_smoothing.fourier.nmodes = 4;
handles.t_smoothing.cbsplines.ncontrolpoints = 15;
handles.t_smoothing.cbsplines.lambda = 0.0;
handles.t_smoothing.cbsplines.mu = 0.1;

handles.t_smoothing.which_method = 1; % fourier by default


t = (0:handles.im{1}.size(4)-1)/handles.patient_info.frame_rate+handles.patient_info.trigger_delay;
handles.time=t;



handles.t_smoothing.x = handles.time;
for i=1:numel(handles.profile)
    handles.t_smoothing.y(i)  = mean(handles.profile{i}.data(:));
end


out = setting_tsmoothing_vp(handles);
close(gcf);

handles.smoothing_t.which_method = out.which_method;
handles.smoothing_t.ncontrol_points = out.ncontrol_points;
handles.smoothing_t.nmodes = out.nmodes;
handles.smoothing_t.lambda = out.lambda;
handles.smoothing_t.mu = out.mu;
guidata(hObject, handles);


% --- Executes on button press in checkbox_positiveOrientation.
function checkbox_positiveOrientation_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_positiveOrientation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_positiveOrientation


% --- Executes on button press in checkbox_positiveSign.
function checkbox_positiveSign_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_positiveSign (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_positiveSign
