function mesh =read_nbcMesh(filenamenbc, filenameebc, filenamecoord)
% Function for reading a mesh in a nbc - crimson compatible format
%
% mesh  = read_vtkMesh(filenamenbc, filenameebc, filenamecoord);
%
% examples:
%   mesh=read_nbcMesh('inflow_aorta.nbc','inflow_aorta.ebc','the.coordinates');
%   mesh would be of the class MeshType

if(exist('filenamenbc','var')==0)
    [filenamenbc, pathname] = uigetfile('*.nbc', 'Read nbc file','Select file');
    filenamenbc = [pathname filenamenbc];
end

if(exist('filenameebc','var')==0)
    [filenameebc, pathname] = uigetfile([ pathname '*.ebc'], 'Read ebc file','Select file');
    filenameebc = [pathname filenameebc];
end

if(exist('filenamecoord','var')==0)
    [filenamecoord, pathname] = uigetfile([ pathname '*.coordinates'], 'Read nbc file','Select file');
    filenamecoord = [pathname filenamecoord];
end


node_index = dlmread(filenamenbc);
coordinates = dlmread(filenamecoord);


mesh_coordinates = coordinates(:,2:4);
topology = dlmread(filenameebc);
mesh_topology = topology(:,3:5);

final_coords = mesh_coordinates(node_index,:)*0;
final_topology = mesh_topology*0;

for i=1:numel(node_index)
    [row,col] = find(mesh_topology==node_index(i));
    for j=1:numel(row)
        final_topology(row(j), col(j))=i;
    end
    nodeidx = find(coordinates(:,1)==node_index(i));
    final_coords(i,:) = coordinates(nodeidx,2:end);
end

[i1,~] = find(final_topology==0);
final_topology(i1,:)=[];

%final_coords = mesh_coordinates;
%final_topology = mesh_topology;

mesh = MeshType(size(final_coords,1),size(final_topology,1));
mesh.points = final_coords;
mesh.triangles = final_topology;

end

