
folder = '/home/ag09/data/tmp/Anonymized - 2501229E/Aortic Dissection/sQFLOW_1 - 401';

for i=1:25
    info(i) = dicominfo([folder '/IM-0008-00' sprintf('%02.0f', i) '-0001.dcm']);
    DATA(:,:,i) = dicomread([folder '/IM-0008-00' sprintf('%02.0f', i) '-0001.dcm']);
end
sz = size(DATA)';
sz = [sz([1 2]) ; 1 ; sz(3)];
sp = [info3(i).PixelSpacing' info3(i).SliceThickness 60/info3(i).HeartRate/sz(4)]';
o = -(sz-1)/2.*sp;

im = ImageType(sz, o, sp,eye(4) );
im.data = zeros(sz');
im.data(:) = double(DATA(:));

%%

% 
% for i=1:25
%     info(i) = dicominfo([folder '/IM-0009-00' sprintf('%02.0f', i+25) '-0001.dcm']);
%     DATA(:,:,i) = dicomread([folder '/IM-0009-00' sprintf('%02.0f', i+25) '-0001.dcm']);
% end
% 
% sz = size(DATA)';
% sz = [sz([1 2]) ; 1 ; sz(3)];
% sp = [1 1 1 1]';
% o = -(sz-1)/2.*sp;
% 
% im2 = ImageType(sz, o, sp,eye(4) );
% im2.data = zeros(sz');
% im2.data(:) = double(DATA(:));
% 

%%

for i=1:25
    info3(i) = dicominfo([folder '/IM-0010-00' sprintf('%02.0f', i+50) '-0001.dcm']);
    DATA(:,:,i) = dicomread([folder '/IM-0010-00' sprintf('%02.0f', i+50) '-0001.dcm']);
end

sz = size(DATA)';
sz = [sz([1 2]) ; 1 ; sz(3)];
sp = [info3(i).PixelSpacing' info3(i).SliceThickness 60/info3(i).HeartRate/sz(4)]';
venc = info3(i).RealWorldValueMappingSequence.Item_1.RealWorldValueIntercept;
%info3(i).RealWorldValueMappingSequence.Item_1.RealWorldValueSlope
o = -(sz-1)/2.*sp;

im3 = ImageType(sz, o, sp,eye(4) );
im3.data = zeros(sz');
im3.data(:) = (double(DATA(:))/(max(double(DATA(:)))/2)-1)*venc;

%%
write_mhd([folder '/magnitude_image.mhd'],im3);
write_mhd([folder '/velocity_image.mhd'],im);