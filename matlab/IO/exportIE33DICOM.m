function [BMode_image, Doppler_image,params] = exportIE33DICOM(dicom_file)
% dicom_file = '/media/AGOMEZ_KCL_2/data/test_data/carotid2/IM_0022'

dinfo = dicominfo(dicom_file);

%% declare dicom tags

t_privateData = 'Private_200d_3cf5';
t_data= 'Private_200d_3cf1'; %   data
t_nframes= 'Private_200d_3010'; %
t_nbytes= 'Private_200d_3011'; %
t_raw_data= 'Private_200d_3cf3'; %  Binary ECG signal data
t_compression= 'Private_200d_3cfa'; %  Compression mechanism, typically ’None’
t_headers= 'Private_200d_3cfb';
t_is3DColourDoppler = 'Private_200d_34xx_Creator';
t_heartRate = 'HeartRate';

% bmode

t_depth= 'Private_200d_3103';

t_theta1value= 'Private_200d_3104';
t_theta2value= 'Private_200d_3105';

t_phi1value= 'Private_200d_3203';
t_phi2value= 'Private_200d_3204';
t_frameRate= 'Private_200d_3207';
t_delay= 'Private_200d_3972';

% Doppler

t_minDepthDoppler= 'Private_200d_3812';
t_maxDepthDoppler= 'Private_200d_3813';
t_venc = 'Private_200d_3817';



if isfield(dinfo,t_is3DColourDoppler)
    params.type = '3DCDI';
    %disp('Colour Doppler')
    nelements=[32 32];
else
    Doppler_image=[];
    params.type = 'other';
    %disp('BMode')
    %nelements=[80 80];
    nelements=[416 64];
    %nelements=[80 64];
end

%% Extract data

private_data =  dinfo.(t_privateData);

% read BMode
bmode_private_data = private_data.(['Item_' num2str(2) ]).(t_data).Item_1;
nframes= bmode_private_data.(t_nframes);
nbytes =  bmode_private_data.(t_nbytes);
raw_data =  bmode_private_data.(t_raw_data);
compression =  bmode_private_data.(t_compression);
headers =  bmode_private_data.(t_headers);

% create image in spherical coordinates
theta1_value  = str2double(dinfo.(t_theta1value));
theta2_value  = str2double(dinfo.(t_theta2value));
phi1_value  = str2double(dinfo.(t_phi1value));
phi2_value  = str2double(dinfo.(t_phi2value));
minDepthBMode=0;
depth_value  = str2double(dinfo.(t_depth));
maxDepthBMode=depth_value;
frameRate = str2double(dinfo.(t_frameRate));
triggerDelay = str2double(dinfo.(t_delay))/1000;
heartRate =  dinfo.(t_heartRate);
params.heart_rate = heartRate;
params.frame_rate=frameRate ;
params.trigger_delay = triggerDelay;



bmode_type='uint8';
volume_4D = read_image_data(raw_data,nelements,bmode_type);
rel_th = 1.5;
BMode_image = cropImageWithBorders(volume_4D, theta1_value,theta2_value,phi1_value, phi2_value,minDepthBMode,maxDepthBMode, frameRate, triggerDelay,rel_th);
clear volume_4D;
Doppler_image=[];
if isfield(dinfo,t_is3DColourDoppler)
    minDepthDoppler  = str2double(dinfo.(t_minDepthDoppler));
    maxDepthDoppler  = str2double(dinfo.(t_maxDepthDoppler));
    venc_value=  str2double(dinfo.(t_venc));
    params.venc = venc_value/2;
    
    doppler_private_data = private_data.(['Item_' num2str(6) ]).(t_data).Item_1; % 6 doppler, 7 power, 8 variance
    %nframes= doppler_private_data.(t_nframes);
    nbytes =  doppler_private_data.(t_nbytes);
    raw_data_doppler=  doppler_private_data.(t_raw_data);
    %compression =  doppler_private_data.(t_compression);
    headers =  doppler_private_data.(t_headers);
    CDI_type='int8';
    volume_4D_doppler = read_image_data(raw_data_doppler,nelements,CDI_type);
    rel_th = 1;
    Doppler_image = cropImageWithBorders(volume_4D_doppler, theta1_value,theta2_value,phi1_value, phi2_value,minDepthDoppler,maxDepthDoppler, frameRate, triggerDelay,rel_th);
    Doppler_image.data = double(Doppler_image.data)/127*venc_value/2;
    clear volume_4D_doppler;
    
end

end

function volume_4D = read_image_data(raw_data,nelements,number_type)

% read the header.
header_length =  typecast(raw_data(1:4),'uint32'); % in bytes
nframes =  double(typecast(raw_data(5:8),'uint32')); % this should be identical to nslices
datalength = length(raw_data);


% calculate offsets
offsets = zeros(nframes,1);
for n=1:nframes
    offsets(n)=typecast(raw_data([9 10 11 12]+(n-1)*4),'uint32');
end

%volume = zeros(132,486,nslices-1);

for n=1
    offset = offsets(n);
    if n==nframes
        next_offset = datalength;
    else
        next_offset = offsets(n+1);
    end
    
    offset = offset+16 ; % I do not know why...
    
    nbytes2 = next_offset - offset; % compressed length
    %uncompressed_length = nbytes; % uncompressed length
    %range = ((offset):(offset+nbytes2))+1; % the plus 1 because numbering
    %starts at 0
    range = ((offset):(offset+nbytes2))+1;
    %compressed_data = dec2hex(raw_data(range));
    compressed_data = raw_data(range);
    z=zlibdecode(compressed_data);
    clear compressed_data;
    %zbin = reshape(dec2hex(z)',1,[]);
    zshort = typecast(z,number_type);
    
%    filename = '/media/AGOMEZ_KCL_2/data/test_data/pablo/IM_0001.raw';
%    fid = fopen(filename,'wb');
%    fwrite(fid,zshort,'uint8');
%    fclose(fid);




    
    clear z;
    c=floor(numel(zshort)/nelements(1)/nelements(2));
    imageData=reshape(zshort(1:nelements(1)*nelements(2)*c),[c nelements(1) nelements(2)]);
    clear z_short
end

volume_4D = zeros([size(imageData) nframes]);
volume_4D(:,:,:,1)=imageData;
clear imageData;
for n=2:nframes
    offset = offsets(n);
    if n==nframes
        next_offset = datalength;
    else
        next_offset = offsets(n+1);
    end
    % Content of the first 16 bytes: (for nframes =17)
     % Taking two bytes (r to l)
    % |        16       |        32        |
    % |        245      |        117       |
    % |        96       |        13        |
    % |        112      |        16        |
     % Taking two bytes (l to r)
    % |        1        |        2         |
    % |        95       |        87        |
    % |        6        |        208       |
    % |        7        |        1         |
     % Taking four bytes (r to l)
    % |                8208                |
    % |               30197                |
    % |                3424                |
    % |                4208                |
     % Taking four bytes (l to r)
    % |                258                 |
    % |               24407                |
    % |                1744                |
    % |                1793                |
    offset = offset+16 ; 
    
    
    nbytes2 = next_offset - offset; % compressed length
    %uncompressed_length = nbytes; % uncompressed length
    %starts at 0
    range = ((offset+1):(offset+nbytes2));% the plus 1 because numbering
    %compressed_data = dec2hex(raw_data(range));
    compressed_data = raw_data(range);
    z=zlibdecode(compressed_data);
    clear compressed_data;
    %zbin = reshape(dec2hex(z)',1,[]);
    zshort = typecast(z,number_type);
    clear z;
    c=floor(numel(zshort)/nelements(1)/nelements(2));
    volume_4D(:,:,:,n)=reshape(zshort(1:nelements(1)*nelements(2)*c),[c nelements(1) nelements(2)]);
    clear z_short
end
end

function borders= findBorders(volume_4D,rel_th)

ssdx = zeros(size(volume_4D,1)-1,1);
for i=1:size(volume_4D,1)-1
    slice1 = volume_4D(i,:,:,:);
    slice2 = volume_4D(i+1,:,:,:);
    ssdx(i) = sum((slice1(:)-slice2(:)).^2);
end

bx = findExternalPeaks(ssdx,rel_th);

% along y
ssdy = zeros(size(volume_4D,2)-1,1);
for i=1:size(volume_4D,2)-1
    slice1 = volume_4D(:,i,:,:);
    slice2 = volume_4D(:,i+1,:,:);
    ssdy(i) = sum((slice1(:)-slice2(:)).^2);
end
by = findExternalPeaks(ssdy,rel_th);

% along z
ssdz = zeros(size(volume_4D,3)-1,1);
for i=1:size(volume_4D,3)-1
    slice1 = volume_4D(:,:,i,:);
    slice2 = volume_4D(:,:,i+1,:);
    ssdz(i) = sum((slice1(:)-slice2(:)).^2);
end
bz = findExternalPeaks(ssdz,rel_th);

borders=[bx by bz];




end

function b = findExternalPeaks(ssdx,rel_th)
% now do that!
centre_index_x = round(numel(ssdx)/2);
centre_value_x = ssdx(centre_index_x);
accumulated_value = centre_value_x ;
for j=1:centre_index_x-1
    %old_centre_value=centre_value_x;
    index_minus=centre_index_x-j;
    centre_value_x = ssdx(index_minus);
    accumulated_value=accumulated_value+centre_value_x ;
    mean_value = accumulated_value/(j+1);
    variation = (centre_value_x -mean_value-1)/(mean_value+1); % the plus one is added to avoid divisions by zero
    variation = abs(variation);
    if (variation >rel_th)
        break;
    end
end

centre_value_x = ssdx(ceil(end/1));
accumulated_value = centre_value_x ;
for j=1:(numel(ssdx) - centre_index_x)
    %old_centre_value=centre_value_x;
    index_plus=centre_index_x+j;
    centre_value_x = ssdx(index_plus);
    accumulated_value=accumulated_value+centre_value_x ;
    mean_value = accumulated_value/(j+1);
    variation = (centre_value_x -mean_value-1)/(mean_value+1); % the plus one is added to avoid divisions by zero
    variation = abs(variation);
    if (variation >rel_th)
        break;
    end
end

b=[index_minus index_plus];

end


function im = cropImageWithBorders(volume_4D, theta1_,theta2_,phi1, phi2,minDepth,maxDepth,frameRate, triggerDelay,rel_th)

bbm= findBorders(volume_4D,rel_th);
volume_4D =volume_4D(1:bbm(2),1:bbm(4),1:bbm(6),:);

volume_4D =permute(volume_4D,[2 3 1 4]);
volume_4D = flipdim(volume_4D ,1);
volume_4D = flipdim(volume_4D ,2);

% TODO deal with the t dimension
s = size(volume_4D)';

% For some reason (comparing with Philips data) inversion of theta  was necessary
theta1=-theta2_;
theta2=-theta1_;

ranges =[ (theta2-theta1)*180/pi (phi2-phi1)*180/pi  maxDepth-minDepth 12]';
origin= [theta1*180/pi phi1*180/pi minDepth  triggerDelay];
spacing = ranges./(s-1);
spacing(4)=1/frameRate;

im = ImageType(s, origin',spacing, eye(4));
im.data = volume_4D;

end