function [thumbnail, patient] = readDICOMDIR(dicomdir)
%%
infodir = dicominfo(dicomdir);
fields = fieldnames(infodir.DirectoryRecordSequence);
nfiles = numel(fields);

npat=0;
nelements=0;
for i=1:nfiles
    %infodir.DirectoryRecordSequence.(fields{i})
    if strcmp(infodir.DirectoryRecordSequence.(fields{i}).DirectoryRecordType,'PATIENT')
        % This is a patient
        npat=npat+1;
        patient{npat}.name = infodir.DirectoryRecordSequence.(fields{i}).PatientName.FamilyName;
        nimages=0;
    else
        if strcmp(infodir.DirectoryRecordSequence.(fields{i}).DirectoryRecordType,'IMAGE')
            nimages=nimages+1;
            nelements = nelements+1;
            name = infodir.DirectoryRecordSequence.(fields{i}).ReferencedFileID;
            i1=find(name=='/' | name=='\');
            name(i1)=filesep;
            patient{npat}.image{nimages} = name;
        end
    end
end
h = waitbar(0,'Read all images from all patients...');


n=1;
for p=1:numel(patient)
    disp(['Read patient ' patient{p}.name ])
    for i=1:numel(patient{p}.image)
        disp(['         Read image ' patient{p}.image{i} ])
        name = [dicomdir(1:end-9) filesep patient{p}.image{i}];
        i1=find(name=='/' | name=='\');
        name(i1)=filesep;
        thumbnail{p,i} = dicomread(name);
        waitbar(n / nelements);
        n=n+1;
    end
end
close(h);

end