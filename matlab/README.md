# Velocity profile with ultrasound

## About this software

This is a Matlab prototype software developed by Alberto Gomez (alberto.gomez@kcl.ac.uk) for patient-specific velocity profile calculation from PC-MRI and 3D colour Doppler ultrasound images.

## Instructions of use

This section needs to be improved, but basically launch `DopplerProfiler_GUI` in matlab

## Related software

This software includes: [the Image Processing Toolkit](https://www.mathworks.com/matlabcentral/fileexchange/41594-medical-image-processing-toolbox) and is loosely based on the [Simple Viewer](https://www.mathworks.com/matlabcentral/fileexchange/47457-simple-viewer-3d).

## Requirements

This software has been tested in Matlab R2015a, R2015b and R2016a


