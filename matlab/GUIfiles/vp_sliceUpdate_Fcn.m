function  myhandles =vp_sliceUpdate_Fcn(x,view_to_update )
%UNTITLED2 Summary of this function goes here
%  This function is called when the line is rotated or when the point is
%  moved around
myhandles = guidata(gcf);

M2_3D = eye(4);
M2_3D(1:3,1:3)= myhandles.CurrentAxisMatrix/myhandles.Rot{view_to_update} * myhandles.Mslice{view_to_update};
M2_3D(1:3,4)=  myhandles.image_centre{view_to_update};

im3D = myhandles.im{1}.extractFrame(myhandles.currentFrame);
im3D.paddingValue=0;
[sl3,slice] = resliceImage(im3D,'mat',M2_3D);
rgb1 =  matrixToRGB(slice.data', myhandles.colormap{1},myhandles.windowLimits{1});

if myhandles.n_images(2)
    % add the overlay image
    im3D2 = myhandles.im{2}.extractFrame(myhandles.currentFrame);
    im3D2.paddingValue=0;
    [~,slice2] = resliceImage(im3D2,'mat',M2_3D);
    rgb2 =  matrixToRGB(slice2.data' , myhandles.colormap{2},myhandles.windowLimits{2});
    alpha2 = abs(slice2.data')>myhandles.overly_th;
end


bds = slice.GetBounds();
bds([2 4]) =bds([2 4])+10000*[eps eps]'; 
delete(findobj(myhandles.axis_h(view_to_update),'type','image'));% if there is image data, remove it (all layers!)
delete(findobj(myhandles.axis_h(view_to_update),'type','rectangle'));% if there is image data, remove it
delete(findobj(myhandles.axis_h(view_to_update),'type','line','-and','Tag','contour'));% if there is image data, remove it

% Maybe change the order in which items are added to axes???--------
% underlying image. I have to set back the handle for the imline
% Add objects to plot
set(gcf,'CurrentAxes',myhandles.axis_h(view_to_update));
%hold on;
hold on;
imagesc(rgb1,'XData',[bds(1) bds(2)],'YData',[bds(3) bds(4)]); % the data is transposed because matlab has rows along y
%imagesc(rgb1,'Parent', myhandles.axis_h(view_to_update),'XData',[bds(1) bds(2)],'YData',[bds(3) bds(4)]); % the data is transposed because matlab has rows along y
%hold off;

% exterior rectangle

rectangle('Position',[bds(1) bds(3)+eps bds(2)-bds(1) (bds(4)-bds(3))+eps ],'EdgeColor',myhandles.colors(view_to_update,:),'LineWidth',3);

% Segmentation
show_segmentation=false;
if  view_to_update==1
    %all_objects = get(myhandles.axis_h(view_to_update),'Children');
    %     for i=1:numel(myhandles.contourhandle)
    %         if myhandles.contourhandle{i}
    %             delete(myhandles.contourhandle{i});
    %             myhandles.contourhandle{i}=0;
    %             guidata(gcf,myhandles);
    %         end
    %     end
    if myhandles.enableViewContour
        % check if there is a segmentation available for this frame
        if numel(myhandles.roi_mask{myhandles.currentFrame})
            % Good! there is a segmentation. Let's display it
            show_segmentation=true;
            myhandles.contourhandle{myhandles.currentFrame} = plot(myhandles.roi_mask{myhandles.currentFrame}.splinepoints(1,:),myhandles.roi_mask{myhandles.currentFrame}.splinepoints(2,:),'w-','LineWidth',2);
            set(myhandles.contourhandle{myhandles.currentFrame},'Tag','contour');
            guidata(gcf,myhandles);
        end
    end
end




%hold off;
% overly image
if myhandles.n_images(2)
    % hold on;
    imagesc(rgb2,'XData',[bds(1) bds(2)],'YData',[bds(3) bds(4)],'AlphaData', alpha2); % the data is transposed because matlab has rows along y
    %imagesc(rgb2,'Parent', myhandles.axis_h(view_to_update),'XData',[bds(1) bds(2)],'YData',[bds(3) bds(4)],'AlphaData', alpha2); % the data is transposed because matlab has rows along y
    %hold off;
end

axis(bds(:)');
axis on;
all_objects = get(myhandles.axis_h(view_to_update),'Children');

if myhandles.n_images(2)
    if show_segmentation
        set(myhandles.axis_h(view_to_update),'Children',all_objects([3 5:end 1 2 4]));
    else
        set(myhandles.axis_h(view_to_update),'Children',all_objects([2 4:end 1 3]));
    end
else
    if show_segmentation
        %line rectangle image line hgggroup -> rectangle line hggroup line
        %image (first line to fourth place)
        set(myhandles.axis_h(view_to_update),'Children',all_objects([2 4:end 1 3]));
    else
        %rectangle image line hgggroup -> rectangle line hggroup image
        set(myhandles.axis_h(view_to_update),'Children',all_objects([1 3:end 2]));
    end
end

%get(get(myhandles.axis_h(view_to_update),'Children'),'Type')
if myhandles.enable3D
    set(gcf,'CurrentAxes',myhandles.axis_h(4));
    if myhandles.handle_3D_slice(view_to_update);
        delete(myhandles.handle_3D_slice(view_to_update));
    end
    hold on;
    myhandles.handle_3D_slice(view_to_update) = sl3.show(); colormap(gray)
    hold off;
    axis(im3D.GetBounds()')
    axis equal
    xlabel('x')
    ylabel('y')
    zlabel('z')
else
    set(gcf,'CurrentAxes',myhandles.axis_h(4));
    axis off;
end
if get(myhandles.checkbox_uspoint_lock,'Value')==0
    set(myhandles.edit_us_x,'String',sprintf('%3.2f',M2_3D(1,4)));
    set(myhandles.edit_us_y,'String',sprintf('%3.2f',M2_3D(2,4)));
    set(myhandles.edit_us_z,'String',sprintf('%3.2f',M2_3D(3,4)));
end
guidata(gcf,myhandles);



end

