function varargout = settings_vp_dialog(varargin)
% SETTINGS_FLOWQ_DIALOG MATLAB code for settings_flowq_dialog.fig
%      SETTINGS_FLOWQ_DIALOG, by itself, creates a new SETTINGS_FLOWQ_DIALOG or raises the existing
%      singleton*.
%
%      H = SETTINGS_FLOWQ_DIALOG returns the handle to a new SETTINGS_FLOWQ_DIALOG or the handle to
%      the existing singleton*.
%
%      SETTINGS_FLOWQ_DIALOG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETTINGS_FLOWQ_DIALOG.M with the given input arguments.
%
%      SETTINGS_FLOWQ_DIALOG('Property','Value',...) creates a new SETTINGS_FLOWQ_DIALOG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before settings_flowq_dialog_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to settings_flowq_dialog_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help settings_flowq_dialog

% Last Modified by GUIDE v2.5 16-Oct-2014 17:41:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @settings_flowq_dialog_OpeningFcn, ...
                   'gui_OutputFcn',  @settings_flowq_dialog_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before settings_flowq_dialog is made visible.
function settings_flowq_dialog_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to settings_flowq_dialog (see VARARGIN)


set(handles.text_resolution_xin,'String',num2str(varargin{1}.resolution(1)));
set(handles.text_resolution_yin,'String',num2str(varargin{1}.resolution(2)));
set(handles.text_resolution_zin,'String',num2str(varargin{1}.resolution(3)));
set(handles.text_resolution_tin,'String',num2str(varargin{1}.model_t_resolution));



% Choose default command line output for settings_flowq_dialog
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes settings_flowq_dialog wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = settings_flowq_dialog_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if ~numel(handles)
    varargout{1}=0;
else
    varargout{1} = handles.output;
end



% --- Executes on button press in pushbutton_apply.
function pushbutton_apply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

rx = str2double(get(handles.text_resolution_xin,'String'));
ry = str2double(get(handles.text_resolution_yin,'String'));
rz = str2double(get(handles.text_resolution_zin,'String'));
rt = str2double(get(handles.text_resolution_tin,'String'));

handles.resolution = [rx(end) ry(end) rz(end)]';
handles.model_t_resolution = rt;
guidata(hObject, handles);
uiresume(handles.figure1);


function text_resolution_xin_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_xin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_xin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_xin as a double


% --- Executes during object creation, after setting all properties.
function text_resolution_xin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_xin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_resolution_yin_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_yin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_yin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_yin as a double


% --- Executes during object creation, after setting all properties.
function text_resolution_yin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_yin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_resolution_zin_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_zin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_zin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_zin as a double


% --- Executes during object creation, after setting all properties.
function text_resolution_zin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_zin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function text_resolution_tin_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_tin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_tin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_tin as a double


% --- Executes during object creation, after setting all properties.
function text_resolution_tin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_tin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_xin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_xin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_xin as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_xin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_yin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_yin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_yin as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_yin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to text_resolution_zin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_resolution_zin as text
%        str2double(get(hObject,'String')) returns contents of text_resolution_zin as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_resolution_zin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
