% test_setting_tsmoothing_vp

input.t_smoothing.fourier.max_modes = 12;
input.t_smoothing.fourier.nmodes = 4;

input.t_smoothing.cbsplines.ncontrolpoints = 15;
input.t_smoothing.cbsplines.lambda = 0.1;
input.t_smoothing.cbsplines.mu = 0.0;

input.patient_info.heart_rate = 60;
input.t_smoothing.which_method = 1; % fourier only
Atin = 0.03;
Atout = 0.001;
input.t_smoothing.x = 0:Atin:0.95;
%input.t_smoothing.x(8:12)=[];
fr=2;
input.t_smoothing.y = sin(input.t_smoothing.x*2*pi*fr)+randn(size(input.t_smoothing.x))*0.1;

%input.sampling_t = 0:0.001:input.t_smoothing.x(end);
input.sampling_t = 0:Atout:60/input.patient_info.heart_rate;


setting_tsmoothing_vp(input);