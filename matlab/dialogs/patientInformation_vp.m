function varargout = patientInformation_vp(varargin)
% PATIENTINFORMATION_VP MATLAB code for patientInformation_vp.fig
%      PATIENTINFORMATION_VP, by itself, creates a new PATIENTINFORMATION_VP or raises the existing
%      singleton*.
%
%      H = PATIENTINFORMATION_VP returns the handle to a new PATIENTINFORMATION_VP or the handle to
%      the existing singleton*.
%
%      PATIENTINFORMATION_VP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PATIENTINFORMATION_VP.M with the given input arguments.
%
%      PATIENTINFORMATION_VP('Property','Value',...) creates a new PATIENTINFORMATION_VP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before patientInformation_vp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to patientInformation_vp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help patientInformation_vp

% Last Modified by GUIDE v2.5 29-Oct-2014 15:59:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @patientInformation_vp_OpeningFcn, ...
                   'gui_OutputFcn',  @patientInformation_vp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before patientInformation_vp is made visible.
function patientInformation_vp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to patientInformation_vp (see VARARGIN)

set(handles.text_HR,'String',num2str(varargin{1}.patient_info.heart_rate));
invHR = 60/varargin{1}.patient_info.heart_rate;
set(handles.text_invHR,'String',[ num2str(invHR) ' s']);
set(handles.text_str,'String',[ num2str(invHR/50) ' s']);
% timestep = varargin{1}.model_t_resolution;
% period = (ceil(invHR/timestep)-1)*timestep+timestep;
% set(handles.text_period,'String',[ num2str(period) ' s']);



% Choose default command line output for patientInformation_vp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes patientInformation_vp wait for user response (see UIRESUME)
 uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = patientInformation_vp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = 0;%handles.output;
