function selected = DICOMthumbnails(thumbnail,patient)

f = figure;
hTabGroup = uitabgroup(f); 
drawnow;

handles = guidata(gcf);

for p=1:numel(patient)
    tab(p) = uitab(hTabGroup, 'title',patient{p}.name);
    a = axes('parent', tab(p));
    nimages = numel(patient{p}.image);
    ncols = ceil(sqrt(nimages));
    nrows = floor(sqrt(nimages));
    if nrows*ncols < nimages
        nrows=ncols;
    end
    for i=1:nimages
        subplot(nrows,ncols,i); 
        if ndims(thumbnail{p,i})==4
            im= imagesc(thumbnail{p,i}(:,:,:,1));
        else
            im= imagesc(thumbnail{p,i}); 
        end
        axis off
        set(im,'HitTest','off');
        set(im,'Tag',num2str([p i]));
        set(im,'ButtonDownFcn',{@imageclicking,handles})
        set(im,'HitTest','on')
    end
    
end

uiwait

function [pat,imn]=imageclicking(gcbo,eventdata,handles)
    str = str2num(get(gcbo,'Tag'));
    pat = str(1);
    imn = str(2);
    disp(['Patient ' num2str(pat) ', image ' num2str(imn)])
    handles.pat = pat;
    handles.imn = imn;
    guidata(gcbo,handles);
    uiresume
end

handles = guidata(gcf);
close(f);

selected = [handles.pat,handles.imn];

end






