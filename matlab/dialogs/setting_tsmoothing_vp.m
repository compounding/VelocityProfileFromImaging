function varargout = setting_tsmoothing_vp(varargin)
% SETTING_TSMOOTHING_VP MATLAB code for setting_tsmoothing_vp.fig
%      SETTING_TSMOOTHING_VP, by itself, creates a new SETTING_TSMOOTHING_VP or raises the existing
%      singleton*.
%
%      H = SETTING_TSMOOTHING_VP returns the handle to a new SETTING_TSMOOTHING_VP or the handle to
%      the existing singleton*.
%
%      SETTING_TSMOOTHING_VP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETTING_TSMOOTHING_VP.M with the given input arguments.
%
%      SETTING_TSMOOTHING_VP('Property','Value',...) creates a new SETTING_TSMOOTHING_VP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setting_tsmoothing_vp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setting_tsmoothing_vp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setting_tsmoothing_vp

% Last Modified by GUIDE v2.5 04-Nov-2014 15:11:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @setting_tsmoothing_vp_OpeningFcn, ...
    'gui_OutputFcn',  @setting_tsmoothing_vp_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before setting_tsmoothing_vp is made visible.
function setting_tsmoothing_vp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setting_tsmoothing_vp (see VARARGIN)

%max_modes = varargin{1}.t_smoothing.fourier.max_modes;
max_modes = ceil(numel(varargin{1}.t_smoothing.x)/2)+1;
nmodes = varargin{1}.t_smoothing.fourier.nmodes;
handles.sampling_t = varargin{1}.sampling_t ;
ncontrolpoints = varargin{1}.t_smoothing.cbsplines.ncontrolpoints;
lambda = varargin{1}.t_smoothing.cbsplines.lambda;
mu = varargin{1}.t_smoothing.cbsplines.mu;
handles.which_method = varargin{1}.t_smoothing.which_method;
handles.sampling_time = varargin{1}.sampling_t;
handles.patient_info = varargin{1}.patient_info;
%set(handles.edit_lambda,'Value',lambda);
set(handles.edit_lambda,'String',num2str(lambda));
set(handles.edit_mu,'String',num2str(mu));

set(handles.popupmenu_smoothingType, 'Value',handles.which_method);

if handles.which_method==1 % fourier
    set(handles.uipanel_smoothingParameters_fourier,'Visible','on');
    set(handles.uipanel_splines,'Visible','off');
    p = get(handles.uipanel_smoothingParameters_fourier,'Position');
    set(handles.uipanel_smoothingParameters_fourier,'Position',[0.05 p(2) 0.9 p(4)]);
    
elseif handles.which_method==2 % bsplines
    set(handles.uipanel_smoothingParameters_fourier,'Visible','off');
    set(handles.uipanel_splines,'Visible','on');
    p = get(handles.uipanel_splines,'Position');
    set(handles.uipanel_splines,'Position',[0.05 p(2) 0.9 p(4)]);
    
end

% configure the sliders
% Fourier
set(handles.slider_nmodesfourier,'Max',max_modes,'Min',1,'Value',nmodes,'SliderStep',[1/(max_modes-1) 3/(max_modes-1)]);
set(handles.text_nmodesfourier,'String',num2str(nmodes));

max_cpoints = 40;
set(handles.slider_controlpoints,'Max',max_cpoints,'Min',1,'Value',ncontrolpoints,'SliderStep',[1/(max_cpoints-1) 3/(max_cpoints-1)]);
set(handles.text_controlpoints,'String',num2str(ncontrolpoints));
% Choose default command line output for setting_tsmoothing_vp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

axes(handles.axes1);
cla;

handles.x = varargin{1}.t_smoothing.x;
handles.y = varargin{1}.t_smoothing.y;
guidata(hObject, handles);
plot(handles.x, handles.y,'*k');

% This sets up the initial plot - only do when we are invisible
% so window can get raised using setting_tsmoothing_vp.
if strcmp(get(hObject,'Visible'),'off')
    plot(rand(5));
end

% UIWAIT makes setting_tsmoothing_vp wait for user response (see UIRESUME)
 uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = setting_tsmoothing_vp_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


handles.which_method = get(handles.popupmenu_smoothingType,'Value');
handles.ncontrol_points = get(handles.slider_controlpoints,'Value');
handles.lambda = str2double(get(handles.edit_lambda,'String'));
handles.mu = str2double(get(handles.edit_mu,'String'));
handles.nmodes = get(handles.slider_nmodesfourier,'Value');
guidata(hObject, handles);

% Get default command line output from handles structure
if ~numel(handles)
    varargout{1}=0;
else
    varargout{1} = handles;
end

% --- Executes on button press in pushbutton_apply.
function pushbutton_apply_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

guidata(hObject, handles);
uiresume(handles.figure1);

% --- Executes on selection change in popupmenu_smoothingType.
function popupmenu_smoothingType_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu_smoothingType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu_smoothingType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu_smoothingType
%contents = get(hObject,'String');
idx = get(hObject,'Value');
%contents{idx};
if idx==1
    set(handles.uipanel_smoothingParameters_fourier,'Visible','on');
    set(handles.uipanel_splines,'Visible','off');
    p = get(handles.uipanel_smoothingParameters_fourier,'Position');
    set(handles.uipanel_smoothingParameters_fourier,'Position',[0.05 p(2) 0.9 p(4)]);
elseif idx==2
    set(handles.uipanel_smoothingParameters_fourier,'Visible','off');
    set(handles.uipanel_splines,'Visible','on');
     p = get(handles.uipanel_splines,'Position');
    set(handles.uipanel_splines,'Position',[0.05 p(2) 0.9 p(4)]);
end

guidata(hObject, handles);
do_smoothing(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu_smoothingType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu_smoothingType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'Fourier smoothing', 'Cyclic B-splines'});


% --- Executes on slider movement.
function slider_nmodesfourier_Callback(hObject, eventdata, handles)
% hObject    handle to slider_nmodesfourier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

val = get(hObject,'Value');
if (val < 1)
    return
end   
set(handles.text_nmodesfourier,'String',num2str(val));
guidata(hObject, handles);
do_smoothing(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function slider_nmodesfourier_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_nmodesfourier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on slider movement.
function slider_controlpoints_Callback(hObject, eventdata, handles)
% hObject    handle to slider_controlpoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
val = get(hObject,'Value');
if (val < 1)
    return
end
set(handles.text_controlpoints,'String',num2str(val));
guidata(hObject, handles);
do_smoothing(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function slider_controlpoints_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_controlpoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit_lambda_Callback(hObject, eventdata, handles)
% hObject    handle to edit_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_lambda as text
%        str2double(get(hObject,'String')) returns contents of edit_lambda as a double
guidata(hObject, handles);
do_smoothing(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function edit_lambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_lambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_mu_Callback(hObject, eventdata, handles)
% hObject    handle to edit_mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_mu as text
%        str2double(get(hObject,'String')) returns contents of edit_mu as a double
guidata(hObject, handles);
do_smoothing(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit_mu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function do_smoothing(hObject, eventdata, handles)
which_method = get(handles.popupmenu_smoothingType,'Value');
x_smooth = handles.sampling_t;
y_smooth = x_smooth*0;

axes(handles.axes1);
cla;
plot(handles.x, handles.y,'*k');


if which_method==1 % Fourier
    nmodes = get(handles.slider_nmodesfourier,'Value');
    M = ceil((length(handles.y)+1)/2);
    Y = fft(handles.y);
    
    if ~mod(length(handles.x),2) && mod(length(x_smooth),2) || ...
            mod(length(handles.x),2) && ~mod(length(x_smooth),2) 
        Y2=fftshift(Y);
        Y2(end+1)=Y2(1);
        Y = ifftshift(Y2);
    end
    Yshifted = fftshift(Y);
    nmodes = min(nmodes, M);
    nzeros = M-nmodes;
    Yshifted([1:nzeros end-nzeros+1:end])=0;
    
    % padd with zeros
    
    npads = max([0 ceil((numel(x_smooth)-numel(Yshifted))/2)]);
    
    upsampling = numel(x_smooth)/numel(handles.x);
    Ypadded = padarray(Yshifted,[0 npads],0);
    y_smooth = ifft(ifftshift(Ypadded))*upsampling;
    
    % the band width will be
    fs =1/(handles.x(2)-handles.x(1)); %Hz
    Af=fs/length(handles.x);
    BW = fs/2 + Af*npads; % Hz
    new_fs = 2*BW;
    new_At = 1/new_fs;
    x_smooth = (0:numel(y_smooth)-1)*new_At;
     

elseif which_method==2 % Cyclic B-splines
    ncontrol_points = get(handles.slider_controlpoints,'Value');
    lambda = str2double(get(handles.edit_lambda,'String'));
    mu = str2double(get(handles.edit_mu,'String'));
    
    bounds = handles.x([1 end])';
    bsd = 3;
    rnth=0.25;    
    border = 0;
    spacing =(bounds(2)-bounds(1))/(ncontrol_points-1);
    
    control_points = createBSplineGrid(bounds, bsd, spacing(:), border,'cyclic',1,[0 60/handles.patient_info.heart_rate]);
    [B,removed_nodes] = createBSplineSamplingMatrix(handles.x(:), control_points,'od',1,'remove_empty_nodes',rnth,'cyclic',1);
    Adiv = createBSplineContinuousDerivativeSamplingMatrix(control_points,'od',1,'operator','grad','remove_empty_nodes',removed_nodes);

    tfull = abs(x_smooth(:)*ones(1,numel(handles.x)) - ones(numel(x_smooth),1)*handles.x(:)');
    tfull_min = min(tfull');
    regindices = find(tfull_min > (handles.x(2)-handles.x(1))/2);

    regpos = x_smooth(regindices)';
    S = createBSplineDerivativeSamplingMatrix(regpos, control_points,'bsd',bsd,'od',1,'remove_empty_nodes',removed_nodes,'operator','grad');
    A = B'*B+lambda/(1-lambda)*(S'*S)+mu/(1-mu)*(Adiv);

    B2 = createBSplineSamplingMatrix(x_smooth(:), control_points,'od',1,'remove_empty_nodes',removed_nodes,'cyclic',1);
    b = B'*handles.y(:);
    c = A\b;
    
    y_smooth = B2*c;
   
    xcontrol_points = ((1:control_points.size)-1)*control_points.spacing+control_points.origin;
    xcontrol_points(find(removed_nodes))=[];
    hold on;
    plot(xcontrol_points, xcontrol_points*0,'ok');
    if (lambda>0)
        plot(regpos,regpos*0,'g.')
    end
    hold off;
    
end


hold on;
plot(x_smooth, y_smooth,'.r');
hold off;
grid on;
xlabel('Time (s)')
ylabel('Velocity (mm/s)')

set(handles.text_maxflow,'String',num2str(max(y_smooth)));
set(handles.text_minflow,'String',num2str(min(y_smooth)))
set(handles.text_meanflow,'String',num2str(mean(y_smooth)))

set(handles.text_maxflow_o,'String',num2str(max(handles.y)));
set(handles.text_minflow_o,'String',num2str(min(handles.y)))
set(handles.text_meanflow_o,'String',num2str(mean(handles.y)))

