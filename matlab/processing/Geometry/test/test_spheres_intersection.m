% test_spheres_intersection


centre = [0 0 0; 0 2 3; 4 1 2]';

radiuses{1} = 14:0.5:14;
radiuses{2} = 14:0.5:14;
radiuses{3} = 14:0.5:14;

theta{1}=[-30 80];
theta{2}=[50 160];
theta{3}=[90 180];

colors = [0.8 0 0; 0 0.5 0 ; 0 0 0.5];

nspheres=size(centre,2);

for i=1:nspheres
    for j=1:numel(radiuses{i})
        sphere{i,j}.radius=radiuses{i}(j);
        sphere{i,j}.centre=centre(:,i);
    end
end

combinations = combnk(1:nspheres,2);

for i =1:size(combinations,1)
    for j=1:numel(radiuses{combinations(i,1)})
        for k=1:numel(radiuses{combinations(i,2)})
            out{i,j,k} = intersectionSphereSphere(sphere{combinations(i,1),j},sphere{combinations(i,2),k});
            if out{i,j,k}.radius==out{i,j,k}.radius
                m{i,j,k}  = circle3DMesh(out{i,j,k}.centre,out{i,j,k}.radius,out{i,j,k}.normal,'resolution',35,'theta',theta{i});
            else
                m{i,j,k}=NaN;
            end
        end
    end
end


% plot spheres
figure;

for i=1:nspheres
    for j=1:numel(radiuses{i})
        mesh = sphereMesh(sphere{i,j}.centre,sphere{i,j}.radius,'resolution',60,'theta',[50 160],'phi',[50 160]);
        mesh.imposePositiveOrientation();
        hold on;
        viewMesh(mesh,'opacity',0.9,'color',colors(i,:));
        hold off
    end
end


%figure;
axis equal
hold on;
for i =1:size(combinations,1)
    for j=1:numel(radiuses{combinations(i,1)})
        for k=1:numel(radiuses{combinations(i,2)})
            if m{i,j,k}==m{i,j,k}
                plotpoints(m{i,j,k}.points','Color',colors(i,:),'LineWidth',2);
            end
        end
    end
end

hold off;