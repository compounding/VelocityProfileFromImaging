function val = dBSpline(degree,x)
% val = DBSPLINE(degree, x)
%
% computes the value of the derivtive of the B-Spline of degree 'degree' on the point 'x',
% x can be a vector
% which must be normalised with respect to the bspline grid


switch degree
    case 1
        val = dB1(x);
    case 3
        val = dB3(x);
    otherwise
        disp('Bad BSpline degree');
        val=0*x;
end
end

%%
function val = dB1(x)
    val=0*x;
    
    i1 = find(x>=-1 & x<0);
    i2 = find(x>=0 & x<=1);

    val(i1) = 1;
    val(i2) = -1;

end
%%
% function val = dB2(k, x)
% switch k
%     case -1
%         val = x-1.0;
%     case 0
%         val=1-2*x;
%     case 1
%         val=x;
%     otherwise
%         %disp('ERROR WHEN COMPUTING BSPLINE DERIVATIVE OF DEGREE 2');
%         val=0*x;
% end
% end
%%
function val = dB3(x)
    
    val=0*x;
    
    i1 = find(x>=-2 & x<-1);
    i2 = find(x>=-1 & x<0);
    i3 = find(x>=0 & x<1);
    i4 = find(x>=1 & x<2);

    val(i1) = (x(i1).^2+4.*x(i1)+4)/2;
    val(i2) = -(3.*x(i2).^2+4.*x(i2))/2;
    val(i3) = (3.*x(i3).^2-4.*x(i3))/2;
    val(i4) = -(x(i4).^2-4.*x(i4)+4)/2;

end

% function val = dB5(k, x)
% switch k
%     case -2
%         val = x.^4/24; %rightmost
%     case -1
%         val =-(5*x.^4-4*x.^3-6*x.^2-4*x-1)/24;
%     case 0
%         val=(5*x.^4-8*x.^3-6*x.^2+4*x+5)/12;
%     case 1
%         val=-(5*x.^4-12*x.^3+12*x)/12;
%     case 2
%         val = (5*x.^4-16*x.^3+12*x.^2+8*x-10)/24;
%     case 3
%         val = -(x.^4-4*x.^3+6*x.^2-4*x+1)/24; % leftmost
%     otherwise
%         val=0*x;
% end
% %val = -val;
% %plot(2:0.1:3,dBSpline(3,-1,0:0.1:1)); hold on; plot(1:0.1:2,dBSpline(3,0,0:0.1:1));  plot(0:0.1:1,dBSpline(3,1,0:0.1:1));  plot(-1:0.1:0,dBSpline(3,2,0:0.1:1));hold off;
% end

