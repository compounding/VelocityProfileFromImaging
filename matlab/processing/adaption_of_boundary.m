% matching inflow boundary conditions from imaging to model

function fittedpoints = adaption_of_boundary(points, npts,varargin)

nOptions = size(varargin,2);

%%
% Argument reading

mode = 'Bsplines'; % or tps

if (nOptions > 0)
    i=1;
    while(i<=nOptions)
        
        if (strcmp(varargin{i},'mode'))
            mode= varargin{i+1};
        end
        i = i+1;
    end
end


%% 1. Fit a closed spline to the input contours


[polarpoints, cmodel, meanradius] = points_to_polar(points(1:2,:));
%points_model_ = points_to_cartesian(polarpoints_model,cmodel,meanradius);
 angles=(-180:361/npts:180);
if strcmp(mode,'Bsplines')
    
    % Fit a closed (periodic) B-spline to that
    border = 0;
    bsd = 3;
    rnth=0.01;
    bounds = [-180 180]';
    spacing=[60];
    %spacing=[30];
    rnth=0.0;
    lambda=0.00001;
    control_points = createBSplineGrid(bounds, bsd, spacing, border,'cyclic',true,[-180 180]);
    [B,removed_nodes] = createBSplineSamplingMatrix(polarpoints(1,:)', control_points,'od',1,'remove_empty_nodes',rnth,'cyclic',true);
    S = createBSplineContinuousDerivativeSamplingMatrix(control_points,'od',1,'operator','grad','remove_empty_nodes',removed_nodes);
    A = B'*B+lambda*S;
    b = B'*polarpoints(2,:)';
    c = A\b;
    
    B2 = createBSplineSamplingMatrix(angles', control_points,'od',1,'remove_empty_nodes',removed_nodes,'cyclic',true);
    
    values = B2*c;
    
elseif strcmp(mode,'tps')
    
    
    bounds = [-180 180]';
    
    c = TPS(polarpoints(1,:)',polarpoints(2,:)','lambda',0.0,'rbs','log',{},'cyclic',1,[-180 180]);
    values = calculate_TPS(polarpoints(1,:)',c,angles(:),'lambda',0.0,'rbs','log',{});
    
else
    disp('Available modes are Bsplines and tps');
    
end

% figure,
% plot(polarpoints(1,:), polarpoints(2,:),'o');
% hold on;
% plot(angles, values,'r.');
% hold off;

points_ = points_to_cartesian(polarpoints,cmodel,meanradius);
fittedpoints = points_to_cartesian([angles ; values'],cmodel,meanradius);




end

function [polarpoints,centroid, meanradius] = points_to_polar(points)

centroid = mean(points,2);
% calculate radius and angle
directions = points - centroid*ones(1,size(points,2));
radius = sqrt(sum((directions).^2));
meanradius =mean(radius);

vecs = directions./[radius ; radius];

angles = atan2d(vecs(2,:),vecs(1,:));

% acosd(dot(ref*ones(1,size(points,2)),vecs));
% angles = real(angles);

%%The following might be a problem

%if angles(1)<0
%     angles=angles-angles(1);
%     angles(1)=360;
% else
%     angles=angles-angles(1);
% end

% cp = cross([ref ;0]*ones(1,size(points,2)),[vecs ; zeros(1,size(points,2))]);
% angles(cp(3,:)<0)=360-angles(cp(3,:)<0);
%
% angles = angles + (360-angles(1));
% angles(1)=0;
% angles = angles.*sign(cp(3,:));

polarpoints=points;
polarpoints(1,:)=angles;
polarpoints(2,:)=(radius-meanradius);

end

function points = points_to_cartesian(polarpoints,centroid,meanradius)


x = (meanradius +polarpoints(2,:)).*cosd(polarpoints(1,:))+centroid(1);
y =  (meanradius + polarpoints(2,:)).*sind(polarpoints(1,:))+centroid(2);

points = [x ; y];

end