function out = convertToCartesianCoordinatesCentered(input,varargin)
% out = convertToCartesianCoordinatesCentered(input);
% out = convertToCartesianCoordinatesCentered(input, Options);
%
%   converts to cartesian coordinates an Spherical image whose point [0 0 0] in wc is
%   located at the beam source
%
%   options:
%       'resolution', [ax ay]   -> resolution of the xy plane, default
%       0.7 mm
%  update: division in chuncks to avoid out of memory problem


resolution = [0.7 0.7 0.7]';
MAX_CHUNK_SIZE = 50;
debug = false;
interpolationMode='NN';
frustumCoords = true;
for i=1:size(varargin,2)
    if (strcmp(varargin{i},'debug'))
        debug = true;
    elseif (strcmp(varargin{i},'interpolation'))
        interpolationMode= varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'noFrustumCoords'))
        frustumCoords= false;
        i=i+1;
    elseif (strcmp(varargin{i},'maxChunkSize'))
        MAX_CHUNK_SIZE = varargin{i+1};
        i=i+1;
    elseif (strcmp(varargin{i},'resolution'))
        resolution= varargin{i+1};
        i=i+1;
    end
    
end


if (debug)
    disp('Initialize image');
end
% Initialize spherical image
outputSpacing =  input.spacing;
outputSpacing(1:3)=resolution;


rmin = input.origin(3);
rmax = input.origin(3) + input.size(3)*input.spacing(3);

axmin = input.origin(1);
axmax = input.origin(1)+input.size(1)*input.spacing(1);
aymin = input.origin(2);
aymax = input.origin(2)+input.size(2)*input.spacing(2);

maxangle = max([abs(axmin) abs(axmax) abs(aymin) abs(aymax)]);
minangle = min([abs(axmin) abs(axmax) abs(aymin) abs(aymax)]);

widthx_top1 = rmax*sin(  min( [ abs(axmin*pi/180) pi/2])*axmin/abs(axmin));
widthx_top2 = rmax*sin(  min( [ abs(axmax*pi/180) pi/2])*axmax/abs(axmax));

if axmin>0
    widthx_bottom1 = rmin*sin(  min([ abs(axmin*pi/180) pi/2])*axmin/abs(axmin));
else
    widthx_bottom1=0;
end

if axmax>0
    widthx_bottom2 = rmin*sin(  min([ abs(axmax*pi/180) pi/2])*axmax/abs(axmax));
else
    widthx_bottom2=0;
end

widthx1=min([widthx_top1  widthx_bottom1]);
widthx2=max([widthx_top2  widthx_bottom2]);




if aymin~=0
    widthy_bottom1 = rmin*sin(  min( [abs(aymin*pi/180) pi/2])*aymin/abs(aymin));
    widthy_top1 = rmax*sin( min( [ abs(aymin*pi/180) pi/2])*aymin/abs(aymin));
else
    widthy_bottom1 = 0;
    widthy_top1 = 0;
end
if aymax~=0
    widthy_bottom2 = rmin*sin(  min(  [abs(aymax*pi/180) pi/2])*aymax/abs(aymax));
    widthy_top2 = rmax*sin(min( [abs(aymax*pi/180) pi/2])*aymax/abs(aymax));
else
    widthy_bottom2 = 0;
    widthy_top2 = 0;
end
widthy1=min([widthy_top1  widthy_bottom1]);
widthy2=max([widthy_top2  widthy_bottom2]);

maxz=0;
if (maxangle<90)
    minz = rmin*cos(maxangle*pi/180);
    if (axmin*axmax<0 || aymin*aymax<0)
        maxz = rmax;
    else
        maxz = rmax*cos(minangle*pi/180);
    end
else
    disp('WARNING: one angle is larger than 180. results in cartesian coordinates  might be wrong');
end


% %------------------------------------------------------------------
% input_bounds = input.GetBounds();
% 
% axmin = input_bounds(1);
% axmax= input_bounds(2)+input.spacing(1);
% aymin = input_bounds(3);
% aymax= input_bounds(4)+input.spacing(2);
% rmin = input_bounds(5);
% rmax = input_bounds(6)+input.spacing(3);
% 
% maxangle = max(abs([axmin axmax aymin aymax]));
% minangle = min(abs([axmin axmax aymin aymax]));
% 
% widthx_top = rmax*sin(min( [ abs([axmin axmax]);  90 90 ]).*sign([axmin axmax]) *pi/180);
% widthx_bottom = rmin*sin(min( [ abs([axmin axmax]);  90 90 ]).*sign([axmin axmax]) *pi/180);
% 
% widthx(1)=min([widthx_top(1) widthx_bottom(1)]);
% widthx(2)=max([widthx_top(2) widthx_bottom(2)]);
% 
% widthy_top = rmax*sin(min( [ abs([aymin aymax]);  90 90 ]).*sign([aymin aymax]) *pi/180);
% widthy_bottom = rmin*sin(min( [ abs([aymin aymax]);  90 90 ]).*sign([aymin aymax]) *pi/180);
% 
% widthy(1)=min([widthy_top(1) widthy_bottom(1)]);
% widthy(2)=max([widthy_top(2) widthy_bottom(2)]);
% 
% 
% if (maxangle<90)
%     
%     minz = rmin*cos(maxangle*pi/180);
%     if axmin*axmax<0 || aymin*aymax<0
%         maxz = rmax;
%     else
%         maxz = rmax*cos(minangle*pi/180);
%     end
% else
%     disp('Warning: one angle is larger than 180. results in convertoCartesianCentered might be wrong');
%     %height = rmax*(1+abs(cos(biggest_angle*pi/180)));
%     %sizeCartesian(3)=2*input.size(3);
%     %originAtBeamSourceCartesian(3) = -height/2;
% end

sizeCartesian= [ceil((widthx2-widthx1)/outputSpacing(1))
             ceil((widthy2-widthy1)/outputSpacing(2))
             ceil((maxz-minz)/outputSpacing(3))];
%sizeCartesian=ceil([ widthx(2)-widthx(1) widthy(2)-widthy(1) maxz-minz]'./outputSpacing(1:3));
output_size = input.size;
output_size(1:3)=sizeCartesian;

outputOrigin = input.origin;
outputOrigin(1:3) = [widthx1 widthy1 minz]';
%outputOrigin([1 2 3]) = [widthx(1) widthy(1) minz]';


out = ImageType(output_size, outputOrigin, outputSpacing, eye(numel(input.size)));

% fill the data
if (debug)
    disp('Fill the data');
end

NCHUNKS = ceil(prod(out.size)/(MAX_CHUNK_SIZE^numel(out.size)));


chunked_size = ceil(prod(out.size)./NCHUNKS)';

h=waitbar(0,'Convert to Cartesian coordinates');
intervals = 0:NCHUNKS(1)-1;
for i=1:numel(intervals)
    ranges(1) = intervals(i).*chunked_size+1;
    ranges(2) = min([(intervals(i)+1).*chunked_size ; prod(out.size)]);
    % generate all the indexes of the target image where the image is non
    
    outputPositionsCartesian = out.GetPosition(ranges(1):ranges(2));
    
    if frustumCoords
        outputPositionsSpherical = pointFromCartesianToSphericalFrustum(outputPositionsCartesian);
    else
        outputPositionsSpherical = pointFromCartesianToSpherical(outputPositionsCartesian);
    end
    
    clear outputPositionsCartesian ;
    outputValues = input.GetValue(outputPositionsSpherical,interpolationMode);
    clear outputPositionsSpherical;
    %out.data(ranges(1):ranges(2),ranges(3):ranges(4),ranges(5):ranges(6)) = reshape(outputValues,ranges_size);
    out.data(ranges(1):ranges(2)) = outputValues;
    clear outputValues;
    waitbar(i/numel(intervals));
end
close(h);
% crop image to make it as small as possible
%out= cropImage(out, out.GetBounds(0.99));


end