function p = pointFromSphericalToCartesianFrustum(pin)
% p = pointFromSphericalToCartesian(pin)
% 
% pin is a 3 X N matrix containing points in centered spherical coordinates
%r = [0, rmax]
% phi = [-pi/2,pi/2]
% theta = [-pi,pi]
% That's why the pi-phi in the phi angle

%     phi = pin(1,:).*pi/180;
%     theta = pin(2,:).*pi/180;
%     r = pin(3,:); % depth (radius) along z
% 
% 
%     x = r.*cos(theta).*sin(phi);
%     y = r.*sin(theta).*cos(phi);
%     z = r.*cos(theta).*cos(phi);
%       
%       
% % from Ji2011
% 
% %     z = r.*sin(theta);
% %     x = r.*cos(phi);
% %     y = r.*cos(theta);


% This is equal to the cpp version
    theta = pin(1,:).*pi/180;
    phi = pin(2,:).*pi/180;
    r = pin(3,:); % depth (radius) along z

    x = r.*sin(theta);
    y = r.*sin(phi);
    z = r.*sqrt(1-sin(theta).*sin(theta)-sin(phi).*sin(phi));

    p = pin;
    p(1:3,:)=[x(:) y(:) z(:)]';
    
end