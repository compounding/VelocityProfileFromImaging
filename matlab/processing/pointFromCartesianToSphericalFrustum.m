function p = pointFromCartesianToSphericalFrustum(pin)
% p = pointFromCartesianToSpherical(pin)
% 
% pin is a 3 X N matrix containing points in centered cartesian coordinates

    x = pin(1,:);
    y = pin(2,:);
    z = pin(3,:);
    
    r =sqrt(x.*x+y.*y+z.*z);
    phi = atan2(y,z)*180/pi;
    theta = atan2(x,z)*180/pi;    
    
    phi(r<0.001)=0;
    theta(r<0.001)=0;
    
    p = pin;
    p(1:3,:)=[theta(:) phi(:) r(:)]';
    
end

%surface_points(sp_counter,:) = [sin(ax) cos(ax)*sin(ay) cos(ax)*cos(ay) ];
%surface_points(sp_counter,:) = [sin(ax)*cos(ay) cos(ax)*sin(ay) sqrt(1-cos(ax).^2*sin(ay).^2-cos(ay).^2*sin(ax).^2) ];