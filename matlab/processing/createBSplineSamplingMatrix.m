function [Ss,nodes_to_remove_] = createBSplineSamplingMatrix(positions,gr,varargin)
% Ss = buildBSplineSamplingMatrix(positions,gr,varargin)
%   positions must be  a N x K array, where N is the number of points and K
%   the input dimensionality
%
% This function works with any input dimensionality and any output
% dimensionality. The latter is to be passed as an argument.
%
% When 'remove_empty_nodes' is used, the next argument can be of two types:
%   - a scalar value, between 0 and 1: normalised threshold
%   - a row vector with one element per gr node, with '0' if the column
%   is to be preserved and '1' if it is to be removed

bsd=3;
output_dimensionality = 1; % by default, scalar function
reorder = false; % only makes sense if output dimensionality >=2
remove_empty_nodes=false;
nodes_to_remove_=[];
iscyclic=-1;
% read args
i=1;
cyclic_range = [NaN NaN];
while (i <= size(varargin,2))
    if (strcmp( varargin{i} , 'bsd'))
        bsd = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'od'))
        output_dimensionality = varargin{i+1};
        i = i+1;
    elseif (strcmp( varargin{i} , 'remove_empty_nodes'))
        remove_empty_nodes = true;
        remove_empty_nodes_th= varargin{i+1};
        i = i+1;
    elseif(strcmp( varargin{i} , 'reorder'))
        reorder= varargin{i+1};
    elseif (strcmp(varargin{i},'cyclic'))
        iscyclic = varargin{i+1};
        i=i+1;
    end
    i = i+1;
end

if iscyclic(1)>-1
    cyclic_range= [gr.origin(iscyclic) gr.origin(iscyclic)+gr.size(iscyclic).*gr.spacing(iscyclic)];
end

input_dimensionality = size(positions,2);

% create the neighbourhood kernel

m=0-ceil(bsd/2);
M=floor(bsd/2);

% the following is equivalent to this:
% [x, y, z]=ndgrid(m:M,m:M,m:M);grpos = [x(:) y(:) z(:)]; clear x y z;
str1='';
str2='';
str3='';
str4='';
for i=1:input_dimensionality
    str1=[str1 'm:M,'] ;
    str2=[str2 'x' num2str(i) '(:) '] ;
    str3=[str3 'x' num2str(i) ',']    ;
    str4=[str4 'x' num2str(i) ' ']    ;
end
if input_dimensionality==1
     eval(['[' str3(1:end-1) ']=ndgrid(' str1(1:end-1) ',1);grpos = [' str2(1:end-1) ']; clear ' str4 ';' ]);
else
    eval(['[' str3(1:end-1) ']=ndgrid(' str1(1:end-1) ');grpos = [' str2(1:end-1) ']; clear ' str4 ';' ]);
end
clear str1 str2 str3 str4;

%get indexes of this positions with respect to the B-Spline grid
gr_positions= gr.GetContinuousIndex(positions')'; 

%Build matrix Ss (spline sampling)
%first segment (1 band of the matrix)
ncols = prod(gr.size); % number of grpoints
nrows = size(positions,1); % number of input points, equal to npts


rows = uint32([]); % this might save a bit of memory
cols = uint32([]);
s = [];

for node=1:size(grpos,1)
    
     % columns: 1 per current grpoint
     % calculate the position with respect to the grpoint of interest
     gr_nodes = gr_positions - floor(gr_positions)  + ones(size(gr_positions,1),1)*grpos(node,:); 
     % Now, in the followin line, I have to provide the appropriate
     % associated grid node taking into account the cyclic nature of
     % B-splines
     associated_gr_nodes = floor(gr_positions - ones(size(gr_positions,1),1)*grpos(node,:));% The minus sign there is weird!!!!!
     if iscyclic(1)>0
         for i=iscyclic
            associated_gr_nodes(:,i) = ...
             floor(mod(gr_positions(:,i) - ones(size(gr_positions,1),1)*grpos(node,i),gr.size(i)))+1; % plus one because nodes start at 1 here
         end
     end
     toremove =[];
    for i=1:input_dimensionality
        s_(i,:) = BSpline(bsd,gr_nodes(:,i)'); 
        toremove = [toremove  find(s_(i,:)==0 )];
        if numel(toremove)
            %disp('Warning: some points were out of the gr. This should not happen if the gr totally encloses input data.')
            
        end
    end
    minimums= min(associated_gr_nodes);
    maximums = max(associated_gr_nodes);
    if numel(find(minimums<1)) || numel(find(gr.size(1:input_dimensionality)'-maximums<0))
        disp('Warning: some points were out of the gr. This is not necesarily bad but this should not happen if the gr totally encloses input data')
        [rmin,~]=find(associated_gr_nodes<1);
        if gr.size(2)==1
             [rmax,~]=find(ones(size(associated_gr_nodes,1),1)*gr.size(1)-associated_gr_nodes<0);
        else
            [rmax,~]=find(ones(size(associated_gr_nodes,1),1)*gr.size'-associated_gr_nodes<0);
        end
        toremove = [toremove rmin(:)' rmax(:)'];
    end   
    
    toremove = unique(toremove);
    topreserve = setdiff(1:size(gr_nodes,1),toremove);
    s_ = s_(:,topreserve);
    s = [s prod(s_,1)]; % row array
    clear s_;
    
   
    % the following is equivalent to this:
    str1='';
    for i=1:input_dimensionality
        str1=[str1 'associated_gr_nodes(topreserve,' num2str(i) '),'] ;
    end
    
    
    eval(['gr_indices = uint32(sub2ind(gr.size'', ' str1(1:end-1) '));']); % 1D indices of the nodes, column vector
    clear str1 gr_nodes;
    
    cols = [cols  gr_indices']; % row vector
    clear gr_indices;
    % rows: 1 per point
    
    rows = [rows topreserve]; % row vector
end
clear floor_continuous_indexes gr_positions;

if output_dimensionality>1 && reorder
    % make it more diagonal by reordering rows to put x y z of each point
    % together
    
    % the following is equivalent to this:
    % totalrows = [(rows-1)*3+1  (rows-1)*3+2 (rows-1)*3+3];
    % totalcols = [(cols-1)*3+1  (cols-1)*3+2 (cols-1)*3+3];
    str1='';
    str2='';
    for i=1:output_dimensionality
        str1=[str1 '(rows-1)*' num2str(output_dimensionality) '+' num2str(i) ' '] ;
        str2=[str2 '(cols-1)*' num2str(output_dimensionality) '+' num2str(i) ' '] ;
    end
    eval(['totalrows = [' str1(1:end-1) ']; ']);
    eval(['totalcols = [' str2(1:end-1) '];']);
    clear str1 str2;
else
    
    % the following is equivalent to this:
    % totalrows = [rows  rows+nrows  rows+2*nrows];
    % totalcols = [cols  cols+ncols  cols+2*ncols];
    str1='';
    str2='';
    for i=1:output_dimensionality
        str1=[str1 'rows+' num2str(i-1) '*nrows '] ;
        str2=[str2 'cols+' num2str(i-1) '*ncols '] ;
    end
    eval(['totalrows = [' str1(1:end-1) ']; ']);
    eval(['totalcols = [' str2(1:end-1) '];']);
    clear str1 str2;
end
clear rows cols;

% the following is equivalent to this:
% Ss = sparse(double(totalrows),double(totalcols) ,[ s s s] ,3*nrows,3*ncols) ;
str1='';
for i=1:output_dimensionality
    str1=[str1 's '] ;
end
eval(['Ss = sparse(double(totalrows),double(totalcols) ,[' str1(1:end-1) '] , ' num2str(output_dimensionality) '*nrows, ' num2str(output_dimensionality) '*ncols) ;']);
clear str1 s totalrows totalcols;

if remove_empty_nodes
   if numel(remove_empty_nodes_th)>1
       nodes_to_remove = find(remove_empty_nodes_th);
   else
       normalised_aggregated_node_value = (sum(Ss,1)/max(sum(Ss,1))).^(1/output_dimensionality);
       nodes_to_remove = find(normalised_aggregated_node_value<=remove_empty_nodes_th);
   end
   nodes_to_remove_ = zeros(1,size(Ss,2));
   Ss(:,nodes_to_remove)=[];   
   nodes_to_remove_(nodes_to_remove)=1;
   
end


end