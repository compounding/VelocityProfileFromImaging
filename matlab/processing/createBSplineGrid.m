function outgrid = createBSplineGrid(bounds,bsd,patch_spacing, border,varargin)
% function outgrid = createBSplineGrid(bounds,bsd,patch_spacing, border,varargin)
dbg = false;
reduced=false;

iscyclic=-1; % wether the grid is cyclic and along which dimension(s)
cyclic_range = [0 1]; % this should be a N x 2 matrix, where N is the number of dimensions which are cyclic

for i=1:size(varargin,2)
    if (strcmp(varargin{i},'reduced'))
        reduced=true;
    elseif (strcmp(varargin{i},'debug'))
        dbg = true;
    elseif (strcmp(varargin{i},'cyclic'))
        iscyclic = varargin{i+1};
        cyclic_range= varargin{i+2};
        i=i+2;
    end
end


%epsilon=min(patch_spacing/100);
epsilon=0;

if ~reduced
    leftborder = ceil((bsd-1)/2) + border;
    rightborder = ceil((bsd-1)/2) + border;
else
    leftborder =0;
    rightborder=0;
end

bounds = bounds(:);
ndims_ = numel(bounds)/2;

patch_size = ceil((bounds(2:2:numel(bounds))-bounds(1:2:numel(bounds))+epsilon)./patch_spacing) +1+ leftborder+rightborder;
patch_origin = ((bounds(2:2:numel(bounds))+bounds(1:2:numel(bounds))-epsilon)/2)-((patch_size-1)/2).*patch_spacing;

if iscyclic(1)>-1
    patch_size(iscyclic) = ceil((cyclic_range(:,2)-cyclic_range(:,1))./patch_spacing(iscyclic));
    patch_spacing(iscyclic) = (cyclic_range(:,2)-cyclic_range(:,1))./patch_size(iscyclic);
    patch_origin(iscyclic) =cyclic_range(:,1);
end

% Should I change this for a periodic image type?
outgrid = ImageType(patch_size,patch_origin,patch_spacing,eye(ndims_));

end