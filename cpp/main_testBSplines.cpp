
/**
  * This is similar to the 3D reconstruction but also over time. Time is treated as just another dimension,
  * however it is imposed periodic. This is achieved by extending input data up to 2 b-spline grid points on
  * each side along the time direction (or is it just 1?). Also, temporal resolution is also increased over
  * the multiple scales is not divided by 2, instead it goes from 4 control points to 10 (maybe change this,
  * we will see). Another difference is that temporal variation can be penalised. For starters, we don't
  * really do this.
  *
  * (c) Alberto Gomez 2014 - King's College London
  */


/// Miscelanea
#include <iostream>
#include <vector>
#include <algorithm>

/// VTK headers
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataNormals.h>

/// ITK headers
#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkResampleImageFilter.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>

/// Smart Pointers from boost library
#include <boost/shared_ptr.hpp>

/// Internal includes
#include "BsplineGrid.hxx"
#include "LinearSolvers.hxx"

/// Internal includes from namespace echo
#include "Image_tools.hxx"
#include "Mesh/Mesh_tools.hxx"
#include "Point_tools.hxx"
#include "String_tools.hxx"

// parallel computing
/// Macros for the maximun value out of three scalars or the three components of one vector
#define MAX2(x,y,z) std::max( std::max(x,y),z  )
#define MAX2_V(x) std::max( std::max(x[0],x[1]),x[2]  )
#define MAX2_V_abs(x) std::max( std::max( std::abs(x[0]), std::abs(x[1])), std::abs(x[2])  )


void usage(); /// Print the correct usage if requested by the user or if arguments are wrong
void read_arguments(int argc, char* argv[]);
void display_arguments();

template <typename  ImageType, typename BsplineGridType>
void replaceVectorImagesWithError(typename ImageType::Pointer image, typename BsplineGridType::Pointer control_points, vtkSmartPointer<vtkMatrix4x4> matrix, bool inverse, int j);

// Variables that are accessed by many methods

/// misc
const bool KEEP_SMALL_PATCHES = true; /// If this is on, patches are adaptively subdivided
/// For mac
//const int MAX_COLS_PER_PATCH = 25000; /// For the A matrix
//const int MAX_POINTS_PER_PATCH = 1E+07; /// For the B matrix

/// For linux
int MAX_COLS_PER_PATCH = 10000; /// For the A matrix
int MAX_POINTS_PER_PATCH = 2.5E+05; /// For the B matrix
float MAX_MEMORY = -1;

const double CMS_TO_MS = 1E-02;
const double MS_TO_CMS = 100.0;
const unsigned int NDIMS=2;
const unsigned int OUTPUT_DIMENSIONS=2;
const double MAX_ERROR=1E-05;
const double B_TO_MB = 1/1024.0/1024.0;
const double GB_TO_MB = 1024.0;
unsigned int number_of_patches;
const double ZERO=1E-10;
double spacing_reduction_factor_space[] = {2.0,2.0,2.0,1.2};
double minimum_grid_size[] = {-1,-1,-1,-1};
bool isCartesian =false;
bool fitData =false;
bool writeErrors = false;
/// mandatory arguments
int n_images;
std::string reference_image_filename("");
std::string input_image_flename;
std::string extra("");
std::string input_motion_mesh_config_file;
std::string output_prefix;
/// default arguments
bool debug = false;
bool parallel_on = false;
int threads_to_use = 2;
bool verbose = false;
unsigned int border=0;
unsigned int bsd=3;
unsigned int max_levels =3;
unsigned int points_between_two_nodes=2;
double user_bounds[2*NDIMS];
double user_spacing[NDIMS];

double image_downsampling[4] = {-1, -1, -1, -1};

bool cyclictime = false;
double cycle_time[2];

bool allLevels=false;
bool use_user_bounds = false;
double th_velocity = -1;
double lambda = 0;
double remove_nodes_th = 0.0;
bool use_extra_components = false;
bool v_in_mps=false;
double wall_velocity_factor=1.0;
std::string wall_velocity("wall_velocity");
int initialLevel = 0;
std::vector <std::string> initial_coeffs_filename;


// Define types

typedef unsigned short BModePixelType;
typedef itk::Image<BModePixelType,NDIMS> BModeImageType;
typedef float DopplerPixelType;
typedef itk::Image<DopplerPixelType,NDIMS> DopplerImageType;

typedef itk::Image<itk::Vector<DopplerPixelType, OUTPUT_DIMENSIONS>, NDIMS> VectorImageType;

typedef BsplineGrid<float,NDIMS ,OUTPUT_DIMENSIONS> BsplineGridType;
typedef BsplineGridType::PointType PointType;
typedef BsplineGridType::CoefficientType VectorType;
std::vector <PointType > beam_sources;

typedef echo::Point<BsplineGridType::MatrixElementType,1> ValueType;


int main(int argc, char* argv[])
{

    user_spacing[0]= -1;
    /****************************************************************************
    *                               ARGUMENT READ                               *
    ****************************************************************************/
    read_arguments(argc, argv);

    if (debug){
        std::cout << "General information of arguments:"<<std::endl;
        display_arguments();
    }

    Eigen::setNbThreads(threads_to_use);
    Eigen::initParallel();

    /****************************************************************************
    *                               READ INPUT IMAGES                           *
    ****************************************************************************/

    std::cout << "Read inputs"<<std::endl;
    VectorImageType::Pointer vector_image; /// only used if fitData activated

    typedef itk::ImageFileReader<VectorImageType> VectorReaderType;

    VectorReaderType::Pointer vectorReader = VectorReaderType::New();
    vectorReader->SetFileName(input_image_flename);
    vectorReader->Update();

    if (image_downsampling[0]>0 || image_downsampling[1]>0 || image_downsampling[2]>0 || image_downsampling[3]>0){
        /// TODO
        std::cout << "WARNING: downsample for vector images to be implemented"<<std::endl;
    } else {
        vector_image = vectorReader->GetOutput();
    }

    /****************************************************************************
    *                               CALCULATE EXTENT                            *
    ****************************************************************************/
    double bounds[2*NDIMS];

    vtkSmartPointer<vtkMatrix4x4> eyeMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
    eyeMatrix->Identity();


    { /// Keep scope to save memory

        std::vector< PointType > image_points;
        std::vector<ValueType> doppler_values;
        std::vector< PointType > vector_points;
        std::vector<VectorType> vector_values;

        /****************************************************************************
            *                    Convert image to a list of points                      *
            ****************************************************************************/
        if (debug) std::cout << "\t\tConvert to cartesian transformed points"<<std::endl;


        echo::VectorImageToPointDataset<VectorImageType, PointType, VectorType>(vector_image,eyeMatrix, vector_points, vector_values);

        if (debug) std::cout << "\t\tGet bounds of the input data"<< std::endl;
        echo::getBounds<PointType >(vector_points, &(bounds[0]));
    }

    /****************************************************************************
     *                 INITIALISE GRID FOR COARSER SCALE                        *
     ****************************************************************************/


    /// Create the grid
    /// The original grid spacing is such that there will be 4 grid points within the domain.
    /// This, for the experiments, seemend like a reasonable starting point. Along the time direction,
    /// we will start by 4 points and, for the time being stay there

    BsplineGridType::PointType  grid_spacing, previous_spacing;
    if (user_spacing[0]<0)
    {
        BsplineGridType::PointType maximum_grid_spacing;
        for (int j=0; j<NDIMS; j++)
        {
            maximum_grid_spacing[j]=(bounds[2*j+1]-bounds[2*j])/std::pow(2.0,1);
            grid_spacing[j]=maximum_grid_spacing[j];
        }
    }
    else
    {
        extra = extra + "_gs";
        for (int j=0; j<NDIMS; j++)
        {
            extra = extra + "-" + echo::str::toString<double>(user_spacing[j]);
            grid_spacing[j]=user_spacing[j];

        }
        if (debug) std::cout << "Spacing set by the user to "<< grid_spacing<<std::endl;
    }

    std::vector<BsplineGridType::Pointer> multilevel_grid;


    /****************************************************************************************************************
     *                                    LAUNCH THE MULTI-SCALE CALCULATION                                        *
     ****************************************************************************************************************/
    if (debug)
    {
        std::cout << "Start the loop over "<< max_levels<< " scales"<<std::endl;
    }

    unsigned int nlevels = 0;
    double meanAbsError = INFINITY;

    BsplineGridType::PointType spacing_reduction_factor;
    for (int j=0; j<NDIMS; j++){
        if (minimum_grid_size[j]>0 && max_levels >1){
            spacing_reduction_factor[j] = std::pow(grid_spacing[j]/minimum_grid_size[j],1.0/( (double) max_levels-1));
        } else {
            spacing_reduction_factor[j]=spacing_reduction_factor_space[j];
        }
    }

    previous_spacing = grid_spacing*spacing_reduction_factor;


    /********************************************************************************************************************************
     *                                               INITIALISE THE MULTI-SCALE CALCULATION                                          *
     *********************************************************************************************************************************/

    while( meanAbsError>MAX_ERROR)
    {
        nlevels++;
        if (nlevels > max_levels )
        {
            nlevels--;
            if (debug)
            {
                std::cout << "Maximum level ("<< max_levels<<") attained, exit loop"<<std::endl;
            }
            break;
        }

        if (debug)
        {
            std::cout << "\tProcessing for level: "<< nlevels<<std::endl;
        }

        ///---------------------------------------------------------------------------//
        ///           CREATE GRID WITH A SPACING HALF THE PREVIOUS ONE                //
        ///---------------------------------------------------------------------------//

        grid_spacing=previous_spacing/spacing_reduction_factor;
        if (debug) std::cout << "\t\tB-spline grid spacing at this level "<< grid_spacing<<std::endl;

        BsplineGridType::Pointer control_points(new BsplineGridType(bounds, bsd, grid_spacing, 0)); /// the control point grid does not need border. This grid covers the whole ROI and there is no patch division.
        control_points->SetDebug(debug);
        if (debug) std::cout << "\t\tUsing "<< threads_to_use<< " threads."<<std::endl;
        control_points->SetParallel(parallel_on, threads_to_use);



        PointType downsampling_factor_vector;
        BsplineGridType::Pointer patch(control_points); /// I am copying the reference so there are two reference but only one object!

        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        ///                             SET UP INPUT DATA                             //
        ///~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

        std::vector< PointType > image_points(0);
        std::vector< VectorType > vector_values(0);

        if (debug) std::cout << "\t\tDownsampling input data ";
        PointType control_point_spacing(&(control_points->GetSpacing()[0]) );
        for (int k=0; k< NDIMS; k++){
            downsampling_factor_vector[k]=std::max(1.0,control_point_spacing[k]/points_between_two_nodes/vector_image->GetSpacing()[k]);
            if (debug) std::cout << downsampling_factor_vector[k] <<" ";
        }
        if (debug) std::cout << std::endl;


        std::vector< PointType > points_current;
        std::vector< VectorType > vector_values_current;

        if (downsampling_factor_vector==1){
            echo::VectorImageToPointDataset<VectorImageType, PointType, VectorType>(vector_image,eyeMatrix, points_current, vector_values);
        } else {
            VectorImageType::Pointer downsampled_vector_image = echo::downsampleVector<VectorImageType>(vector_image, &(downsampling_factor_vector[0]), echo::linear);
            echo::VectorImageToPointDataset<VectorImageType, PointType, VectorType>(downsampled_vector_image,eyeMatrix, points_current, vector_values);
        }


        image_points.insert( image_points.end(), points_current.begin(), points_current.end() );
        vector_values.insert( vector_values.end(), vector_values_current.begin(), vector_values_current.end() );


        if (debug) std::cout << "\t\tDownsampled data has "<< image_points.size()<< " points and "<<vector_values.size()<< " vectors"<<std::endl;

        ///---------------------------------------------------------------------------//
        ///                       FIND GRID COEFFICIENTS                       //
        ///---------------------------------------------------------------------------//

        echo::Point<double,2*NDIMS> patch_bounds;
        patch->GetBorderBoundsOfInfluence(&patch_bounds[0]);


        if (debug) std::cout << "\t\tBounds are "<< patch_bounds<<std::endl;

        BsplineGridType::DenseVectorType values_vector(2*vector_values.size(),1);
        std::vector<VectorType>::const_iterator const_iter;
        BsplineGridType::MatrixElementType *eigen_iter;

        if (debug) std::cout << "\t\tCopy "<< values_vector.rows() <<" values "<<std::endl;
        for (const_iter = vector_values.begin(), eigen_iter= values_vector.data();const_iter!=vector_values.end();  ++const_iter)
        {
            for (int nd = 0; nd< NDIMS; nd++){
                (*eigen_iter)=(*const_iter)[nd];
                ++eigen_iter;
            }
        }

        if (debug) std::cout << "\tCreate Matrices "<< std::endl;
        BsplineGridType::SparseMatrixType A;
        BsplineGridType::DenseVectorType b;
        std::vector<unsigned int> kept_nodes;
        std::vector<unsigned int> corresponding_nodes;
        {
            /// Keep environment to save memory

            BsplineGridType::SparseMatrixType B = patch->createSamplingMatrix(image_points, remove_nodes_th, kept_nodes, corresponding_nodes);

            { /// Keep scope to save memory
                Eigen::setNbThreads(8);
                A = B.transpose() *B;
                b = B.transpose()*values_vector;
            }

        } /// End of environment

        if (lambda)
        {
            if (debug) std::cout << "\tUse regularisation with lambda ="<<lambda<< std::endl;
            BsplineGridType::SparseMatrixType Adiv = patch->createContinuousDivergenceSamplingMatrix(kept_nodes,corresponding_nodes);
            A = A+lambda/(1-lambda)*Adiv;/// See notes on p118 of my thesis for an explanation
        }


        if (debug) std::cout << "\tCompute coefficients in this patch"<< std::endl;
        /// Available additional solvers
        /// BiCGSTAB + IncompletLUT preconditioner
        /// SparseQR
        /// SPQR
        /// PastixLU

        BsplineGridType::DenseVectorType x;//,x2,x3,x4;
        echo::solvers::ConfigurationParameter config;
        config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
        config.tol=1E-06;
        config.verbose = true;
        std::string str_solver;

        /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
        if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
        echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
        /// This can happen if there is a region with very densely populated points
        //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
        //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

        if (debug) std::cout << "\tCoefficients computed, replace patch values with the calculated coefficients"<< std::endl;
        /// Reorganize x into an array of coefficients, taking into account the reordering

        patch->setAll<BsplineGridType::DenseVectorType>(x,  kept_nodes);

        if (debug) std::cout << "Put all patch together into the full grid at level "<< nlevels<< std::endl;

        control_points->SetBlock(patch);

        std::string coefficients_filename = output_prefix +  std::string("_coefficients_level") + echo::str::toString<unsigned int>(nlevels)
                +  std::string("_lambda") +  echo::str::toString<double>(lambda)
                +  std::string("_b") +  echo::str::toString<unsigned int>(border)
                +  std::string("_pb2n") +  echo::str::toString<unsigned int>(points_between_two_nodes)
                +  std::string("_thv") +  echo::str::toString<double>(th_velocity)
                +  std::string("_thrn") +  echo::str::toString<double>(remove_nodes_th)
                +  std::string("_bsd") +  echo::str::toString<unsigned int>(bsd) + extra
                +std::string(".mhd");
        if (debug) std::cout << "Write coefficients at level  "<< nlevels<< " to " << coefficients_filename << std::endl;
        control_points->writeAsVectorImage(coefficients_filename);

        ///---------------------------------------------------------------------------//
        ///          COMPUTE THE ERROR AND USE IT AS INPUT FOR NEXT LEVEL             //
        ///---------------------------------------------------------------------------//
        if (debug) std::cout << "Compute error at level "<< nlevels<<std::endl;
        replaceVectorImagesWithError<VectorImageType, BsplineGridType>(vector_image, control_points, eyeMatrix, false, OUTPUT_DIMENSIONS);
        multilevel_grid.push_back(control_points);

        ///---------------------------------------------------------------------------//
        ///           UPDATE SPACING FOR THE NEXT ITERATION                           //
        ///---------------------------------------------------------------------------//
        if (debug) std::cout << "\t\tUpdate spacing for next iteration"<<std::endl;
        previous_spacing = grid_spacing;

    }

    if (debug) std::cout << "All levels done"<<std::endl;

    /****************************************************************************
     *                     BUILD VECTOR IMAGE FROM ALL COEFFICIENTS             *
     ****************************************************************************/

    if (std::strcmp(reference_image_filename.c_str(), ""))
    {
        if (debug) std::cout << "Build vector image from coefficients (to the highest level)"<<std::endl;
        typedef itk::ImageFileReader<DopplerImageType> RefReaderType;
        RefReaderType::Pointer refReader = RefReaderType::New();
        refReader->SetFileName(reference_image_filename);
        refReader->Update();
        DopplerImageType::Pointer reference_image = refReader->GetOutput();

        typedef itk::Image<itk::Vector<BsplineGridType::MatrixElementType,OUTPUT_DIMENSIONS>,NDIMS> VectorImageType;

        std::string output_image_flename = output_prefix +  std::string("_velocity") + echo::str::toString<unsigned int>(nlevels)
                +  std::string("_lambda") +  echo::str::toString<double>(lambda)
                +  std::string("_b") +  echo::str::toString<unsigned int>(border)
                +  std::string("_pb2n") +  echo::str::toString<unsigned int>(points_between_two_nodes)
                +  std::string("_thv") +  echo::str::toString<double>(th_velocity)
                +  std::string("_thrn") +  echo::str::toString<double>(remove_nodes_th)
                +  std::string("_bsd") +  echo::str::toString<unsigned int>(bsd) + extra;


        if (allLevels){
            std::vector<VectorImageType::Pointer> reconstructed_images = echo::calculateVectorImageAllLevels<VectorImageType, DopplerImageType,BsplineGridType>(reference_image, multilevel_grid, nlevels, OUTPUT_DIMENSIONS,  remove_nodes_th);
            for (int j=0; j< reconstructed_images.size(); ++j){
                if (debug) std::cout << "\tSave image at level "<< j<<std::endl;
                std::string my_filename = output_image_flename + std::string("_level") + echo::str::toString<int>(j) + std::string(".mhd");
                echo::writeImage<VectorImageType>(reconstructed_images[j], my_filename);
            }
        } else {
            if (debug) std::cout << "\tGenerate image"<<std::endl;
            VectorImageType::Pointer reconstructed_image = echo::calculateVectorImage<VectorImageType, DopplerImageType, BsplineGridType>(reference_image, multilevel_grid, nlevels, OUTPUT_DIMENSIONS,  remove_nodes_th);
            if (debug) std::cout << "\tSave image"<<std::endl;
            echo::writeImage<VectorImageType>(reconstructed_image, output_image_flename);
        }

    }



    return 0;

}




template < typename ImageType, typename BsplineGridType>
void replaceVectorImagesWithError(typename ImageType::Pointer image,  typename BsplineGridType::Pointer control_points, vtkSmartPointer<vtkMatrix4x4> matrix, bool inverse, int ndims){

    /// Calculate the positions that I need from the original non-downsampled data
    std::vector< PointType > points_current;
    std::vector< typename BsplineGridType::CoefficientType  > vector_values_current;


    echo::VectorImageToPointDataset< ImageType, PointType, VectorType>
            (image,matrix, points_current, vector_values_current,inverse); /// Converts an image into a 4D point dataset

    std::vector<typename BsplineGridType::CoefficientType > reconstructed_velocity = control_points->evaluate(points_current);

    /// Copy reconstructed velocity into an structure of type BsplineGridType::MatrixType and this should all be fine.

    typename std::vector<typename BsplineGridType::CoefficientType  >::const_iterator cit1;
    typename std::vector<typename BsplineGridType::CoefficientType>::const_iterator cit2;

    //std::vector<typename BsplineGridType::CoefficientType> error(reconstructed_velocity.size());
    //typename std::vector<typename BsplineGridType::CoefficientType>::iterator it;

    std::vector<VectorImageType::PixelType> error_vector(reconstructed_velocity.size());
    std::vector<VectorImageType::PixelType>::iterator it_vector;

    /// Calculate the error

    for(cit1= vector_values_current.begin(),cit2= reconstructed_velocity.begin(),  it_vector = error_vector.begin() ; // it = error.begin()
        it_vector != error_vector.end() ; //cit1 != vector_values_current.end(),
        ++cit1, ++cit2,  ++it_vector){ //++it,
        if (std::abs( (*cit1)[0]) > th_velocity){
            //*it= (*cit1)- *cit2;
            for (int j=0; j<ndims; j++)
                (*it_vector)[j]=(*cit1)[j]-(*cit2)[j];
        } else {
            //*it=(*cit1);
            for (int j=0; j<ndims; j++)
                (*it_vector)[j]=(*cit1)[j];
        }
    }

    if (debug) std::cout << "\t\tReplace current frame by error data"<<std::endl;
    /// Replace the image value by the error value (for next iteration)
    echo::replaceValues<ImageType>(image,  error_vector);

}

/**
 * display_arguments()
 *  Display input arguments
 */

void display_arguments()
{

    std::cout << "\tinput image\t"<<": "<<input_image_flename<<std::endl;
    std::cout << "\tinput prefix:\t"<<output_prefix<<std::endl;
    std::cout << "\tNumber of threads:\t"<< threads_to_use<<std::endl;
    std::cout << "\tOverlap border:\t"<< border<<std::endl;
    std::cout << "\tWall velocity array name:\t"<< wall_velocity<<std::endl;
    std::cout << "\tUser bounds set?:\t"<< use_user_bounds<< std::endl;
    std::cout << "\tGrid spacing set?:\t"<< (user_spacing[0]>0)<< std::endl;
    std::cout << "\tNumber of levels:\t"<<max_levels<<std::endl;
    std::cout << "\tBspline degree:\t"<<bsd<<std::endl;
    std::cout << "\tPoints between grid nodes:\t"<<points_between_two_nodes<<std::endl;
    std::cout << "\tVelocity threshold:\t"<<th_velocity<<std::endl;
    std::cout << "\tLambda:\t"<<lambda<<std::endl;
    std::cout << "\tRemove-nodes threshold:\t"<<remove_nodes_th<<std::endl;
    std::cout << "\tUse wall motion?:\t"<<use_extra_components<<std::endl;
    std::cout << "\tBlood velocity in m/s?:\t"<<v_in_mps<<std::endl;
    std::cout << "\tWall velocity in mm/s?:\t"<<wall_velocity_factor<<std::endl;
    std::cout << "\tReference image filename:\t"<<reference_image_filename<<std::endl;
    std::cout << "\tcyclic behaviour?:\t"<<cyclictime<<", cycle duration: "<<cycle_time[0]<<", "<< cycle_time[1]<<std::endl;
    std::cout << "\tMinimum grid resolution set?:\t"<<(minimum_grid_size[0]>0)<<std::endl;
    std::cout << "\tMax A size:\t"<<MAX_COLS_PER_PATCH<<std::endl;
    std::cout << "\tMax B rows:\t"<<MAX_POINTS_PER_PATCH<<std::endl;
    std::cout << "\tMax memory:\t"<<MAX_MEMORY<<" GB"<<std::endl;
    std::cout << "\tIs cartesian?:\t"<<isCartesian <<std::endl;
    std::cout << "\tWrite error images?:\t"<<writeErrors <<std::endl;
    std::cout << "\tCalculate output at all levels?:\t"<<allLevels <<std::endl;
    std::cout << "\tImage downsampling:\t"<< image_downsampling[0]<<","<< image_downsampling[1]<<","<<image_downsampling[2]<<","<<image_downsampling[3]<<std::endl;

}

/**
 * read_arguments()
 *  Read input arguments from the user and stores them in global variables
 */

void read_arguments(int argc, char* argv[])
{

    /// MANDATORY ARGUMENTS


    if (argc < 2)
    {
        std::cerr<< "Not enough arguments"<<std::endl<<std::endl;
        usage();
        exit(-1);
    }

    input_image_flename=argv[1];

    output_prefix=argv[2];

    /// OPTIONAL ARGUMENTS

    int i = 3;
    while (i < argc)
    {

        if (!std::strcmp(argv[i], "-h"))
        {
            usage();
            exit(0);
        }
        else if (!std::strcmp(argv[i], "-d"))
        {
            debug = true;
        }
        else if (!std::strcmp(argv[i], "-p"))
        {
            parallel_on = true;
            threads_to_use = atoi(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-v"))
        {
            verbose= true;
        }
        else if (!std::strcmp(argv[i], "-border"))
        {
            border= atoi(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-wall_velocity_array_name"))
        {
            wall_velocity= argv[++i];
        }
        else if (!std::strcmp(argv[i], "-bounds"))
        {
            use_user_bounds = true;
            for (int j=0; j<2*NDIMS; j++)
            {
                user_bounds[j]= atof(argv[++i]);
            }
        }
        else if (!std::strcmp(argv[i], "-grid_spacing"))
        {
            for (int j=0; j<NDIMS; j++)
            {
                user_spacing[j]= atof(argv[++i]);
            }
        }
        else if (!std::strcmp(argv[i], "-levels"))
        {
            max_levels= atoi(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-bsd"))
        {
            bsd= atoi(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-downsample"))
        {
            image_downsampling[0]= atof(argv[++i]);
            image_downsampling[1]= atof(argv[++i]);
            image_downsampling[2]= atof(argv[++i]);
            image_downsampling[3]= atof(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-pb2n"))
        {
            points_between_two_nodes= atoi(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-th_velocity"))
        {
            th_velocity= atof(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-lambda"))
        {
            lambda= atof(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-th_rn"))/// Remove nodes threshold
        {
            remove_nodes_th= atof(argv[++i]);
        }
        else if (!std::strcmp(argv[i], "-wall_motion"))/// Remove nodes threshold
        {
            input_motion_mesh_config_file =argv[++i];
            use_extra_components=true;
        }
        else if (!std::strcmp(argv[i], "-v_in_mps"))
        {
            v_in_mps=true;
        }
        else if (!std::strcmp(argv[i], "-ref"))/// Remove nodes threshold
        {
            reference_image_filename = argv[++i];
        }
        else if (!std::strcmp(argv[i], "-minGridResolution"))
        {
            for (int j=0; j< NDIMS; j++){
                minimum_grid_size[j] = atof(argv[++i]);
            }
        }
        else if (!std::strcmp(argv[i], "-error"))
        {
            writeErrors = true;
        }
        else if (!std::strcmp(argv[i], "-allLevels"))
        {
            allLevels= true;
        }
        else if (!std::strcmp(argv[i], "-initialLevel"))
        {
            initialLevel = atoi(argv[++i]);
            if (initialLevel<2){
                std::cout << "If -initialLevel is used, the minimum start level is 2."<<std::endl;
                usage();
                exit(0);
            }
            initial_coeffs_filename.resize(initialLevel-1);
            for (int j=0; j<(initialLevel-1); j++)
            {
                initial_coeffs_filename[j]= argv[++i];
            }
        }
        else
        {
            std::cout << "Argument " << argv[i] << " is not recognised."
                      << std::endl;
            usage();
            exit(-1);
        }

        i++;
    }
}

/**
        * usage()
        * Display instructions
        */

void usage()
{
    std::cout << "----- testbspline -----" << std::endl;
    std::cout << "velocity reconstruction from subsampled data. (c) Alberto Gomez 2016 -- alberto.gomez@kcl.ac.uk" << std::endl<<std::endl;
    std::cout << "Usage:" << std::endl << std::endl;
    std::cout
            << "testbspline <input filename> <output prefix>"
            << "[-bsd <bsd>] [-border <border>] [-levels <n_max_levels>] [-pb2n <npoints>] [-bounds <xmin xmax ymin ymax zmin zmax tmin tmax>]"
            << "[-th_velocity <th>] [-lambda <lambda>] [-rn_th <rn_th>] [-wall_motion <wall_motion_mesh1> ... <wall_motion_meshN>] [-v_in_mps]"
            << " [-wall_velocity_array_name <name>] [-grid_spacing <gsx gsy gsz gst>] [-ref <filename>] [-initialLevel <N> <coeff_level1> <coeff_levelL-1>] "
            << " [-cyclic <t0> <t1>][-d] [-h] [-v] [-p <#threads>]   [-minGridResolution <rx ry rz rt>] [-wall_velocity_in_mmps] [-max_A_size <n>] [-max_B_rows <n>]"
            << " [-cartesian <bs1> <bs2> ... <bsN>] [-fit] [-allLevels] [-error] [-downsample <df>]"
            << std::endl;
    std::cout << std::endl;
    std::cout << "Mandatory arguments (these must be in order):" << std::endl;
    std::cout << "\t\timage filename"	<< std::endl;
    std::cout << "\t\toutput_prefix filename for the output, without extension"	<< std::endl;
    std::cout << "Optional arguments (these can be in any order):" << std::endl;
    std::cout << "\t-bsd\tBspline degree [3]" << std::endl;
    std::cout << "\t-d\tDebug mode [off]." << std::endl;
    std::cout << "\t-p\tParallel computation [off]. Also, specify the number of threads." << std::endl;
    std::cout << "\t-border\tThe gridpoints to be added to the patch to increase accuracy. Default is border=0" << std::endl;
    std::cout << "\t-levels\tThe maximum number of refinement levels. Default is 3." << std::endl;
    std::cout << "\t-pb2n\tnumber of input data points between two grid nodes after downsampling. Default is 2." << std::endl;
    std::cout << "\t-bounds\tRegion of interest where vectors will be reconstructed. Default is the bounds of the input data." << std::endl;
    std::cout << "\t-th_velocity\tThreshold of low velocity. Velocities below this value will not be considered. Default value is -1 (no thresholding)." << std::endl;
    std::cout << "\t-th_rn\tThreshold of node removal, bounded between [0,1]. a value less than zero means no removal. Default is 0 (nodes with no input data points within their support are removed)" << std::endl;
    std::cout << "\t-lambda\tRegularization value, in the interval [0, 1) where 0 is no regularisation. Default is 0." << std::endl;
    std::cout << "\t-wall_motion\tVTK polydata files with mesh motion (there should be a vector array associated with the nodes containing the velocity vectors)" << std::endl;
    std::cout << "\t-o\tfilename for the output" << std::endl;
    std::cout << "\t-v_in_mps\tSets the output velocity to be in meters per second. Default false (i.e. in cm/s)" << std::endl;
    std::cout << "\t-wall_velocity_in_mmps\tTakes the input wall velocity as mmps. Default is the same units as the images (i.e. in cm/s)" << std::endl;
    std::cout << "\t-wall_velocity_array_name\tName in the input meshes of the array with wall velocities. Default wall_velocity" << std::endl;
    std::cout << "\t-grid_spacing\tMaximum spacing between grid nodes. By default, the extent of input data." << std::endl;
    std::cout << "\t-ref\tFilename of the reference image (4D mask). Enables output writing."	<< std::endl;
    std::cout << "\t-initialLevel\tStart at level N (first level starts at 1), and give the coefficients for levels 1 to N-1."	<< std::endl;
    std::cout << "\t-inv\tInvert matrices (default off)." << std::endl;
    std::cout << "\t-cyclic\tImpose cyclic behaviour over time between t0 and t1 (default off)." << std::endl;
    std::cout << "\t-minGridResolution\tGrid resolution of the B-spline grid at the finest level. Any dimension set to -1 is ignored" << std::endl;
    std::cout << "\t-max_A_size\tMaximum rows (=columns) of the matrix to be solved (default 10000)." << std::endl;
    std::cout << "\t-max_B_rows\tMaximum rows of B (=number of input data points) considered  for each patch (default 2.5E+05)." << std::endl;
    std::cout << "\t-maxMemory\tMaximum memory (in GB) that the problem can use. If not used, then the system is queried." << std::endl;
    std::cout << "\t-cartesian\tInput images in cartesian coordinates (default: OFF, in spherical). The beam source position for each image has to be then specified." << std::endl;
    std::cout << "\t-fit\tInstead of taking input data as proyected values along the beam direction, takes the data *as is*, i.e. input data should be vectors, which will be taken as projections. This option assumes that data is in cartesian coordinates. In this case, beam source will be ignored." << std::endl;
    std::cout << "\t-error\tWrite residual images at each level (DEFAULT OFF)." << std::endl;
    std::cout << "\t-allLevels\tCreates one output image for each level. If this option is selected, the output filename must be a prefix." << std::endl;
    std::cout << "\t-downsample\tDownsample input images by a factor <dfx> <dfy> <dfz> <dft>"<<std::endl;
    std::cout << "\t-h\tShow this dialog (default off)." << std::endl;
    std::cout << "\t-v\tVerbose mode. Writes plenty out outputs to files. (default off)." << std::endl;


    /// Old arguments, now not in use
    /*
            std::cout << "\t-mask\tImage which will be used as mask. Note that velocity vectors will be computed at any point where the mask value is equal or higher than a threshold indicated by -mask_th" << std::endl;
            std::cout << "\t-mask_th\tThreshold for the mask, by default 3" << std::endl;
            std::cout << "\t-tol\tMinimum tolerance to be attained, by default set to 0.00001" << std::endl;
            std::cout << "\t-tol_th\tAvove this tol, coefficients are set to 0" << std::endl;
            std::cout << "\t-maxit\tmaximum number of iterations, by default 20" << std::endl;
            std::cout << "\t-method\tIterative method used for solving the linear system: BiCG (0, default), BiCGSTAB (1), CG (2), CGS (3), QMR (4, not adviced!)" << std::endl;
            std::cout << "\t-subdivide\tSubdivide patches if there is not convergency (default, false)" << std::endl;
            std::cout << "\t-weighting\t Enables weighting fo the linear system. 'nc' determines the number of clusters (default, false)" << std::endl;
            std::cout << "\t-nc\t number of clusters (default 3)" << std::endl;
            std::cout << "\t-th_vel\tThreshold for the Doppler signal, by default 0. If a negative number is used, all velocity is included." << std::endl;
            std::cout << "\t-directSolve\tUse direct solver instead of iterative solvers." << std::endl;
            */
}


