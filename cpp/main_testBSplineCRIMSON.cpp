
/**
  * This is similar to the 3D reconstruction but also over time. Time is treated as just another dimension,
  * however it is imposed periodic. This is achieved by extending input data up to 2 b-spline grid points on
  * each side along the time direction (or is it just 1?). Also, temporal resolution is also increased over
  * the multiple scales is not divided by 2, instead it goes from 4 control points to 10 (maybe change this,
  * we will see). Another difference is that temporal variation can be penalised. For starters, we don't
  * really do this.
  *
  * (c) Alberto Gomez 2014 - King's College London
  */


/// Miscelanea
#include <iostream>
#include <vector>
#include <algorithm>

/// VTK headers
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataNormals.h>

/// ITK headers
#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkResampleImageFilter.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>

/// Smart Pointers from boost library
#include <boost/shared_ptr.hpp>

/// Internal includes
#include "BsplineGrid.hxx"
#include "LinearSolvers.hxx"

/// Internal includes from namespace echo
#include "Image_tools.hxx"
#include "Mesh/Mesh_tools.hxx"
#include "Point_tools.hxx"
#include "String_tools.hxx"

// parallel computing
/// Macros for the maximun value out of three scalars or the three components of one vector
#define MAX2(x,y,z) std::max( std::max(x,y),z  )
#define MAX2_V(x) std::max( std::max(x[0],x[1]),x[2]  )
#define MAX2_V_abs(x) std::max( std::max( std::abs(x[0]), std::abs(x[1])), std::abs(x[2])  )


// Variables that are accessed by many methods

/// misc
const bool KEEP_SMALL_control_pointsES = true; /// If this is on, control_pointses are adaptively subdivided
/// For mac
//const int MAX_COLS_PER_control_points = 25000; /// For the A matrix
//const int MAX_POINTS_PER_control_points = 1E+07; /// For the B matrix

/// For linux
int MAX_COLS_PER_control_points = 10000; /// For the A matrix
int MAX_POINTS_PER_control_points = 2.5E+05; /// For the B matrix
float MAX_MEMORY = -1;

const double CMS_TO_MS = 1E-02;
const double MS_TO_CMS = 100.0;

const unsigned int NDIMS=1;
const unsigned int OUTPUT_DIMENSIONS=1;

const double MAX_ERROR=1E-05;
const double B_TO_MB = 1/1024.0/1024.0;
const double GB_TO_MB = 1024.0;
unsigned int number_of_control_pointses;
const double ZERO=1E-10;
double spacing_reduction_factor_space[] = {2.0,2.0,2.0,1.2};
double minimum_grid_size[] = {-1,-1,-1,-1};
bool isCartesian =false;
bool fitData =false;
bool writeErrors = false;
/// mandatory arguments
int n_images;
std::string reference_image_filename("");
std::string input_image_flename;
std::string extra("");
std::string input_motion_mesh_config_file;
std::string output_prefix;
/// default arguments
bool debug = false;
bool parallel_on = false;
int threads_to_use = 2;
bool verbose = false;
unsigned int border=0;
unsigned int max_levels =3;
unsigned int points_between_two_nodes=2;
double user_bounds[2*NDIMS];

double image_downsampling[4] = {-1, -1, -1, -1};

bool cyclictime = false;
double cycle_time[2];

bool allLevels=false;
bool use_user_bounds = false;
double th_velocity = -1;
double lambda = 0;
double remove_nodes_th = 0.0;
bool use_extra_components = false;
bool v_in_mps=false;
double wall_velocity_factor=1.0;
std::string wall_velocity("wall_velocity");
int initialLevel = 0;
std::vector <std::string> initial_coeffs_filename;


// Define types

typedef unsigned short BModePixelType;
typedef itk::Image<BModePixelType,NDIMS> BModeImageType;
typedef float DopplerPixelType;
typedef itk::Image<DopplerPixelType,NDIMS> DopplerImageType;

typedef itk::Image<itk::Vector<DopplerPixelType, OUTPUT_DIMENSIONS>, NDIMS> VectorImageType;

typedef BsplineGrid<float,NDIMS ,OUTPUT_DIMENSIONS> BsplineGridType;
typedef BsplineGridType::PointType PointType;
typedef BsplineGridType::CoefficientType VectorType;
std::vector <PointType > beam_sources;

typedef echo::Point<BsplineGridType::MatrixElementType,1> ValueType;


template <typename TBsplineGridType>
void fit_cyclicBspline1D(const std::vector< typename TBsplineGridType::PointType > &t, const std::vector< typename TBsplineGridType::CoefficientType > &v,
                         double lambda, typename TBsplineGridType::Pointer control_points);


int main(int argc, char* argv[])
{


    /****************************************************************************
    *                               Create input data                           *
    ****************************************************************************/

    /// for cyclic BSpline interpolation. Create a trace with with 9 samples
    int N=9;
    double t[] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8};
    double v[] = {1.0,2.0,3.0,3.5,3.1,2.8,2.5,2.2,1.9};

    // copy input into the appropriate vectors
    std::vector< BsplineGridType::PointType > coordinates; // in this case, 1D
    std::vector<BsplineGridType::CoefficientType> values; // in this case, velocity

    for (int i=0; i<N; i++){
        coordinates.push_back(t[i]);
        values.push_back(v[i]);
    }



    /// Algorithmic parameters
    double bspline_spacing_1D = 0.2;
    double cycleDuration = 1.0; // 60bpm
    int N2 = 100; /// Number of samples after interpolation
    unsigned int bsd=3; // b-spline degree, by default is cubic

    ///****************************************************************************
    ///                    Initialise the B-spline grid
    ///****************************************************************************
    double bounds[2*NDIMS]; // bounds of the domain, time in this case
    bounds[0] = t[0];
    bounds[1] = t[N-1];
    BsplineGridType::PointType grid_spacing;
    grid_spacing[0]=bspline_spacing_1D; // for example!

    BsplineGridType::Pointer control_points(new BsplineGridType(bounds, bsd, grid_spacing, 0)); /// the control point grid does not need border. This grid covers the whole ROI and there is no control_points division.
    BsplineGridType::IndexType cyclicDimensions;
    cyclicDimensions[0]=1; // first dimension is cyclic
    control_points->SetCyclicDimensions(cyclicDimensions);
    control_points->SetCyclicBounds(0,t[0],cycleDuration);
    control_points->SetDebug(debug);
    if (debug) std::cout << "\t\tUsing "<< threads_to_use<< " threads."<<std::endl;
    control_points->SetParallel(parallel_on, threads_to_use);
    control_points->UpdateCyclicBehaviour();

    double lambda=0; // no regularization



    fit_cyclicBspline1D<BsplineGridType>(coordinates, values, lambda, control_points);

    /// interpolate ---------------------------------------

    std::vector< BsplineGridType::PointType > interpolated_t(N2);
    for (int i=0; i<N2; i++){
        interpolated_t[i] = i*cycleDuration/N2;
    }
    std::vector<BsplineGridType::CoefficientType>  interpolated_values;
    interpolated_values =  control_points->evaluate(interpolated_t);


    // ----------------------- print to file ---------------------

    ///---------------------------------------------------------------------------//
    ///                       PRINT RESULTS TO FILE                               //
    ///---------------------------------------------------------------------------//

    int ncycles = 3;

    {
        std::ofstream file;
        std::string filename = "/tmp/original.txt";
        file.open(filename, ofstream::out);
        for (int cycle=0; cycle<ncycles; ++cycle ){
            for (int i=0; i<N; i++){
                file << coordinates[i]+cycleDuration*cycle << " "<< values[i]<<std::endl;
            }
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/interpolated.txt";
        file.open(filename, ofstream::out);
        for (int cycle=0; cycle<ncycles; ++cycle ){
            for (int i=0; i<N2; i++){
                file << interpolated_t[i]+cycleDuration*cycle << " "<< interpolated_values[i]<<std::endl;
            }
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }




    return 0;
}


template <typename TBsplineGridType>
void fit_cyclicBspline1D(const std::vector<typename TBsplineGridType::PointType > &t, const std::vector< typename TBsplineGridType::CoefficientType > &v,
                                   double lambda, typename TBsplineGridType::Pointer control_points){

    /****************************************************************************
    *                               CALCULATE EXTENT                            *
    ****************************************************************************/

    Eigen::setNbThreads(threads_to_use);
    Eigen::initParallel();



    ///---------------------------------------------------------------------------//
    ///                       FIND BSPLINE COEFFICIENTS                              //
    ///---------------------------------------------------------------------------//

    echo::Point<double,2*TBsplineGridType::IDims> control_points_bounds;
    control_points->GetBorderBoundsOfInfluence(&control_points_bounds[0]);

    typename TBsplineGridType::DenseVectorType values_vector(v.size(),1);
    std::vector<ValueType>::const_iterator const_iter;
    typename TBsplineGridType::MatrixElementType *eigen_iter;

    for (const_iter = v.begin(), eigen_iter= values_vector.data();const_iter!=v.end();  ++const_iter)
    {
        for (int nd = 0; nd< NDIMS; nd++){
            (*eigen_iter)=(*const_iter)[nd];
            ++eigen_iter;
        }
    }

    typename TBsplineGridType::SparseMatrixType A;
    typename TBsplineGridType::DenseVectorType b;
    std::vector<unsigned int> kept_nodes;
    std::vector<unsigned int> corresponding_nodes;

    /// Keep environment to save memory

    typename TBsplineGridType::SparseMatrixType B = control_points->createSamplingMatrix(t, remove_nodes_th, kept_nodes, corresponding_nodes);

    { /// Keep scope to save memory
        Eigen::setNbThreads(8);
        A = B.transpose() *B;
        b = B.transpose()*values_vector;
    }


     // Regularisation
        if (lambda)
        {
            if (debug) std::cout << "\tUse regularisation with lambda ="<<lambda<< std::endl;
            BsplineGridType::SparseMatrixType Adiv = control_points->createContinuousDivergenceSamplingMatrix(kept_nodes,corresponding_nodes);
            A = A+lambda/(1-lambda)*Adiv;/// See notes on p118 of my thesis for an explanation
        }


    /// Available additional solvers
    /// BiCGSTAB + IncompletLUT preconditioner
    /// SparseQR
    /// SPQR
    /// PastixLU

    typename TBsplineGridType::DenseVectorType x;//,x2,x3,x4;
    echo::solvers::ConfigurationParameter config;
    config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
    config.tol=1E-06;
    config.verbose = true;
    std::string str_solver;

    /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
    if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
    echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
    /// This can happen if there is a region with very densely populated points
    //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
    //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

    /// Reorganize x into an array of coefficients, taking into account the reordering
    control_points->setAll(x,  kept_nodes);
}

