
/**
  * This is similar to the 3D reconstruction but also over time. Time is treated as just another dimension,
  * however it is imposed periodic. This is achieved by extending input data up to 2 b-spline grid points on
  * each side along the time direction (or is it just 1?). Also, temporal resolution is also increased over
  * the multiple scales is not divided by 2, instead it goes from 4 control points to 10 (maybe change this,
  * we will see). Another difference is that temporal variation can be penalised. For starters, we don't
  * really do this.
  *
  * (c) Alberto Gomez 2014 - King's College London
  */


/// Miscelanea
#include <iostream>
#include <vector>
#include <algorithm>

/// VTK headers
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataNormals.h>

/// ITK headers
#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkResampleImageFilter.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>

/// Smart Pointers from boost library
#include <boost/shared_ptr.hpp>

/// Internal includes
#include "BsplineGrid.hxx"
#include "LinearSolvers.hxx"

/// Internal includes from namespace echo
#include "Image_tools.hxx"
#include "Mesh/Mesh_tools.hxx"
#include "Point_tools.hxx"
#include "String_tools.hxx"

// parallel computing
/// Macros for the maximun value out of three scalars or the three components of one vector
#define MAX2(x,y,z) std::max( std::max(x,y),z  )
#define MAX2_V(x) std::max( std::max(x[0],x[1]),x[2]  )
#define MAX2_V_abs(x) std::max( std::max( std::abs(x[0]), std::abs(x[1])), std::abs(x[2])  )


// Variables that are accessed by many methods

/// misc
const bool KEEP_SMALL_control_pointsES = true; /// If this is on, control_pointses are adaptively subdivided
/// For mac
//const int MAX_COLS_PER_control_points = 25000; /// For the A matrix
//const int MAX_POINTS_PER_control_points = 1E+07; /// For the B matrix

/// For linux
int MAX_COLS_PER_control_points = 10000; /// For the A matrix
int MAX_POINTS_PER_control_points = 2.5E+05; /// For the B matrix
float MAX_MEMORY = -1;

const double CMS_TO_MS = 1E-02;
const double MS_TO_CMS = 100.0;

const unsigned int NDIMS=2;
const unsigned int OUTPUT_DIMENSIONS=1;

const double MAX_ERROR=1E-05;
const double B_TO_MB = 1/1024.0/1024.0;
const double GB_TO_MB = 1024.0;
unsigned int number_of_control_pointses;
const double ZERO=1E-10;
double spacing_reduction_factor_space[] = {2.0,2.0,2.0,1.2};
double minimum_grid_size[] = {-1,-1,-1,-1};
bool isCartesian =false;
bool fitData =false;
bool writeErrors = false;
/// mandatory arguments
int n_images;
std::string reference_image_filename("");
std::string input_image_flename;
std::string extra("");
std::string input_motion_mesh_config_file;
std::string output_prefix;
/// default arguments
bool debug = false;
bool parallel_on = false;
int threads_to_use = 2;
bool verbose = false;
unsigned int border=0;
unsigned int bsd=3;
unsigned int max_levels =3;
unsigned int points_between_two_nodes=2;
double user_bounds[2*NDIMS];

double image_downsampling[4] = {-1, -1, -1, -1};

bool cyclictime = false;
double cycle_time[2];

bool allLevels=false;
bool use_user_bounds = false;
double th_velocity = -1;
double remove_nodes_th = 0.0;
bool use_extra_components = false;
bool v_in_mps=false;
double wall_velocity_factor=1.0;
std::string wall_velocity("wall_velocity");
int initialLevel = 0;
std::vector <std::string> initial_coeffs_filename;


// Define types

typedef unsigned short BModePixelType;
typedef itk::Image<BModePixelType,NDIMS> BModeImageType;
typedef float DopplerPixelType;
typedef itk::Image<DopplerPixelType,NDIMS> DopplerImageType;

typedef itk::Image<itk::Vector<DopplerPixelType, OUTPUT_DIMENSIONS>, NDIMS> VectorImageType;

typedef BsplineGrid<float,NDIMS ,OUTPUT_DIMENSIONS> BsplineGridType;
typedef BsplineGrid<float,1 ,1> BsplineGrid1DType;

typedef BsplineGridType::PointType PointType;
typedef BsplineGridType::CoefficientType VectorType;
std::vector <PointType > beam_sources;

typedef echo::Point<BsplineGridType::MatrixElementType,1> ValueType;





template <typename TBsplineGridType>
void fit_cyclicBspline1D(const std::vector<typename TBsplineGridType::PointType > &t, const std::vector< typename TBsplineGridType::CoefficientType > &v,
                         double lambda, typename TBsplineGridType::Pointer control_points);



template <typename TBsplineGrid1DType, typename TBsplineGrid2DType>
void interpolateClosedContour(const std::vector< typename TBsplineGrid2DType::PointType> &coordinates,  const std::vector< typename TBsplineGrid1DType::PointType> &interpolated_contour_angle,
                              std::vector< typename TBsplineGrid2DType::PointType> &interpolated_contour);








int main(int argc, char* argv[])
{



    Eigen::setNbThreads(threads_to_use);
    Eigen::initParallel();



    /****************************************************************************
    *                               Create input data                           *
    ****************************************************************************/

#include "test_data.h"

    /// We are assuming that the first point is the reference point for the rotation.
    /// If this is determined by other means it does not matter, in the end what we need
    /// is the value 'a' of the angle between the two (computed below)

    int Ninterp = 75; /// Number of corresponding points
    double lambda = 0.1;
    int ncontrol_pts = 4;

    ///****************************************************************************
    ///*                            Find centroids                             *
    ///****************************************************************************

    BsplineGridType::PointType centroidA, centroidB;
    centroidA[0]=0; centroidA[1]=0;
    for (int i=0; i<coordinatesA.size(); i++){
        centroidA+=coordinatesA[i];
    }
    centroidA/=coordinatesA.size();

    centroidB[0]=0; centroidB[1]=0;
    for (int i=0; i<coordinatesB.size(); i++){
        centroidB+=coordinatesB[i];
    }
    centroidB/=coordinatesB.size();

    std::vector< BsplineGridType::PointType > centred_coordinatesA(coordinatesA.size()), centred_coordinatesB(coordinatesB.size());
    for (int i=0; i<coordinatesA.size(); i++){
        centred_coordinatesA[i]= coordinatesA[i] - centroidA;
    }
    for (int i=0; i<coordinatesB.size(); i++){
        centred_coordinatesB[i]= coordinatesB[i] -centroidB;
    }

    ///****************************************************************************
    ///*                            Find the rotation                             *
    ///****************************************************************************
     BsplineGridType::PointType vA, vB;
     vA = centred_coordinatesA[0];
     vB = centred_coordinatesB[0];

     double angleA = std::atan2(vA[1],vA[0]);
     double angleB = std::atan2(vB[1],vB[0]);
     double a = angleB-angleA;

     BsplineGridType::DenseMatrixType Rot(2,2);
     Rot << std::cos(a), -std::sin(a),
            std::sin(a), std::cos(a);

     std::vector< BsplineGridType::PointType> rotated_centred_coordinatesA(centred_coordinatesA.size());

     for (int i=0; i< centred_coordinatesA.size(); i++){
         /// not very elegant, sorry !
         rotated_centred_coordinatesA[i][0] = Rot(0,0) * centred_coordinatesA[i][0] + Rot(0,1) * centred_coordinatesA[i][1];
         rotated_centred_coordinatesA[i][1] = Rot(1,0) * centred_coordinatesA[i][0] + Rot(1,1) * centred_coordinatesA[i][1];
     }



    ///****************************************************************************
    ///*                            FIT A CYCLIC B-spline to each contour         *
    ///****************************************************************************

    std::vector< BsplineGrid1DType::PointType > interpolated_contour_angle(Ninterp);
    for (int i=0; i<Ninterp; i++){
        interpolated_contour_angle[i] = i*2*M_PI/Ninterp+angleB;
    }
    std::vector< BsplineGridType::PointType> interpolated_contourA, interpolated_contourB;

    interpolateClosedContour<BsplineGrid1DType,BsplineGridType>(rotated_centred_coordinatesA, interpolated_contour_angle, interpolated_contourA);
    interpolateClosedContour<BsplineGrid1DType,BsplineGridType>(centred_coordinatesB, interpolated_contour_angle, interpolated_contourB);



    ///****************************************************************************
    ///                Calculate the vectors from one contour to another          *
    ///****************************************************************************
    std::vector< BsplineGridType::CoefficientType> contour_deformation_vectorsx(Ninterp), contour_deformation_vectorsy(Ninterp);
    for (int i=0; i<Ninterp; i++){

        auto vec = interpolated_contourB[i]- interpolated_contourA[i];
        contour_deformation_vectorsx[i][0] = vec[0];
        contour_deformation_vectorsy[i][0] = vec[1];
    }

    ///****************************************************************************
    ///             Interpolate the deformation field densely                     *
    ///****************************************************************************

    //interpolated_contourA; // coordinates
    //contour_deformation_vectors; // values

    // compute bounds
    double bounds[BsplineGridType::IDims*2];
    for (int i=0; i<BsplineGridType::IDims; i++){
        bounds[2*i]=std::numeric_limits<double>::max();
        bounds[2*i+1]=-std::numeric_limits<double>::max();
    }
    for (int i=0; i<Ninterp; i++){
        for (int j=0; j< BsplineGridType::IDims; j++){
            //  if (interpolated_contourA[i][j]<bounds[2*j]){
            //      bounds[2*j]  =interpolated_contourA[i][j];
            //  }
            if (interpolated_contourB[i][j]<bounds[2*j]){
                bounds[2*j]  =interpolated_contourB[i][j];
            }
            //  if (interpolated_contourA[i][j]>bounds[2*j+1]){
            //      bounds[2*j+1]  =interpolated_contourA[i][j];
            //  }
            if (interpolated_contourB[i][j]>bounds[2*j+1]){
                bounds[2*j+1]  =interpolated_contourB[i][j];
            }
        }
    }

    BsplineGridType::PointType grid_spacing;
    for (int j=0; j< BsplineGridType::IDims; j++){
        grid_spacing[j] = (bounds[2*j+1]-bounds[2*j])/ncontrol_pts;
    }

    BsplineGridType::Pointer control_points2D(new BsplineGridType(bounds, bsd, grid_spacing, 0)); /// the control point grid does not need border. This grid covers the whole ROI and there is no control_points division.
    control_points2D->SetParallel(parallel_on, threads_to_use);


    BsplineGridType::DenseVectorType values_vectorx(BsplineGridType::ODims*contour_deformation_vectorsx.size(),1);
    BsplineGridType::DenseVectorType values_vectory(BsplineGridType::ODims*contour_deformation_vectorsy.size(),1);
    std::vector< BsplineGridType::CoefficientType>::const_iterator const_iter;
    BsplineGridType::MatrixElementType *eigen_iter;


    for (const_iter = contour_deformation_vectorsx.begin(), eigen_iter= values_vectorx.data();const_iter!=contour_deformation_vectorsx.end();  ++const_iter)
    {
        for (int nd = 0; nd< BsplineGridType::ODims; nd++){
            (*eigen_iter)=(*const_iter)[nd];
            ++eigen_iter;
        }
    }


    for (const_iter = contour_deformation_vectorsy.begin(), eigen_iter= values_vectory.data();const_iter!=contour_deformation_vectorsy.end();  ++const_iter)
    {
        for (int nd = 0; nd< BsplineGridType::ODims; nd++){
            (*eigen_iter)=(*const_iter)[nd];
            ++eigen_iter;
        }
    }

    BsplineGridType::SparseMatrixType A;
    BsplineGridType::DenseVectorType b;
    std::vector<unsigned int> kept_nodes;
    std::vector<unsigned int> corresponding_nodes;

    std::vector<BsplineGridType::CoefficientType> interpolated_vectorsx, interpolated_vectorsy;
    BsplineGridType::SparseMatrixType B = control_points2D->createSamplingMatrix(interpolated_contourA, remove_nodes_th, kept_nodes, corresponding_nodes);
    BsplineGridType::SparseMatrixType Agrad ;
    if (lambda)
    {
        //BsplineGridType::SparseMatrixType Adiv = control_points2D->createContinuousDivergenceSamplingMatrix(kept_nodes,corresponding_nodes);
        Agrad= control_points2D->createContinuousGradientSamplingMatrix(remove_nodes_th, kept_nodes);
    }


    std::vector<BsplineGridType::PointType> coordinatesOut_centred(coordinatesOut);
    for (int i=0; i<coordinatesOut.size(); i++){
        coordinatesOut_centred[i] = coordinatesOut[i] -centroidB;
    }


    ///---------------------------------------
    ///     Do For X
    ///---------------------------------------
    {

        { /// Keep scope to save memory
            Eigen::setNbThreads(8);
            A = B.transpose() *B;
            b = B.transpose()*values_vectorx;
        }

        // Regularisation
        if (lambda){
            A = A+lambda/(1-lambda)*Agrad;/// See notes on p118 of my thesis for an explanation
        }


        if (debug) std::cout << "\tCompute coefficients in this control_points"<< std::endl;
        /// Available additional solvers
        /// BiCGSTAB + IncompletLUT preconditioner
        /// SparseQR
        /// SPQR
        /// PastixLU

        BsplineGridType::DenseVectorType x;//,x2,x3,x4;
        echo::solvers::ConfigurationParameter config;
        config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
        config.tol=1E-06;
        config.verbose = true;
        std::string str_solver;

        /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
        if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
        echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
        /// This can happen if there is a region with very densely populated points
        //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
        //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

        if (debug) std::cout << "\tCoefficients computed, replace control_points values with the calculated coefficients"<< std::endl;
        /// Reorganize x into an array of coefficients, taking into account the reordering
        control_points2D->setAll<BsplineGridType::DenseVectorType>(x,  kept_nodes);


        interpolated_vectorsx= control_points2D->evaluate(coordinatesOut_centred);
    }

    ///---------------------------------------
    ///     Do For Y
    ///---------------------------------------
    {



        { /// Keep scope to save memory
            Eigen::setNbThreads(8);
            A = B.transpose() *B;
            b = B.transpose()*values_vectory;
        }

        // Regularisation
        if (lambda){
            A = A+lambda/(1-lambda)*Agrad;/// See notes on p118 of my thesis for an explanation
        }


        if (debug) std::cout << "\tCompute coefficients in this control_points"<< std::endl;
        /// Available additional solvers
        /// BiCGSTAB + IncompletLUT preconditioner
        /// SparseQR
        /// SPQR
        /// PastixLU

        BsplineGridType::DenseVectorType x;//,x2,x3,x4;
        echo::solvers::ConfigurationParameter config;
        config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
        config.tol=1E-06;
        config.verbose = true;
        std::string str_solver;

        /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
        if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
        echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
        /// This can happen if there is a region with very densely populated points
        //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
        //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

        if (debug) std::cout << "\tCoefficients computed, replace control_points values with the calculated coefficients"<< std::endl;
        /// Reorganize x into an array of coefficients, taking into account the reordering
        control_points2D->setAll<BsplineGridType::DenseVectorType>(x,  kept_nodes);


        interpolated_vectorsy= control_points2D->evaluate(coordinatesOut_centred);
    }


    ///---------------------------------------------------------------------------//
    ///                       PRINT RESULTS TO FILE                               //
    ///---------------------------------------------------------------------------//


    {
        std::ofstream file;
        std::string filename = "/tmp/original2DA.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<coordinatesA.size(); i++){
            file << coordinatesA[i][0]<< " "<< coordinatesA[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/centred2DA.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<centred_coordinatesA.size(); i++){
            file << centred_coordinatesA[i][0]<< " "<< centred_coordinatesA[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/rotated_centred2DA.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<rotated_centred_coordinatesA.size(); i++){
            file << rotated_centred_coordinatesA[i][0]<< " "<< rotated_centred_coordinatesA[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/interpolated2DA.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<interpolated_contourA.size(); i++){
            file << interpolated_contourA[i][0] << " "<< interpolated_contourA[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/original2DB.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<coordinatesB.size(); i++){
            file << coordinatesB[i][0]<< " "<< coordinatesB[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/centred2DB.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<centred_coordinatesB.size(); i++){
            file << centred_coordinatesB[i][0] << " "<< centred_coordinatesB[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/interpolated2DB.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<interpolated_contourB.size(); i++){
            file << interpolated_contourB[i][0] << " "<< interpolated_contourB[i][1]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/contourVectors.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<interpolated_contourA.size(); i++){
            file << interpolated_contourA[i][0] << " "<< interpolated_contourA[i][1]<<" "<<contour_deformation_vectorsx[i][0] << " "<< contour_deformation_vectorsy[i][0]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/denseDeformation.txt";
        file.open(filename, ofstream::out);
        for (int i=0; i<coordinatesOut_centred.size(); i++){
            file << coordinatesOut_centred[i][0] << " "<< coordinatesOut_centred[i][1]<<" "<<interpolated_vectorsx[i][0] << " "<< interpolated_vectorsy[i][0]<<std::endl;
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

















    // evaluate at Ninterp points

    /*
    ///****************************************************************************
    ///*                 INITIALISE GRID                                           *
    ///****************************************************************************


    /// Create the grid
    /// The original grid spacing is such that there will be 4 grid points within the domain.
    /// This, for the experiments, seemend like a reasonable starting point. Along the time direction,
    /// we will start by 4 points and, for the time being stay there





    BsplineGridType::Pointer control_points(new BsplineGridType(bounds, bsd, grid_spacing, 0)); /// the control point grid does not need border. This grid covers the whole ROI and there is no control_points division.
    BsplineGridType::IndexType cyclicDimensions;
    cyclicDimensions[0]=1; // first dimension is cyclic
    control_points->SetCyclicDimensions(cyclicDimensions);
    control_points->SetCyclicBounds(0,t[0],cycleDuration);
    control_points->SetDebug(debug);
    if (debug) std::cout << "\t\tUsing "<< threads_to_use<< " threads."<<std::endl;
    control_points->SetParallel(parallel_on, threads_to_use);
    control_points->UpdateCyclicBehaviour();

    ///---------------------------------------------------------------------------//
    ///                       FIND BSPLINE COEFFICIENTS                              //
    ///---------------------------------------------------------------------------//

    echo::Point<double,2*NDIMS> control_points_bounds;
    control_points->GetBorderBoundsOfInfluence(&control_points_bounds[0]);


    if (debug) std::cout << "\t\tBounds are "<< control_points_bounds<<std::endl;

    BsplineGridType::DenseVectorType values_vector(values.size(),1);
    std::vector<ValueType>::const_iterator const_iter;
    BsplineGridType::MatrixElementType *eigen_iter;

    if (debug) std::cout << "\t\tCopy "<< values_vector.rows() <<" values "<<std::endl;
    for (const_iter = values.begin(), eigen_iter= values_vector.data();const_iter!=values.end();  ++const_iter)
    {
        for (int nd = 0; nd< NDIMS; nd++){
            (*eigen_iter)=(*const_iter)[nd];
            ++eigen_iter;
        }
    }

    if (debug) std::cout << "\tCreate Matrices "<< std::endl;
    BsplineGridType::SparseMatrixType A;
    BsplineGridType::DenseVectorType b;
    std::vector<unsigned int> kept_nodes;
    std::vector<unsigned int> corresponding_nodes;

    /// Keep environment to save memory

    BsplineGridType::SparseMatrixType B = control_points->createSamplingMatrix(coordinates, remove_nodes_th, kept_nodes, corresponding_nodes);

    { /// Keep scope to save memory
        Eigen::setNbThreads(8);
        A = B.transpose() *B;
        b = B.transpose()*values_vector;
    }


     // Regularisation
     //   if (lambda)
     //   {
     //       if (debug) std::cout << "\tUse regularisation with lambda ="<<lambda<< std::endl;
     //       BsplineGridType::SparseMatrixType Adiv = control_points->createContinuousDivergenceSamplingMatrix(kept_nodes,corresponding_nodes);
     //       A = A+lambda/(1-lambda)*Adiv;/// See notes on p118 of my thesis for an explanation
     //   }



    if (debug) std::cout << "\tCompute coefficients in this control_points"<< std::endl;
    /// Available additional solvers
    /// BiCGSTAB + IncompletLUT preconditioner
    /// SparseQR
    /// SPQR
    /// PastixLU

    BsplineGridType::DenseVectorType x;//,x2,x3,x4;
    echo::solvers::ConfigurationParameter config;
    config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
    config.tol=1E-06;
    config.verbose = true;
    std::string str_solver;

    /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
    if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
    echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
    /// This can happen if there is a region with very densely populated points
    //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
    //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

    if (debug) std::cout << "\tCoefficients computed, replace control_points values with the calculated coefficients"<< std::endl;
    /// Reorganize x into an array of coefficients, taking into account the reordering
    control_points->setAll<BsplineGridType::DenseVectorType>(x,  kept_nodes);

    ///---------------------------------------------------------------------------//
    ///                       Calculate interpolated result                       //
    ///---------------------------------------------------------------------------//

    std::vector< PointType > interpolated_t(N2);
    for (int i=0; i<N2; i++){
        interpolated_t[i] = i*cycleDuration/N2;
    }
    BsplineGridType::SparseMatrixType Bi = control_points->createSamplingMatrix(interpolated_t, remove_nodes_th, kept_nodes, corresponding_nodes);
    BsplineGridType::DenseVectorType interpolated_values = Bi * x;

    ///---------------------------------------------------------------------------//
    ///                       PRINT RESULTS TO FILE                               //
    ///---------------------------------------------------------------------------//

    int ncycles = 3;

    {
        std::ofstream file;
        std::string filename = "/tmp/original.txt";
        file.open(filename, ofstream::out);
        for (int cycle=0; cycle<ncycles; ++cycle ){
            for (int i=0; i<N; i++){
                file << coordinates[i]+cycleDuration*cycle << " "<< values[i]<<std::endl;
            }
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    {
        std::ofstream file;
        std::string filename = "/tmp/interpolated.txt";
        file.open(filename, ofstream::out);
        for (int cycle=0; cycle<ncycles; ++cycle ){
            for (int i=0; i<N2; i++){
                file << interpolated_t[i]+cycleDuration*cycle << " "<< interpolated_values[i]<<std::endl;
            }
        }
        file.close();
        std::cout << "output in "<< filename<<std::endl;
    }

    */

    return 0;

}


template <typename TBsplineGridType>
void fit_cyclicBspline1D(const std::vector<typename TBsplineGridType::PointType > &t, const std::vector< typename TBsplineGridType::CoefficientType > &v,
                         double lambda, typename TBsplineGridType::Pointer control_points){

    /****************************************************************************
    *                               CALCULATE EXTENT                            *
    ****************************************************************************/

    Eigen::setNbThreads(threads_to_use);
    Eigen::initParallel();



    ///---------------------------------------------------------------------------//
    ///                       FIND BSPLINE COEFFICIENTS                              //
    ///---------------------------------------------------------------------------//

    echo::Point<double,2*TBsplineGridType::IDims> control_points_bounds;
    control_points->GetBorderBoundsOfInfluence(&control_points_bounds[0]);

    typename TBsplineGridType::DenseVectorType values_vector(v.size(),1);
    std::vector<ValueType>::const_iterator const_iter;
    typename TBsplineGridType::MatrixElementType *eigen_iter;

    for (const_iter = v.begin(), eigen_iter= values_vector.data();const_iter!=v.end();  ++const_iter)
    {
        for (int nd = 0; nd< TBsplineGridType::IDims; nd++){
            (*eigen_iter)=(*const_iter)[nd];
            ++eigen_iter;
        }
    }

    typename TBsplineGridType::SparseMatrixType A;
    typename TBsplineGridType::DenseVectorType b;
    std::vector<unsigned int> kept_nodes;
    std::vector<unsigned int> corresponding_nodes;

    /// Keep environment to save memory

    typename TBsplineGridType::SparseMatrixType B = control_points->createSamplingMatrix(t, remove_nodes_th, kept_nodes, corresponding_nodes);

    { /// Keep scope to save memory
        Eigen::setNbThreads(8);
        A = B.transpose() *B;
        b = B.transpose()*values_vector;
    }


    // Regularisation
    if (lambda)
    {
        if (debug) std::cout << "\tUse regularisation with lambda ="<<lambda<< std::endl;
        BsplineGridType::SparseMatrixType Adiv = control_points->createContinuousDivergenceSamplingMatrix(kept_nodes,corresponding_nodes);
        A = A+lambda/(1-lambda)*Adiv;/// See notes on p118 of my thesis for an explanation
    }


    /// Available additional solvers
    /// BiCGSTAB + IncompletLUT preconditioner
    /// SparseQR
    /// SPQR
    /// PastixLU

    typename TBsplineGridType::DenseVectorType x;//,x2,x3,x4;
    echo::solvers::ConfigurationParameter config;
    config.max_iterations=1000; /// This number might look huge but we need to get to the desired tolerance
    config.tol=1E-06;
    config.verbose = true;
    std::string str_solver;

    /// Add some condition like if the matrix is larger than e.g. 5000 x 5000 then use an iterative method such as BiCGSTAB
    if (debug) std::cout << "\tSolve linear system where A is " << A.rows() <<"x"<< A.cols()<<std::endl;
    echo::solvers::solveWithSPQR<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_SPQR";
    /// This can happen if there is a region with very densely populated points
    //echo::solvers::solveWithGMRES<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_GMRES";
    //echo::solvers::solveWithBiCGSTAB<BsplineGridType::SparseMatrixType, BsplineGridType::DenseVectorType>(A,b,x,config); str_solver="_BiCGSTAB";

    /// Reorganize x into an array of coefficients, taking into account the reordering
    control_points->setAll(x,  kept_nodes);
}


template <typename TBsplineGrid1DType, typename TBsplineGrid2DType>
void interpolateClosedContour(const std::vector< typename TBsplineGrid2DType::PointType> &coordinates,  const std::vector< typename TBsplineGrid1DType::PointType> &interpolated_contour_angle,
                              std::vector< typename TBsplineGrid2DType::PointType> &interpolated_contour){

    /// parameters: these normally do not need changing
    double grid_spacing1D = M_PI/2;
    double lambda = 0;

    /// Points should be passed centred but in case they are not, we centre them

    typename TBsplineGrid2DType::PointType centroid;
    centroid[0]=0; centroid[1]=0;
    for (int i=0; i<coordinates.size(); i++){
        centroid+=coordinates[i];
    }
    centroid/=coordinates.size();
    /// -------
    std::vector< typename TBsplineGrid2DType::PointType > coordinatesAs(coordinates.size());
    // compute centroid


    // compute polar coords
    for (int i=0; i<coordinates.size(); i++){
        double angle = std::atan2(coordinates[i][1]-centroid[1],coordinates[i][0]-centroid[0]);
        double magnitude = sqrt((coordinates[i][0]-centroid[0])*(coordinates[i][0]-centroid[0]) +
                (coordinates[i][1]-centroid[1])*(coordinates[i][1]-centroid[1]) );
        coordinatesAs[i][0] = magnitude;
        coordinatesAs[i][1] = angle;
    }
    // do cyclic interpolation on the polar coords
    std::vector<typename TBsplineGrid1DType::PointType> coordinates1D(coordinates.size());
    typedef echo::Point<typename TBsplineGrid1DType::MatrixElementType,1> Value1DType;
    std::vector<Value1DType> values(coordinates.size());
    double mint = std::numeric_limits<double>::max();
    double maxt= -std::numeric_limits<double>::max();
    for (int i=0; i<coordinates.size(); i++){
        coordinates1D[i] = coordinatesAs[i][1];// angle
        if (coordinates1D[i][0]<mint){
            mint = coordinates1D[i][0];
        }
        if (coordinates1D[i][0]>maxt){
            maxt = coordinates1D[i][0];
        }
        values[i] = coordinatesAs[i][0];
    }
    double bounds[] = {mint, maxt};

    typename TBsplineGrid1DType::Pointer control_points(new TBsplineGrid1DType(bounds, bsd, grid_spacing1D, 0)); /// the control point grid does not need border. This grid covers the whole ROI and there is no control_points division.
    typename TBsplineGrid1DType::IndexType cyclicDimensions;
    cyclicDimensions[0]=1; // first dimension is cyclic
    control_points->SetCyclicDimensions(cyclicDimensions);
    control_points->SetCyclicBounds(0,-M_PI,M_PI);

    control_points->SetParallel(parallel_on, threads_to_use);
    control_points->UpdateCyclicBehaviour();

    fit_cyclicBspline1D<TBsplineGrid1DType>(coordinates1D, values, lambda, control_points);
    auto interpolated_contourAs = control_points->evaluate(interpolated_contour_angle);
    interpolated_contour.resize(interpolated_contourAs.size());
    // re-convert into cartesian
    for (int i=0; i<interpolated_contourAs.size(); i++){
        interpolated_contour[i][0] = interpolated_contourAs[i][0]*std::cos(interpolated_contour_angle[i][0]) +centroid[0];
        interpolated_contour[i][1] = interpolated_contourAs[i][0]*std::sin(interpolated_contour_angle[i][0]) + centroid[1];
    }
}
